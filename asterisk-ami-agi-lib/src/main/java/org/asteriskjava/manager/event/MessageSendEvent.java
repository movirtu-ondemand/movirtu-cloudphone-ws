
package org.asteriskjava.manager.event;

/**
 * Response to an MessageSendEvent.
 * 
 * @see org.asteriskjava.manager.action.MessageSendEvent
 * @author srt
 * @version $Id$
 */
public class MessageSendEvent extends ResponseEvent
{
    /**
     * Serial version identifier.
     */
    private static final long serialVersionUID = 110724860606259687L;
    private String response;
    private String to;
    private String from;
    private String body;
    

    /**
     * @param source
     */
    public MessageSendEvent(Object source)
    {
        super(source);
    }

    /**
     * Returns the result of the corresponding Originate action.
     * 
     * @return "Success" or "Failure"
     */
    public String getResponse()
    {
        return response;
    }

    /**
     * Sets the result of the corresponding Originate action.
     * 
     * @param response "Success" or "Failure"
     */
    public void setResponse(String response)
    {
        this.response = response;
    }
    
    
	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}
