package com.movirtu.asterisk.ami;

import java.io.IOException;

import org.asteriskjava.manager.AuthenticationFailedException;
import org.asteriskjava.manager.DefaultManagerConnection;
import org.asteriskjava.manager.TimeoutException;
import org.asteriskjava.manager.action.MessageSendAction;

import com.movirtu.mxmanyme.domain.WinsSmsInfo;
import com.movirtu.persistence.SmsPersistence;
import com.movirtu.sms.handler.SmsHandler;
import com.movirtu.sms.util.PropertyManager;
import com.movirtu.sms.util.SmsDirection;
import com.movirtu.sms.util.SmsState;
import com.movirtu.sms.util.SmsUtils;
import com.movirtu.usermapping.UserMappingManager;



public class AmiSmsHandler implements SmsHandler {

	private static volatile AmiSmsHandler client;
	private DefaultManagerConnection dmc; 
	private int DEFAULT_ASTE_MGMT_PORT = 5038;

	public static AmiSmsHandler getInstace() {
		if(client==null) {
			client = new AmiSmsHandler();
		}
		return client;
	}

	private AmiSmsHandler() {
		dmc = new DefaultManagerConnection();		
		dmc.setUsername(PropertyManager.getInstance().getPropertyValue(PropertyManager.ASTERISK_MGMT_USER));
		dmc.setPassword(PropertyManager.getInstance().getPropertyValue(PropertyManager.ASTERISK_MGMT_PWD));
		dmc.setHostname(PropertyManager.getInstance().getPropertyValue(PropertyManager.ASTERISK_MGMT_IP));
		dmc.setPort(PropertyManager.getInstance().getIntProperty(PropertyManager.ASTERISK_MGMT_PORT, DEFAULT_ASTE_MGMT_PORT));

		try {
			dmc.login();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AuthenticationFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dmc.addEventListener(new ManagerEventListenerImpl());
	}

	public boolean sendSms(WinsSmsInfo sms) {
		System.out.println("Sending sms using AMI "+sms);

		WinsSmsInfo newSms = SmsUtils.createSMS(SmsUtils.generateUUID(), SmsDirection.INCOMING.getDirection(), SmsState.DELIVERED.getState(),sms.getSender(), sms.getReceiver(), sms.getLogTime(), sms.getMessage());
		System.out.println("Saving sms="+newSms);
		SmsPersistence.getInstace().saveSMS(newSms);

		//		SmsUtils.mapSenderPhoneNumberToSip(sms);
		//		SmsUtils.mapReceiverPhoneNumberToSip(sms);
		MessageSendAction msgSnd = new MessageSendAction();
		String to = UserMappingManager.getInstance().getSipUserFromPhoneNumber(sms.getReceiver());
		if(to==null) {
			System.out.println("TO field can not be null. Returning without sending SMS");
			return false;
		}
		msgSnd.setTo("pjsip:"+to);
		String from = UserMappingManager.getInstance().getPhoneNumberFromSipUser(sms.getSender());
		if(from==null) {
			from = sms.getSender();
		}
		msgSnd.setFrom(from);
		msgSnd.setBody(sms.getMessage());

		try {
			System.out.println("Sending sms "+msgSnd);
			dmc.sendAction(msgSnd);
			return true;
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}