package com.movirtu.asterisk.ami;

import org.asteriskjava.manager.event.UserEvent;

public class SMSEvent extends UserEvent {

	public SMSEvent(Object source) {
		super(source);
		// TODO Auto-generated constructor stub
	}

	public String getTo() {
		return To;
	}
	public void setTo(String to) {
		To = to;
	}

	public String getFrom() {
		return From;
	}

	public void setFrom(String from) {
		From = from;
	}

	public String getBody() {
		return Body;
	}

	public void setBody(String body) {
		Body = body;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String To;
	private String From;
	private String Body;

}
