package com.movirtu.asterisk.ari;

import java.io.IOException;

import org.codehaus.jettison.json.JSONException;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.movirtu.mxmanyme.domain.WinsSmsInfo;
import com.movirtu.persistence.SmsPersistence;
import com.movirtu.sms.handler.SmsHandlerManager;
import com.movirtu.sms.util.PropertyManager;
import com.movirtu.sms.util.SmsUtils;

/**
 * Basic Echo Client Socket
 */
@WebSocket(maxTextMessageSize = 64 * 1024)
public class AriCallbackSocket {

	//private CountDownLatch closeLatch;
	private String channelId;
	private String memberContent;
	private AriClient ariClient;

	@SuppressWarnings("unused")
	private Session session;

	public AriCallbackSocket(AriClient ariClient) {
		this.ariClient=ariClient;
		//this.closeLatch = new CountDownLatch(1);
	}

	//	public void awaitClose()
	//			throws InterruptedException {
	//		this.closeLatch.await();
	//	}

	@OnWebSocketClose
	public void onClose(int statusCode, String reason) {
		System.out.printf("Connection closed: %d - %s%n", statusCode, reason);
		this.session = null;
		this.ariClient.reinitializeAriClient();
	}

	@OnWebSocketConnect
	public void onConnect(Session session) {
		System.out.printf("Got connect: %s%n", session);
		this.session = session;
		String receivedData = null;
		//String destUri = "http://54.72.39.131:8088/ari/channels/1401817092.0/continue";
		//String destUri1 = "http://54.72.39.131:8088/ari/channels";
		String destUri1 = PropertyManager.getInstance().getPropertyValue(PropertyManager.ARI_SMS_CHANNEL);
		try {
			receivedData = HttpConnectionHandler.httpGet(destUri1);
			//System.out.printf("channels: %s", receivedData);
		} catch (Throwable t) {
			// t.printStackTrace();
		} 
		System.out.println("receivedData="+receivedData);
		try {
			JSONArray channelArray=(JSONArray) JSONValue.parse(receivedData);
			if(channelArray.size()==0) {
				this.channelId=null;
			}
			if(channelArray.size()>1) {
				throw new Exception("More than one channel received from server");
			}
			if(channelArray.size()==0) {
				System.out.println("No channel object returned from server");
				return;
			}
			JSONObject channelObject = (JSONObject) channelArray.get(0);
			System.out.println("channelObject="+channelObject.toString());
			channelId=(String) channelObject.get("id");
			System.out.println("Channel id="+channelId);
			//TODO This method was throwing exception
			clearChannel(channelId);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void clearChannel(String channelId) {
		System.out.println("Inside clearChannel with channelId="+channelId);
		String ariChannel = PropertyManager.getInstance().getPropertyValue(PropertyManager.ARI_SMS_CHANNEL);
		//String destUri = "http://54.72.39.131:8088/ari/channels/"+channelId +"/continue";
		String destUri = ariChannel+"/"+channelId +"/continue";
		try {
			String myKey[] = {};
			String myValue[] = {};
			String receivedData = HttpConnectionHandler.httpPost(destUri, myKey, myValue);
			//System.out.printf("Connecting to : %s:%s", destUri, receivedData);
		} catch (Throwable t) {
			//t.printStackTrace();
		}
	}

	@OnWebSocketMessage
	public void onMessage(String msg) {
		System.out.printf("onMessage incoming mesasge string " +msg);
		try {
			if(channelId==null) {
				channelId = SmsUtils.parseChannelId(msg);
			}
			memberContent=null;
			WinsSmsInfo sms = SmsUtils.parseAndCreateSMS(msg);
			if(sms!=null) {
				System.out.printf("SMS Created" +sms);
				SmsUtils.mapSenderSipToPhoneNumber(sms);
				SmsUtils.mapReceiverSipToPhoneNumber(sms);
				//UserMappingManager.getInstance().mapSenderSipToPhoneNumber(sms);
				System.out.printf("SMS final" +sms);
				clearChannel(channelId);
				SmsPersistence.getInstace().saveSMS(sms);
				sendSMS(sms);
				//SmsPersistence.getInstace().saveSMS(sms);
			}else {
				System.out.printf("False onMessage Callback");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void sendSMS(WinsSmsInfo sms) {
		System.out.println("Sending sms: "+sms);
		try {
			//AmiClient.getInstace().sendMessage(memberContent);
			boolean success = SmsHandlerManager.getInstance().sendSms(sms);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
