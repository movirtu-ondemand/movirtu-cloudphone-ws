package com.movirtu.asterisk.ari;


import java.net.URI;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.movirtu.mxmanyme.dao.PropertiesDao;
import com.movirtu.persistence.SmsPersistence;
import com.movirtu.sms.handler.SmsHandlerManager;
import com.movirtu.sms.util.PropertyManager;
import com.movirtu.sms.util.TmpWinsSmsInfoDaoImpl;
import com.movirtu.usermapping.UserMappingManager;


/**
 * Example of a simple Echo Client.
 */
public class AriClient {

	private PropertiesDao propertiesDao;
	//private static String ARI_URL="ari_url";
	private static String PROPERTIES_TABLE="wins_properties";
	private WebSocketClient client;
	private AriCallbackSocket socket;
	private String destUri;
	private CountDownLatch closeLatch;
	ExecutorService executor = Executors.newFixedThreadPool(1);

	public static void main(String[] args) {
		AriClient ariClient = new AriClient();
		ariClient.initializeAppContext();
		ariClient.initializeAriClient();
		// Initialize SmsHandlerManager
		//SmsHandlerManager.getInstance().initializeManager();
	}

	class Initializationtask implements Runnable {

		AriClient ariClient;

		Initializationtask(AriClient ariClient) {
			this.ariClient=ariClient;
		}

		@Override
		public void run() {
			//destUri = propertiesDao.findProperties(ARI_URL, PROPERTIES_TABLE);
			destUri = PropertyManager.getInstance().getPropertyValue(PropertyManager.ARI_SMS_URL);
			client = new WebSocketClient();
			socket = new AriCallbackSocket(ariClient);
			try {
				URI echoUri = new URI(destUri);
				ClientUpgradeRequest request = new ClientUpgradeRequest();
				client.start();
				client.connect(socket, echoUri, request);
				System.out.printf("Connecting to :: %s%n", echoUri);
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}
	}

	class Reinitializationtask implements Runnable {

		AriClient ariClient;

		Reinitializationtask(AriClient ariClient) {
			this.ariClient=ariClient;
		}

		@Override
		public void run() {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				client.stop();
				//client.destroy();
			}catch(InterruptedException ex) {
				System.out.println("InterruptedException ");
			} catch (Exception e) {
				e.printStackTrace();
			}
			//destUri = propertiesDao.findProperties(ARI_URL, PROPERTIES_TABLE);
			//client = new WebSocketClient();
			//socket = new AriCallbackSocket(ariClient);
			try {
				URI echoUri = new URI(destUri);
				ClientUpgradeRequest request = new ClientUpgradeRequest();
				client.start();
				client.connect(socket, echoUri, request);
				System.out.printf("Re-connecting to :: %s%n", echoUri);
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}
	}

	public void initializeAriClient() {
		executor.execute(new Initializationtask(this));
		try {
			this.closeLatch = new CountDownLatch(1);
			this.closeLatch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void reinitializeAriClient() {
		executor.execute(new Reinitializationtask(this));
	}

	private void initializeAppContext() {
		// open/read the application context file
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

		// instantiate our spring dao object from the application context
		propertiesDao = (PropertiesDao)ctx.getBean("propertiesDao");
		System.out.println("propertiesDao="+propertiesDao);
		PropertyManager.getInstance().initialize(propertiesDao);
		SmsHandlerManager.getInstance().initialize(propertiesDao);

		TmpWinsSmsInfoDaoImpl smsDao = (TmpWinsSmsInfoDaoImpl)ctx.getBean("smsDao");
		System.out.println("smsDao="+smsDao);
		SmsPersistence.getInstace().initialize(smsDao);

		UserMappingManager.getInstance().initialize(ctx);
	}

}
