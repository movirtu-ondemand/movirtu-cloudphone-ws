package com.movirtu.asterisk.ari;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import sun.misc.BASE64Encoder;

public class HttpConnectionHandler {

	public static String httpGet(String urlStr) throws IOException {
		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		String username = "hey";
		String password = "peekaboo";

		String userpass = username + ":" + password;
		BASE64Encoder enc = new sun.misc.BASE64Encoder();
		String encodedAuthorization = enc.encode( userpass.getBytes() );

		String basicAuth = "Basic " + encodedAuthorization;
		conn.setRequestProperty ("Authorization", basicAuth);

		//conn.setRequestProperty(, value);
		if (conn.getResponseCode() != 200) {
			System.out.println("reponse="+conn.getResponseCode());
			throw new IOException(conn.getResponseMessage());
		}

		// Buffer the result into a string
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(conn.getInputStream()));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			sb.append(line);
		}
		bufferedReader.close();
		conn.disconnect();
		return sb.toString();
	}

	public static String httpPost(String urlStr, String[] paramName,
			String[] paramVal) throws Exception {
		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setUseCaches(false);
		conn.setAllowUserInteraction(false);
		String username = "hey";
		String password = "peekaboo";

		String userpass = username + ":" + password;
		BASE64Encoder enc = new sun.misc.BASE64Encoder();
		String encodedAuthorization = enc.encode( userpass.getBytes() );

		String basicAuth = "Basic " + encodedAuthorization;
		conn.setRequestProperty ("Authorization", basicAuth);
		//conn.setRequestProperty("Content-Type",
		//		"application/x-www-form-urlencoded");

		// Create the form content
		//OutputStream out = conn.getOutputStream();
		java.io.OutputStream out =  conn.getOutputStream();
		Writer writer = new OutputStreamWriter(out, "UTF-8");
		for (int i = 0; i < paramName.length; i++) {
			writer.write(paramName[i]);
			writer.write("=");
			writer.write(URLEncoder.encode(paramVal[i], "UTF-8"));
			writer.write("&");
		}
		writer.close();
		out.close();

		if (conn.getResponseCode() != 200) {
			//throw new IOException(conn.getResponseMessage());
		}

		// Buffer the result into a string
		BufferedReader rd = new BufferedReader(
				new InputStreamReader(conn.getInputStream()));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = rd.readLine()) != null) {
			sb.append(line);
		}
		rd.close();

		conn.disconnect();
		return sb.toString();
	}

}
