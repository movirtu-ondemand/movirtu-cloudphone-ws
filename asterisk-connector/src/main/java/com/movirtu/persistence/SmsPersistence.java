package com.movirtu.persistence;

import com.movirtu.mxmanyme.domain.WinsSmsInfo;
import com.movirtu.sms.util.TmpWinsSmsInfoDaoImpl;


public class SmsPersistence {

	private static volatile SmsPersistence persistenceClient; 
	private TmpWinsSmsInfoDaoImpl smsDao;

	public static SmsPersistence getInstace() {
		if(persistenceClient==null) {
			persistenceClient = new SmsPersistence();
		}
		return persistenceClient;
	}

	private SmsPersistence() {

	}

	public void initialize(TmpWinsSmsInfoDaoImpl smsDao) {
		this.smsDao = smsDao;
	}

	public void saveSMS(WinsSmsInfo sms) {
		smsDao.saveSMS(sms);
	}
}
