package com.movirtu.sms.handler;

import com.movirtu.mxmanyme.domain.WinsSmsInfo;

public interface SmsHandler {

	public boolean sendSms(WinsSmsInfo sms);

}
