package com.movirtu.sms.handler;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.asterisk.ami.AmiSmsHandler;
import com.movirtu.mxmanyme.dao.PropertiesDao;
import com.movirtu.mxmanyme.domain.WinsSmsInfo;
import com.movirtu.smshandler.csoft.CSoftSmsHandler;
import com.movirtu.smshandler.loopback.LoopbackSmsHandler;
import com.movirtu.smshandler.smpp.SmppSmsHandler;

public class SmsHandlerManager {

	private static volatile SmsHandlerManager smsManager;
	private SmsHandler smsTransmitter;
	private SmsHandler smsReceiver1;
	private SmsHandler smsReceiver2;
	private SmsHandler loopbackSmsHandler;
	private SmsHandler incomingSmsHandler;
	private String smppServer1Address="127.0.0.1";
	private int smppServer1Port=0;
	private String smppServer2Address="127.0.0.1";
	private int smppServer2Port=0;

	@Autowired
	public static PropertiesDao propertiesDao;
	private static String SMS_HANDLER_KEY="sms_handler";
	private static String smsHandlerName="ami";
	private static String PROPERTIES_TABLE="wins_properties";

	public static enum SmsHandlerName {

		AMI("ami"), SMPP ("smpp"), CSOFT("csoft");

		private String name;

		SmsHandlerName(String name) {
			this.name=name;
		}

		public String getName() {
			return this.name;
		}
	}

	public static SmsHandlerManager getInstance() {
		if(smsManager==null) {
			smsManager = new SmsHandlerManager();
		}
		return smsManager;
	}

	private SmsHandlerManager() {
		//TODO
		//initializeManager();
	}

	// TODO
	//public void initializeManager() {
	public void initialize(PropertiesDao propertiesDao) {
		this.propertiesDao = propertiesDao;
		this.loopbackSmsHandler= LoopbackSmsHandler.getInstace();
		this.incomingSmsHandler = AmiSmsHandler.getInstace();

		// TODO this is not a right way to read the properties as this makes a call 
		// to DB every time, instead we should load the properties like we do in 
		// SmppSmsHandler.java
		smppServer1Address = propertiesDao.findProperties("smpp-ip-address", PROPERTIES_TABLE);
		smppServer1Port = Integer.parseInt(propertiesDao.findProperties("smpp-port", PROPERTIES_TABLE));

		smppServer2Address = propertiesDao.findProperties("smpp2-ip-address", PROPERTIES_TABLE);
		smppServer2Port = Integer.parseInt(propertiesDao.findProperties("smpp2-port", PROPERTIES_TABLE));


		String smsHandlerName = propertiesDao.findProperties(SMS_HANDLER_KEY, PROPERTIES_TABLE);

		if(smsHandlerName.equalsIgnoreCase(SmsHandlerManager.SmsHandlerName.AMI.getName())) {

			this.smsTransmitter= AmiSmsHandler.getInstace();
			System.out.println("using AMI sms handler="+smsTransmitter);

		} else if(smsHandlerName.equalsIgnoreCase(SmsHandlerManager.SmsHandlerName.CSOFT.getName())) {

			this.smsTransmitter= CSoftSmsHandler.getInstace();
			System.out.println("using csoft sms handler"+smsTransmitter);

			//		} else if(smsHandlerName.equalsIgnoreCase(SmsHandlerManager.SmsHandlerName.LOOPBACK.getName())) {
			//			
			//			this.smsHandler= LoopbackSmsHandler.getInstace();
			//			System.out.println("using loopback sms handler"+smsHandler);
		} else if(smsHandlerName.equalsIgnoreCase(SmsHandlerManager.SmsHandlerName.SMPP.getName())) {

			this.smsTransmitter= new SmppSmsHandler(smppServer1Address,smppServer1Port, "tr");
			System.out.println("using smpp sms handler1"+smsTransmitter);

			//			this.smsReceiver1= new SmppSmsHandler(smppServer1Address,smppServer1Port, "r");
			//			System.out.println("using smpp sms handler2"+smsTransmitter);

			this.smsReceiver2= new SmppSmsHandler(smppServer2Address,smppServer2Port, "tr");
			System.out.println("using smpp sms handler2"+smsReceiver2);

		} else {
			System.out.println("TODO : need work");
		}
		//		switch(smsHandlerName) {
		//		case SmsHandlerManager.SmsHandlerName.AMI.getName():
		//			break;
		//		case SmsHandlerManager.SmsHandlerName.SMPP.getName():
		//			break;
	}

	public SmsHandler getSmsHandler() {
		return this.smsTransmitter;
	}

	public SmsHandler getLoopbackSmsHandler() {
		return this.loopbackSmsHandler;
	}

	public boolean sendSms(WinsSmsInfo sms) {
		if(((LoopbackSmsHandler)loopbackSmsHandler).useLoopbackHandler(sms)) {
			return loopbackSmsHandler.sendSms(sms);
		}else{
			return smsTransmitter.sendSms(sms);
		}
	}

	public boolean DeliverIncomingSms(WinsSmsInfo sms) {
		return incomingSmsHandler.sendSms(sms);
	}

	//	public void registerSmsHandler(SmsHandler smsHandler) {
	//		this.smsHandler = smsHandler;
	//	}

}
