package com.movirtu.sms.util;

import com.movirtu.mxmanyme.dao.PropertiesDao;


public class PropertyManager {

	public static PropertiesDao propertiesDao;
	private static volatile PropertyManager porpertyManager;
	private static String PROPERTIES_TABLE="wins_properties";
	public static String ARI_SMS_URL="ari_sms_url";
	public static String ARI_SMS_CHANNEL="ari_sms_channel";
	public static String ASTERISK_MGMT_USER = "asterisk_mgmt_user";
	public static String ASTERISK_MGMT_PWD = "asterisk_mgmt_password";
	public static String ASTERISK_MGMT_IP = "asterisk_mgmt_ip";
	public static String ASTERISK_MGMT_PORT = "asterisk_mgmt_port";

	public static PropertyManager getInstance() {
		if(porpertyManager==null) {
			porpertyManager = new PropertyManager();
		}
		return porpertyManager;
	}

	private PropertyManager() {

	}

	public String getPropertyValue(String key) {
		return PropertyManager.propertiesDao.findProperties(key, PROPERTIES_TABLE);
	}

	public String getPropertyValue(String key, String defultValue) {
		String propValue = PropertyManager.propertiesDao.findProperties(key, PROPERTIES_TABLE);
		if(propValue ==null || propValue.length()==0) {
			return defultValue;
		}
		return propValue;
	}

	/**
	 * Gets a property and converts it into byte.
	 */
	public byte getByteProperty(String propName, byte defaultValue) {
		return Byte.parseByte(getPropertyValue(propName, Byte.toString(defaultValue)));
	}

	/**
	 * Gets a property and converts it into integer.
	 */
	public int getIntProperty(String propName, int defaultValue) {
		return Integer.parseInt(getPropertyValue(propName, Integer.toString(defaultValue)));
	}

	public void initialize(PropertiesDao propertiesDao) {
		PropertyManager.propertiesDao=propertiesDao;
	}
}
