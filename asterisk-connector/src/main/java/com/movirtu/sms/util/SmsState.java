package com.movirtu.sms.util;

public enum SmsState {
	PENDING(1), DELIVERED (0);

	private int state;

	SmsState(int state) {
		this.state=state;
	}

	public int getState() {
		return this.state;
	}
}
