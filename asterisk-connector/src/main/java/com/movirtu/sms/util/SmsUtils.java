package com.movirtu.sms.util;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.UUID;

import org.codehaus.jettison.json.JSONException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.movirtu.mxmanyme.domain.WinsSmsInfo;
import com.movirtu.usermapping.UserMappingManager;


//TODO this class shoud be in a common project as this is accesed by multiple projects.
public class SmsUtils {

	public static WinsSmsInfo createSMS(String messageid,int state,String sender,String receiver,String logTime,String message) {
		return createSMS(messageid, SmsDirection.OUTGOING.getDirection(), state, sender, receiver, logTime, message);
	}

	public static WinsSmsInfo createSMS(String messageid, String sender,String receiver,String message) {
		return createSMS(messageid, SmsDirection.OUTGOING.getDirection(), SmsState.DELIVERED.getState(), sender, receiver, new DateTime().toString(), message);
	}

	public static WinsSmsInfo createSMS(String messageid, String sender,String receiver,String logTime,String message) {
		return createSMS(messageid, SmsDirection.OUTGOING.getDirection(), SmsState.DELIVERED.getState(), sender, receiver, logTime, message);
	}

	public static WinsSmsInfo createSMS(String messageid,int direction,int state,String sender,String receiver,String logTime,String message) {
		return createSMS(messageid, direction, state, sender, receiver, convertToSqlTimestamp(logTime), message);
	}

	public static WinsSmsInfo createSMS(String messageid,int direction,int state,String sender,String receiver,Timestamp logTime,String message) {
		WinsSmsInfo sms = new WinsSmsInfo();
		sms.setMessageid(messageid);
		sms.setDirection(direction);
		sms.setState(state);
		sms.setSender(sender);
		sms.setReceiver(receiver);
		sms.setLogTime(logTime);
		sms.setMessage(message);
		return sms;
	}

	public static long convertToLong(final String iso8601string) throws ParseException {
		//DateTime dt = new DateTime();
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		Date date = new Date(fmt.parseMillis(iso8601string));
		return fmt.parseMillis(iso8601string);
	}

	public static Timestamp convertToSqlTimestamp(String timestamp) {
		Timestamp sqlTimestamp=null;
		try {
			sqlTimestamp = new Timestamp(convertToLong(timestamp));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sqlTimestamp;
	}

	public static String generateUUID() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	public static String parseChannelId(String shortMessage) throws IOException, JSONException {
		JSONObject smsObject=(JSONObject) JSONValue.parse(shortMessage);
		System.out.println("smsObject="+smsObject);
		JSONArray args = (JSONArray) smsObject.get("args");

		if(args==null) {
			return null;
		}

		String from = (String) args.get(0);
		String callerNumber = parseParam("from" , from);
		System.out.println("callerNumber" +callerNumber);

		String content = (String) args.get(1);
		String messageContent = parseParam("content" , content);
		System.out.println("memberContent" +messageContent);

		String timestamp = (String) smsObject.get("timestamp");
		System.out.println("timestamp="+timestamp);

		JSONObject channel = (JSONObject)smsObject.get("channel");
		System.out.println("channel="+channel);

		return (String) channel.get("id");
	}

	public static WinsSmsInfo parseAndCreateSMS(String shortMessage) throws IOException, JSONException {
		JSONObject smsObject=(JSONObject) JSONValue.parse(shortMessage);
		System.out.println("smsObject="+smsObject);
		JSONArray args = (JSONArray) smsObject.get("args");

		if(args==null) {
			return null;
		}

		String from = (String) args.get(0);
		String callerNumber = parseParam("from" , from);
		System.out.println("callerNumber" +callerNumber);

		String content = (String) args.get(1);
		String messageContent = parseParam("content" , content);
		System.out.println("memberContent" +messageContent);

		String timestamp = (String) smsObject.get("timestamp");
		System.out.println("timestamp="+timestamp);

		JSONObject channel = (JSONObject)smsObject.get("channel");
		System.out.println("channel="+channel);

		JSONObject dialplan = (JSONObject) channel.get("dialplan");
		System.out.println("dialplan="+dialplan);

		String calledNumber = (String) dialplan.get("exten");
		System.out.println("calledNumber="+calledNumber);

		return SmsUtils.createSMS(generateUUID(), callerNumber, calledNumber, timestamp, messageContent);
	}

	// This tokenizer is directly linked to the asterisk dial plan configuration.
	// Asterisk does not send the caller name and number in json string for SMS. It sends
	//  "caller":{"name":"","number":""} which does not contain the value.
	// So we have to make changes in the asterisk dialplan file to send caller name and number
	// inside the body. Following is the value to be added in the asterisk dialplan file:
	// same => n,Stasis(hello,from\":\"${CUT(MESSAGE(from),<,1)},content\":\"${MESSAGE(body)})
	private static String parseParam(String paramName, String dataStr) {
		String paramValue = null;
		String tokenValue = null;
		StringTokenizer strings = new StringTokenizer(dataStr,":\"");
		while (strings.hasMoreElements()) {
			tokenValue = strings.nextElement().toString().trim();
			if(tokenValue.equals(paramName)) {
				continue;
			}
			paramValue=tokenValue;
		}
		//System.out.println("Returning paramValue="+paramValue +" for paramName="+paramName);
		return paramValue;
	}

	public static void mapSenderSipToPhoneNumber(WinsSmsInfo sms) {
		System.out.println("Converting sender="+sms.getSender());
		String convertedSender = UserMappingManager.getInstance().getPhoneNumberFromSipUser(sms.getSender());
		if(convertedSender!=null) {
			sms.setSender(convertedSender);
		}
		System.out.println("Converted sender="+sms.getSender());
	}

	public static void mapSenderPhoneNumberToSip(WinsSmsInfo sms) {
		System.out.println("Converting sender="+sms.getSender());
		String convertedSender = UserMappingManager.getInstance().getSipUserFromPhoneNumber(sms.getSender());
		if(convertedSender!=null) {
			sms.setSender(convertedSender);
		}
		System.out.println("Converted sender="+sms.getSender());
	}
	public static void mapReceiverSipToPhoneNumber(WinsSmsInfo sms) {
		System.out.println("Converting receiver="+sms.getReceiver());
		String convertedReceiver = UserMappingManager.getInstance().getPhoneNumberFromSipUser(sms.getReceiver());
		if(convertedReceiver!=null) {
			sms.setReceiver(convertedReceiver);
		}
		System.out.println("Converted receiver="+sms.getReceiver());
	}

	public static void mapReceiverPhoneNumberToSip(WinsSmsInfo sms) {
		System.out.println("Converting receiver="+sms.getReceiver());
		String convertedReceiver = UserMappingManager.getInstance().getSipUserFromPhoneNumber(sms.getReceiver());
		if(convertedReceiver!=null) {
			sms.setReceiver(convertedReceiver);
		}
		System.out.println("Converted receiver="+sms.getReceiver());
	}
}
