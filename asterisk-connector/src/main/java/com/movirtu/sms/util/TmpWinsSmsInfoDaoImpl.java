package com.movirtu.sms.util;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.movirtu.mxmanyme.dao.WinsSmsInfoDao;
import com.movirtu.mxmanyme.dao.impl.spring.AbstractDAO;
import com.movirtu.mxmanyme.domain.WinsSmsInfo;

@Repository
public class TmpWinsSmsInfoDaoImpl extends AbstractDAO<WinsSmsInfo> {

	private static Logger logger = Logger.getLogger(TmpWinsSmsInfoDaoImpl.class);
	private static final String TABLE_NAME = "call";

	public TmpWinsSmsInfoDaoImpl() {
		super(TABLE_NAME, WinsSmsInfo.class);
	}

	/**
	 * @see WinsSmsInfoDao#insert(Call)
	 */
	public void saveSMS(WinsSmsInfo sms) {
		System.out.println("saving SMS " +sms);

		String sqlQuery = "INSERT into df_sms_info (messageid,direction,state,sender,receiver,log_time,message) VALUES (?,?,?,?,?,?,?)";
		int rowsInseted = getSimpleJdbcTemplate().update(sqlQuery, sms.getMessageid(), sms.getDirection(), sms.getState(), sms.getSender(), sms.getReceiver(),sms.getLogTime(), sms.getMessage());
		System.out.println("saved successfully in the DB " + rowsInseted);
	}

}
