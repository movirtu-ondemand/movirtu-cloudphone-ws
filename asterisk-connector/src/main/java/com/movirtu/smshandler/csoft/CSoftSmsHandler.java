package com.movirtu.smshandler.csoft;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import com.movirtu.mxmanyme.domain.WinsSmsInfo;
import com.movirtu.sms.handler.SmsHandler;

public class CSoftSmsHandler implements SmsHandler {

	private static volatile CSoftSmsHandler smsHandler; 
	private static String userName = "KHartlev.257640";
	private static String password = " JOURET.SOONG";
	private static String pin = "27414232";

	public static CSoftSmsHandler getInstace() {
		if(smsHandler==null) {
			smsHandler = new CSoftSmsHandler();
		}
		return smsHandler;
	}

	private CSoftSmsHandler() {

	}

	@Override
	public boolean sendSms(WinsSmsInfo sms) {
		System.out.println("Inside Csoft sendSms with sms="+sms);
		sendToCSoft(userName, pin, sms.getReceiver(), sms.getMessage());
		return true;
	}

	private void sendToCSoft(String username, String pin, String sendTo, String message) {
		System.out.println("Inside sendToCSoft with username="+username);
		System.out.println("Inside sendToCSoft with pin="+pin);
		System.out.println("Inside sendToCSoft with sendTo="+sendTo);
		System.out.println("Inside sendToCSoft with message="+message);
		try {
			URL csoftURL = new URL("https://www.csoft.co.uk/sendsms");
			HttpsURLConnection connection = (HttpsURLConnection)csoftURL.openConnection();
			System.out.println("Connection ="+connection);

			String postBody = "Username=" + URLEncoder.encode(username, "UTF-8") + "&" +
					"PIN=" + URLEncoder.encode(pin, "UTF-8") + "&" +
					"SendTo=" + URLEncoder.encode(sendTo, "UTF-8") + "&" +
					"Message=" + URLEncoder.encode(message, "UTF-8") + "&" +
					"ResponseFormat=1";

			connection.setRequestMethod("POST");

			// post the parameters
			connection.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
			System.out.println("OutputStreamWriter ="+wr);
			wr.write(postBody);
			wr.flush();
			wr.close();

			// now let's get the results
			System.out.println("connecting");
			connection.connect(); // throws IOException
			System.out.println("connecting sucsessfully");
			int responseCode = connection.getResponseCode();  // 200, 404, etc
			System.out.println("responseCode ="+responseCode);
			String responseMsg = connection.getResponseMessage(); // OK, Forbidden, etc
			System.out.println("responseMsg ="+responseMsg);
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuffer results = new StringBuffer();
			System.out.println("results ="+results);

			String oneline;
			while ( (oneline = br.readLine()) != null) {
				results.append(oneline);
			}

			br.close();

			//JOptionPane.showMessageDialog(this, "Server Response:" +  URLDecoder.decode(results.toString(), "UTF-8"));
		}
		catch(Exception ex) {
			System.out.println("Exception in sending SMS"+ex);
		}
	}

}
