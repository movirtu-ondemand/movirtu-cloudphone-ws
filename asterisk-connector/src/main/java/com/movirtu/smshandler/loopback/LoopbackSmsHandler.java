package com.movirtu.smshandler.loopback;

import com.movirtu.asterisk.ami.AmiSmsHandler;
import com.movirtu.mxmanyme.domain.WinsSmsInfo;
import com.movirtu.sms.handler.SmsHandler;
import com.movirtu.sms.util.PropertyManager;
import com.movirtu.usermapping.UserMappingManager;

public class LoopbackSmsHandler implements SmsHandler {

	private static volatile LoopbackSmsHandler smsHandler;
	private AmiSmsHandler amiSmsHandler; 
	private final String useLoopbackKey="useLoopback";

	public static LoopbackSmsHandler getInstace() {
		if(smsHandler==null) {
			smsHandler = new LoopbackSmsHandler();			
		}
		return smsHandler;
	}

	private LoopbackSmsHandler() {
		amiSmsHandler = AmiSmsHandler.getInstace();
	}

	@Override
	public boolean sendSms(WinsSmsInfo sms) {
		System.out.printf("Inside LoopbackSmsHandler sendSms" +sms);
		//		String sipUser = UserMappingManager.getInstance().getSipUserFromPhoneNumber(sms.getReceiver());
		//		if(sipUser==null || sipUser.length()==0) {
		//			System.out.println("Sip user can not be null");
		//			return false;
		//		}else {
		//			UserMappingManager.getInstance().mapReceiverPhoneNumberToSip(sms);
		//			System.out.printf("Loopback sms=" +sms);
		return amiSmsHandler.sendSms(sms);
		//		}
		//		return true;
	}

	public boolean useLoopbackHandler(WinsSmsInfo sms) {
		System.out.printf("Inside useLoopbackHandler useLoopbackHandler" +sms);
		String useLoopback = PropertyManager.getInstance().getPropertyValue(useLoopbackKey);
		if(useLoopback!=null && useLoopback.equalsIgnoreCase("false")) {
			return false;
		}
		String sipUser = UserMappingManager.getInstance().getSipUserFromPhoneNumber(sms.getReceiver());
		if(sipUser==null || sipUser.length()==0) {
			return false;
		}else {
			return true;
		}
	}

	private void sendToCSoft(String username, String pin, String sendTo, String message) {

	}

}
