package com.movirtu.wins.mclient.common;

public enum ErrorCode {

	GENERIC_ERROR(601), 
	SUBSCRIBER_NOT_FOUND(602),
	MESSAGE_NOT_FOUND(603),
	SYSTEM_FAILURE(604),
	USER_NOT_AUTHENTICATED(605),
	USER_ALREADY_EXISTS(606),
	USER_NOT_FOUND(607);

	private int errorCode;

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	ErrorCode(int errorCode) {
		this.errorCode =errorCode;
	}
}
