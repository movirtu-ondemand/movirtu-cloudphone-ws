package com.movirtu.wins.mclient.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.movirtu.mxmanyme.domain.UserInfoBean;
import com.movirtu.mxmanyme.domain.WinsSmsInfo;

public class Utils {

	public static List normanizeList(List completeList, int offset, int size) {
		List normalizedList = new ArrayList(size);
		int upperIndex = offset+size < completeList.size() ? offset+size:completeList.size();
		for(int i=offset;i<upperIndex;i++) {
			normalizedList.add(completeList.get(i));
		}
		return normalizedList;
	}

	public static List normanizeList(List completeList, int offset) {
		return Utils.normanizeList(completeList, offset, completeList.size());
	}

	//TODO This is a work around till we find a sql query to get sindle entry from DB. Currently
	// we get two entries for recent. one for sender=a and receiver=b; two sender=b and receiver=a;
	public static List<WinsSmsInfo> removeDuplicateRecent(List<WinsSmsInfo> completeList) {
		List<WinsSmsInfo> normalizedList = new ArrayList<WinsSmsInfo>();
		List<String> listOfIds = new ArrayList<String>();

		for(WinsSmsInfo entry : completeList) {

			if( !listOfIds.contains(entry.getReceiver()+entry.getSender()) &&  
					!listOfIds.contains(entry.getSender()+entry.getReceiver())) {
				normalizedList.add(entry);
				listOfIds.add(entry.getSender()+entry.getReceiver());
			}
		}

		return normalizedList;
	}

	public static List<WinsSmsInfo> sortListOnTime(List<WinsSmsInfo> completeList, int offset, int size) {

		class TimeComparator implements Comparator<WinsSmsInfo> {

			@Override
			public int compare(WinsSmsInfo o1, WinsSmsInfo o2) {

				if(o1.getLogTime().getTime() < o2.getLogTime().getTime()){
					return 1;
				} else {
					return -1;
				}
			}
		}

		TimeComparator timeComparator = new TimeComparator();
		Collections.sort(completeList, timeComparator);
		return normanizeList(completeList, offset, size);

	}

	public static JSONObject makeJsonObject(List<WinsSmsInfo> messageList) throws JSONException {
		JSONObject obj = null;
		JSONObject obj1 = null;
		JSONArray jsonArray = new JSONArray();

		for (WinsSmsInfo SMS :messageList) {
			obj = new JSONObject();
			try {
				obj.put("UID", SMS.getMessageid());
				obj.put("direction", SMS.getDirection());
				obj.put("state", SMS.getState());
				obj.put("receiver", SMS.getReceiver());
				obj.put("sender", SMS.getSender());
				obj.put("timestamp", SMS.getLogTime());
				obj.put("sms-content", SMS.getMessage());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			obj1 = new JSONObject();
			obj1.put("sms", obj);
			jsonArray.put(obj1);
		}

		JSONObject finalobject = new JSONObject();
		//		finalobject.put("numOfMessage", this.numOfMessage);
		//		finalobject.put("userMsisdn", this.userMsisdn);
		//		finalobject.put("offset", this.offset);
		finalobject.put("sms_list", jsonArray);
		return finalobject;
	}

	//TODO string should be static final.
	public static JSONObject makeUserProfile(UserInfoBean userInfoBean) throws JSONException {
		JSONObject obj = new JSONObject();
		try {
			obj.put("msisdn", userInfoBean.getMsisdn());
			obj.put("user_name", userInfoBean.getUser_name());
			obj.put("sip-user", userInfoBean.getSip_user());
			obj.put("sip-password", userInfoBean.getSip_password());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		JSONObject finalobject = new JSONObject();
		finalobject.put("user_profile", obj);
		return finalobject;
	}

	public static UserInfoBean makeUserProfileFromJson(JSONObject jsonObject, UserInfoBean userInfoBean) throws JSONException {

		if(jsonObject.get("email") !=null) {
			userInfoBean.setEmail_id((String) jsonObject.get("email"));
		} 
		if(jsonObject.get("password") !=null) {
			userInfoBean.setPassword((String) jsonObject.get("password"));
		}
		if(jsonObject.get("firstName") !=null) {
			userInfoBean.setFirst_name((String) jsonObject.get("firstName"));
		}
		if(jsonObject.get("lastName") !=null) {
			userInfoBean.setLast_name((String) jsonObject.get("lastName"));
		}
		if(jsonObject.get("userImage") !=null) {
			userInfoBean.setUser_image((String) jsonObject.get("userImage"));
		}
		if(jsonObject.get("msisdn") !=null) {
			userInfoBean.setMsisdn((String) jsonObject.get("msisdn"));
		}
		if(jsonObject.get("sip-user") !=null) {
			userInfoBean.setSip_user((String) jsonObject.get("sip-user"));
		}
		if(jsonObject.get("sip-password") !=null) {
			userInfoBean.setSip_password((String) jsonObject.get("sip-password"));
		}
		if(jsonObject.get("user_name") !=null) {
			userInfoBean.setUser_name((String) jsonObject.get("user_name"));
		}

		return userInfoBean;
	}

	/**
	 * Returns a pseudo-random number between min and max, inclusive.
	 * The difference between min and max can be at most
	 * <code>Integer.MAX_VALUE - 1</code>.
	 *
	 * @param min Minimum value
	 * @param max Maximum value.  Must be greater than min.
	 * @return Integer between min and max, inclusive.
	 * @see java.util.Random#nextInt(int)
	 */
	public static int randomInt(int min, int max) {

		// NOTE: Usually this should be a field rather than a method
		// variable so that it is not re-seeded every call.
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}
}
