/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.wins.mclient.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.mxmanyme.domain.UserInfoBean;
import com.movirtu.wins.mclient.common.ErrorCode;
import com.movirtu.wins.mclient.common.Utils;
import com.movirtu.wins.mclient.service.AppContext;
import com.movirtu.wins.mclient.service.MclientServiceImpl;

/**
 *
 * @author Prashant
 */
//@XmlType(propOrder = { "cmdjson","result", "num_of_messages", "offset","user_msisdn", "sms_list" })
@XmlType(propOrder = {"output"})

public class AuthenticateUser extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AuthenticateUser.class);

	@Autowired
	MclientServiceImpl service = (MclientServiceImpl)new AppContext().getDAO();

	String user_msisdn;
	String user_name;
	String user_pin;

	@XmlTransient
	String output;

	public AuthenticateUser(String user_msisdn, String user_name, String user_pin) {
		super("g_res_authenticate_user");
		this.user_msisdn=user_msisdn;
		this.user_name = user_name;
		this.user_pin = user_pin;
	}

	public String getUser_msisdn() {
		return user_msisdn;
	}

	public void setUser_msisdn(String user_msisdn) {
		this.user_msisdn = user_msisdn;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}


	public String getUser_pin() {
		return user_pin;
	}

	public void setUser_pin(String user_pin) {
		this.user_pin = user_pin;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	@Override
	public void process(){
		ResultSetWrapper resultSet = service.authenticateUser(user_msisdn, user_name, user_pin);
		result = resultSet.getResult();
		if(result.equals(result.FAIL)) {
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			return;
		}
		// Performing location update
		resultSet = service.performLoctionUpdate(user_msisdn, user_name);
		
		resultSet = service.getUserProfile(user_msisdn, user_name);
		UserInfoBean user = (UserInfoBean) resultSet.getObject();

		try {
			JSONObject jsonOutput = Utils.makeUserProfile(user);
			output = jsonOutput.toString();

		} catch (JSONException e) {

			result = Result.FAIL;
			errcode = ""+ErrorCode.GENERIC_ERROR.getErrorCode();
			log.error("Unable to ceate JSON object for "+user_msisdn);
			log.error(e);
			return;
		}

		log.debug("After Req-> Authenticate User, user_msisdn: "+user_msisdn + " ,user_name: "+user_name +" result="+result);
	}
}
