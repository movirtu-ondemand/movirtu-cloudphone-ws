/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.wins.mclient.request;


import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.wins.mclient.service.AppContext;
import com.movirtu.wins.mclient.service.MclientServiceImpl;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"output"})

public class DeleteAllSMS extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DeleteAllSMS.class);

	@Autowired
	MclientServiceImpl service = (MclientServiceImpl)new AppContext().getDAO();

	String user_msisdn;

	public DeleteAllSMS(String user_msisdn) {
		super("g_res_del_all_msg");
		this.user_msisdn = user_msisdn;
	}

	public String getUser_msisdn() {
		return user_msisdn;
	}

	public void setUser_msisdn(String user1_msisdn) {
		this.user_msisdn = user1_msisdn;
	}

	@Override
	public void process(){

		ResultSetWrapper resultSet = service.deleteAllSMSThreads(user_msisdn);
		result = resultSet.getResult();
		if(result.equals(result.FAIL)) {
			errcode = ""+resultSet.getErrorCode().getErrorCode();
		}

		log.debug("After Req-> delete SMS, user_msisdn: "+user_msisdn +" result="+result);
	}


}
