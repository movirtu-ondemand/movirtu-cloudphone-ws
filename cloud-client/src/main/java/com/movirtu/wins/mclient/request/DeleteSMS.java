/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.wins.mclient.request;


import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.wins.mclient.service.AppContext;
import com.movirtu.wins.mclient.service.MclientServiceImpl;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"output"})

public class DeleteSMS extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DeleteSMS.class);

	@Autowired
	MclientServiceImpl service = (MclientServiceImpl)new AppContext().getDAO();

	String user_msisdn;
	String msg_uid;

	public DeleteSMS(String user_msisdn, String msg_uid) {
		super("g_res_del_msg");
		this.user_msisdn = user_msisdn;
		this.msg_uid = msg_uid;
	}

	public String getUser_msisdn() {
		return user_msisdn;
	}

	public void setUser_msisdn(String user_msisdn) {
		this.user_msisdn = user_msisdn;
	}

	public String getMsg_uid() {
		return msg_uid;
	}

	public void setMsg_uid(String msg_uid) {
		this.msg_uid = msg_uid;
	}

	@Override
	public void process(){

		ResultSetWrapper resultSet = service.deleteSMS(msg_uid);
		result = resultSet.getResult();
		if(result.equals(result.FAIL)) {
			errcode = ""+resultSet.getErrorCode().getErrorCode();
		}

		log.debug("After Req-> delete SMS, user_msisdn: "+user_msisdn + " ,msg_uid: "+msg_uid +" result="+result);
	}

}
