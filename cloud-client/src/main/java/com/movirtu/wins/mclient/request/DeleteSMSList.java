/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.wins.mclient.request;


import java.util.List;

import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.wins.mclient.service.AppContext;
import com.movirtu.wins.mclient.service.MclientServiceImpl;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"output"})

public class DeleteSMSList extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DeleteSMSList.class);

	@Autowired
	MclientServiceImpl service = (MclientServiceImpl)new AppContext().getDAO();

	String user_msisdn;
	List<String> msg_uid_list;

	public DeleteSMSList(String user_msisdn, List<String> msg_uid_list) {
		super("g_res_del_msg_list");
		this.user_msisdn = user_msisdn;
		this.msg_uid_list = msg_uid_list;
	}

	public String getUser_msisdn() {
		return user_msisdn;
	}

	public void setUser_msisdn(String user_msisdn) {
		this.user_msisdn = user_msisdn;
	}

	public List<String> getMsg_uid() {
		return msg_uid_list;
	}

	public void setMsg_uid(List<String> msg_uid) {
		this.msg_uid_list = msg_uid;
	}

	@Override
	public void process(){

		ResultSetWrapper resultSet = service.deleteSMSList(msg_uid_list);
		result = resultSet.getResult();
		if(result.equals(result.FAIL)) {
			errcode = ""+resultSet.getErrorCode().getErrorCode();
		}

		log.debug("After Req-> delete SMS List, user_msisdn: "+user_msisdn + " ,msg_uid: "+msg_uid_list +" result="+result);
	}

}
