/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.wins.mclient.request;


import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.wins.mclient.service.AppContext;
import com.movirtu.wins.mclient.service.MclientServiceImpl;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"output"})

public class DeleteSMSThread extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DeleteSMSThread.class);

	@Autowired
	MclientServiceImpl service = (MclientServiceImpl)new AppContext().getDAO();

	String user1_msisdn;
	String user2_msisdn;

	public DeleteSMSThread(String user_msisdn, String msg_uid) {
		super("g_res_del_msg_thread");
		this.user1_msisdn = user_msisdn;
		this.user2_msisdn = msg_uid;
	}

	public String getUser1_msisdn() {
		return user1_msisdn;
	}

	public void setUser1_msisdn(String user1_msisdn) {
		this.user1_msisdn = user1_msisdn;
	}

	public String getUser2_msisdn() {
		return user2_msisdn;
	}

	public void setUser2_msisdn(String user2_msisdn) {
		this.user2_msisdn = user2_msisdn;
	}

	@Override
	public void process(){

		ResultSetWrapper resultSet = service.deleteSMSThread(user1_msisdn, user2_msisdn);
		result = resultSet.getResult();
		if(result.equals(result.FAIL)) {
			errcode = ""+resultSet.getErrorCode().getErrorCode();
		}

		log.debug("After Req-> delete SMS, user_msisdn: "+user1_msisdn + " ,user2_msisdn: "+user2_msisdn +" result="+result);
	}


}
