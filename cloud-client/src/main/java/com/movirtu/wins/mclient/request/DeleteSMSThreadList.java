/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.wins.mclient.request;


import java.util.List;

import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.wins.mclient.service.AppContext;
import com.movirtu.wins.mclient.service.MclientServiceImpl;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"output"})

public class DeleteSMSThreadList extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DeleteSMSThreadList.class);

	@Autowired
	MclientServiceImpl service = (MclientServiceImpl)new AppContext().getDAO();

	String user1_msisdn;
	List<String> user2_msisdn_list;

	public DeleteSMSThreadList(String user_msisdn,  List<String> user2_msisdn_list) {
		super("g_res_del_msg_thread_list");
		this.user1_msisdn = user_msisdn;
		this.user2_msisdn_list = user2_msisdn_list;
	}

	public String getUser1_msisdn() {
		return user1_msisdn;
	}

	public void setUser1_msisdn(String user1_msisdn) {
		this.user1_msisdn = user1_msisdn;
	}

	public List<String> getUser2_msisdn_list() {
		return user2_msisdn_list;
	}

	public void setUser2_msisdn_list(List<String> user2_msisdn_list) {
		this.user2_msisdn_list = user2_msisdn_list;
	}

	@Override
	public void process(){

		ResultSetWrapper resultSet = service.deleteSMSThreadList(user1_msisdn, user2_msisdn_list);
		result = resultSet.getResult();
		if(result.equals(result.FAIL)) {
			errcode = ""+resultSet.getErrorCode().getErrorCode();
		}

		log.debug("After Req-> delete SMS, user_msisdn: "+user1_msisdn + " ,user2_msisdn: "+user2_msisdn_list +" result="+result);
	}


}
