/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.wins.mclient.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.wins.mclient.service.AppContext;
import com.movirtu.wins.mclient.service.MclientServiceImpl;

/**
 *
 * @author Prashant
 */
//@XmlType(propOrder = { "cmdjson","result", "num_of_messages", "offset","user_msisdn", "sms_list" })
@XmlType(propOrder = {"output"})

public class DeleteUser extends Resp {

	private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DeleteUser.class);

	@Autowired
	MclientServiceImpl service = (MclientServiceImpl)new AppContext().getDAO();

	String user_msisdn;
	String user_name;
	String user_password;

	@XmlTransient
	String output;

	public DeleteUser(String user_msisdn, String user_name, String user_password) {
		super("g_res_delete_user");
		this.user_msisdn=user_msisdn;
		this.user_name=user_name;
		this.user_password = user_password;
	}

	public String getUser_msisdn() {
		return user_msisdn;
	}

	public void setUser_msisdn(String user_msisdn) {
		this.user_msisdn = user_msisdn;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_password() {
		return user_password;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	@Override
	public void process() {
		logger.debug("Inside Req-> delete User, user_msisdn: "+user_msisdn + " ,user_name: "+user_name +",user_password: "+user_password);

		ResultSetWrapper resultSet = service.deleteUser(user_msisdn, user_name, user_password);
		result = resultSet.getResult();

		if(result.equals(result.FAIL)) {
			errcode = ""+resultSet.getErrorCode().getErrorCode();
		}

		logger.debug("After Req-> update User, user_msisdn: "+user_msisdn + " ,user_name: "+user_name +",user_password: "+user_password);
	}
}
