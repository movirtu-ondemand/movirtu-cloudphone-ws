/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.wins.mclient.request;


import java.util.List;

import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.mxmanyme.domain.WinsSmsInfo;
import com.movirtu.wins.mclient.common.ErrorCode;
import com.movirtu.wins.mclient.common.Utils;
import com.movirtu.wins.mclient.service.AppContext;
import com.movirtu.wins.mclient.service.MclientServiceImpl;

/**
 *
 * @author Prashant
 */
//@XmlType(propOrder = { "cmdjson","result", "num_of_messages", "offset","user_msisdn", "sms_list" })
@XmlType(propOrder = {"output"})

public class FetchSMS extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FetchSMS.class);

	@Autowired
	MclientServiceImpl service = (MclientServiceImpl)new AppContext().getDAO();

	String num_of_messages;
	String user_msisdn;
	String offset;
	//String sms_list;
	@XmlTransient
	String output;

	public String getNum_of_messages() {
		return num_of_messages;
	}

	public void setNum_of_messages(String numOfMessage) {
		this.num_of_messages = numOfMessage;
	}

	public String getUser_msisdn() {
		return user_msisdn;
	}

	public void setUser_msisdn(String userMsisdn) {
		this.user_msisdn = userMsisdn;
	}

	public String getOffset() {
		return offset;
	}

	public void setOffset(String offset) {
		this.offset = offset;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public FetchSMS(String num_of_messages, String user_msisdn, String offset) {
		super("g_resp_fetch_msg");
		this.num_of_messages = num_of_messages;
		this.user_msisdn = user_msisdn;
		this.offset = offset;
	}

	@Override
	public void process() {
		result = Result.SUCCESS;
		
		ResultSetWrapper resultSet = service.fetchSMS(num_of_messages, user_msisdn, offset);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Fetch SMS failed for "+user_msisdn + " due to "+errcode);
			return;
		}

		List<WinsSmsInfo> messageList = (List<WinsSmsInfo>) resultSet.getObject();
		
		try {
			//makeJsonObject(messageList);
			//			JSONArray jsonList = makeJsonObject(messageList);
			//			sms_list = jsonList.toString();
			JSONObject jsonOutput = Utils.makeJsonObject(messageList);
			output = jsonOutput.toString();
		
		} catch (JSONException e) {
			
			result = Result.FAIL;
			errcode = ""+ErrorCode.GENERIC_ERROR.getErrorCode();
			log.error("Unable to ceate JSON object for "+user_msisdn);
			log.error(e);
			return;
		}
		
		log.debug("After Req-> fetch SMS, user_msisdn: "+user_msisdn + " ,num_of_messages: "+num_of_messages+" result="+result);
	}

}
