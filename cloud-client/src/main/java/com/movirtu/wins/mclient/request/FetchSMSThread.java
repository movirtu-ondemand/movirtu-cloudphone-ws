/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.wins.mclient.request;


import java.util.List;

import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.mxmanyme.domain.WinsSmsInfo;
import com.movirtu.wins.mclient.common.ErrorCode;
import com.movirtu.wins.mclient.common.Utils;
import com.movirtu.wins.mclient.service.AppContext;
import com.movirtu.wins.mclient.service.MclientServiceImpl;

/**
 *
 * @author Prashant
 */
//@XmlType(propOrder = { "cmdjson","result", "num_of_messages", "offset","user_msisdn", "sms_list" })
@XmlType(propOrder = {"output"})

public class FetchSMSThread extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FetchSMSThread.class);

	@Autowired
	MclientServiceImpl service = (MclientServiceImpl)new AppContext().getDAO();

	String num_of_messages;
	String user1_msisdn;
	String user2_msisdn;
	String offset;
	//String sms_list;
	@XmlTransient
	String output;

	public String getNum_of_messages() {
		return num_of_messages;
	}

	public void setNum_of_messages(String numOfMessage) {
		this.num_of_messages = numOfMessage;
	}

	public String getUser1_msisdn() {
		return user1_msisdn;
	}

	public void setUser1_msisdn(String userMsisdn) {
		this.user1_msisdn = userMsisdn;
	}

	public String getUser2_msisdn() {
		return user2_msisdn;
	}

	public void setUser2_msisdn(String userMsisdn) {
		this.user2_msisdn = userMsisdn;
	}

	public String getOffset() {
		return offset;
	}

	public void setOffset(String offset) {
		this.offset = offset;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public FetchSMSThread(String num_of_messages, String user1_msisdn, String user2_msisdn, String offset) {
		super("g_res_get_msg_thread ");
		this.num_of_messages = num_of_messages;
		this.user1_msisdn = user1_msisdn;
		this.user2_msisdn = user2_msisdn;
		this.offset = offset;
	}

	@Override
	public void process(){
		result = Result.SUCCESS;

		ResultSetWrapper resultSet = service.fetchSMSThread(num_of_messages, user1_msisdn, user2_msisdn, offset);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			return;
		}

		List<WinsSmsInfo> messageList = (List<WinsSmsInfo>) resultSet.getObject();

		try {

			JSONObject jsonOutput = Utils.makeJsonObject(messageList);
			output = jsonOutput.toString();

		} catch (JSONException e) {

			result = Result.FAIL;
			errcode = ""+ErrorCode.GENERIC_ERROR.getErrorCode();
			log.error("SMS fetch Failed for: "+user1_msisdn);
			log.error(e);
			return;
		}

		log.debug("After Req-> fetch SMS, user_msisdn: "+user1_msisdn + " ,num_of_messages: "+num_of_messages+" result="+result);
	}

}
