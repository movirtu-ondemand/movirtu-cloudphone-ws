/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.wins.mclient.request;


import java.util.List;

import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.mxmanyme.domain.WinsSmsInfo;
import com.movirtu.wins.mclient.common.ErrorCode;
import com.movirtu.wins.mclient.common.Utils;
import com.movirtu.wins.mclient.service.AppContext;
import com.movirtu.wins.mclient.service.MclientServiceImpl;

/**
 *
 * @author Prashant
 */
//@XmlType(propOrder = { "cmdjson","result", "num_of_messages", "offset","user_msisdn", "sms_list" })
@XmlType(propOrder = {"output"})

public class SearchSMS extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SearchSMS.class);

	@Autowired
	MclientServiceImpl service = (MclientServiceImpl)new AppContext().getDAO();

	String user_msisdn;
	String num_of_messages;
	String search_string;
	String offset;
	@XmlTransient
	String output;

	public SearchSMS(String user_msisdn, String search_string, String num_of_messages, String offset) {
		super("g_res_search_msg");
		this.user_msisdn=user_msisdn;
		this.search_string = search_string;
		this.num_of_messages = num_of_messages;
		this.offset = offset;
	}

	public String getNum_of_messages() {
		return num_of_messages;
	}

	public void setNum_of_messages(String num_of_messages) {
		this.num_of_messages = num_of_messages;
	}

	public String getSearch_string() {
		return search_string;
	}

	public void setSearch_string(String search_string) {
		this.search_string = search_string;
	}

	public String getOffset() {
		return offset;
	}

	public void setOffset(String offset) {
		this.offset = offset;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	@Override
	public void process(){
		result = Result.SUCCESS;

		ResultSetWrapper resultSet = service.searchContent(user_msisdn, search_string, num_of_messages, Integer.parseInt(num_of_messages), offset);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			return;
		}

		List<WinsSmsInfo> messageList = (List<WinsSmsInfo>) resultSet.getObject();

		try {
			JSONObject jsonOutput = Utils.makeJsonObject(messageList);
			output = jsonOutput.toString();
		} catch (JSONException e) {
			result = Result.FAIL;
			errcode = ""+ErrorCode.GENERIC_ERROR.getErrorCode();
			log.error("SMS search Failed for string: "+search_string);
			log.error(e);
			return;
		}

		log.debug("After Req-> fetch SMS, search_string: "+search_string + " ,num_of_messages: "+num_of_messages+" result="+result);
	}


}
