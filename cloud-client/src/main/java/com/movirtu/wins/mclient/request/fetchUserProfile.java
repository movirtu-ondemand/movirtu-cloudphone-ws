/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.wins.mclient.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.mxmanyme.domain.UserInfoBean;
import com.movirtu.wins.mclient.common.ErrorCode;
import com.movirtu.wins.mclient.common.Utils;
import com.movirtu.wins.mclient.service.AppContext;
import com.movirtu.wins.mclient.service.MclientServiceImpl;

/**
 *
 * @author Prashant
 */
//@XmlType(propOrder = { "cmdjson","result", "num_of_messages", "offset","user_msisdn", "sms_list" })
@XmlType(propOrder = {"output"})

public class fetchUserProfile extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(fetchUserProfile.class);

	@Autowired
	MclientServiceImpl service = (MclientServiceImpl)new AppContext().getDAO();

	String user_msisdn;
	String user_name;

	@XmlTransient
	String output;

	public fetchUserProfile(String user_msisdn, String user_name) {
		super("g_res_user_profile");
		this.user_msisdn=user_msisdn;
		this.user_name = user_name;
	}

	public String getUser_msisdn() {
		return user_msisdn;
	}

	public void setUser_msisdn(String user_msisdn) {
		this.user_msisdn = user_msisdn;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	@Override
	public void process(){

		ResultSetWrapper resultSet = service.getUserProfile(user_msisdn, user_name);

		result = resultSet.getResult();
		if(result.equals(result.FAIL)) {
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			return;
		}

		UserInfoBean user = (UserInfoBean) resultSet.getObject();

		try {
			JSONObject jsonOutput = Utils.makeUserProfile(user);
			output = jsonOutput.toString();

		} catch (JSONException e) {

			result = Result.FAIL;
			errcode = ""+ErrorCode.GENERIC_ERROR.getErrorCode();
			log.error("Unable to ceate JSON object for "+user_msisdn);
			log.error(e);
			return;
		}

		log.debug("After Req-> fetchUserProfile, user_msisdn: "+user_msisdn + " ,user_name: "+user_name +" result="+result);
	}
}
