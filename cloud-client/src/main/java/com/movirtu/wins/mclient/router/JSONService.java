/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.wins.mclient.router;

/**
 *
 * @author david
 */

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
//import javax.xml.ws.handler.MessageContext;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.movirtu.wins.mclient.request.AuthenticateUser;
import com.movirtu.wins.mclient.request.DeleteAllSMS;
import com.movirtu.wins.mclient.request.DeleteSMS;
import com.movirtu.wins.mclient.request.DeleteSMSList;
import com.movirtu.wins.mclient.request.DeleteSMSThread;
import com.movirtu.wins.mclient.request.DeleteSMSThreadList;
import com.movirtu.wins.mclient.request.DeleteUser;
import com.movirtu.wins.mclient.request.FetchSMS;
import com.movirtu.wins.mclient.request.FetchSMSRecent;
import com.movirtu.wins.mclient.request.FetchSMSThread;
import com.movirtu.wins.mclient.request.PError;
import com.movirtu.wins.mclient.request.RegisterUser;
import com.movirtu.wins.mclient.request.Resp;
import com.movirtu.wins.mclient.request.Result;
import com.movirtu.wins.mclient.request.SearchSMS;
import com.movirtu.wins.mclient.request.UpdateUserPassword;
import com.movirtu.wins.mclient.request.UpdateUserProfile;
import com.movirtu.wins.mclient.request.fetchUserProfile;

//import org.apache.cxf.jaxrs.ext.MessageContext;

//@WebService
@Path("/request")
public class JSONService<T> {

	private static final Logger log = Logger.getLogger(JSONService.class);

	//	@Context
	//	private HttpServletRequest request;

	//	private MessageContext messageContext;
	//
	//	@Context
	//	public void setMessageContext(MessageContext messageContext) {
	//	    this.messageContext = messageContext;
	//	}
	//	
	@Context ServletConfig sc;

	//	@Context  
	//	private HttpServletRequest request; 
	//
	//	@Resource(name="wsContext") 
	//	WebServiceContext wsCtxt;

	public void CheckMsisdn(String msisdn)
	{            
		String prefix = sc.getInitParameter("prefix");
		log.debug("Prefix is: "+ prefix);
	}

	T proces(T result){
		Resp res = (Resp)result;
		res.process();
		if(res.getResult().equals(Result.FAIL)){ 
			PError err = new PError(res.errcode, res.getCmdjson());
			return (T)err;
		}

		return (T)res;
	}

	// Temporary method to fix UI issue.
	private String temporarilyFixMSISDN(String msisdn) {

		String fixedMsisdn = null;

		if(msisdn==null || msisdn.length()<4) {
			return msisdn;
		}

		msisdn=msisdn.trim();

		if(msisdn.startsWith("+440")) {
			fixedMsisdn = "0"+msisdn.substring(4);
		}else if(msisdn.startsWith("440")) {
			fixedMsisdn = "0"+msisdn.substring(3);
		}else if(msisdn.startsWith("+44")) {
			fixedMsisdn = "0"+msisdn.substring(3);
		}else if(msisdn.startsWith("44")) {
			fixedMsisdn = "0"+msisdn.substring(2);
		} else {
			fixedMsisdn= msisdn;
		}
		log.debug("Returning :"+fixedMsisdn);
		return fixedMsisdn;
	}

	// Temporary method to fix UI issue.
	public List<String> temporarilyFixMsisdnList(List completeList) {
		List<String> tempFixedList = new ArrayList<String>(completeList.size());

		for(int i=0;i<completeList.size();i++) {
			tempFixedList.add(temporarilyFixMSISDN((String) completeList.get(i)));
		}
		return tempFixedList;
	}

	@POST
	@Path("/mclient")
	@Produces(MediaType.APPLICATION_JSON)       
	public Resp postRequest(
			@QueryParam("cmdjson") String cmd, 
			@QueryParam("num_of_messages") String num_of_messages,
			@QueryParam("user_msisdn") String user_msisdn,
			@QueryParam("user1_msisdn") String user1_msisdn,
			@QueryParam("user2_msisdn") String user2_msisdn,
			@QueryParam("msg_uid") String msg_uid, 
			@QueryParam("msg_uid_list") List<String> msg_uid_list,
			@QueryParam("direction") String direction,
			@QueryParam("offset") String offset,
			@QueryParam("search_string") String search_string,
			@QueryParam("user_name") String user_name,
			@QueryParam("user2_msisdn_list") List<String> user2_msisdn_list,
			@QueryParam("user_profile")JSONObject user_profile,
			@QueryParam("user_password")String user_password
			) {              
		Resp res = null;
		log.debug("Wins POST request : "+cmd 
				+" For user_msisdn: '"+user_msisdn
				+" , num_of_messages is : "+num_of_messages
				+" , user1_msisdn is : "+user1_msisdn
				+" , user2_msisdn is : "+user2_msisdn
				+" , msg_uid is : "+msg_uid
				+" , msg_uid_list is : "+msg_uid_list
				+" , direction is : "+direction
				+" , offset is : "+offset 
				+" , search_string is : "+search_string
				+" , user_name is : "+user_name
				+" , user2_msisdn_list is : "+user2_msisdn_list
				+" , user_profile is : "+user_profile
				+" , user_password is : "+user_password
				//+" , request="+request
				);
		user_msisdn = temporarilyFixMSISDN(user_msisdn);
		user1_msisdn = temporarilyFixMSISDN(user1_msisdn);
		user2_msisdn = temporarilyFixMSISDN(user2_msisdn);
		user2_msisdn_list = temporarilyFixMsisdnList(user2_msisdn_list);

		log.debug("updated value : " 
				+" user_msisdn: "+user_msisdn
				+" , user1_msisdn is : "+user1_msisdn
				+" , user2_msisdn is : "+user2_msisdn
				+" , user2_msisdn_list is : "+user2_msisdn_list
				);

		//		HttpServletRequest req = messageContext.getHttpServletRequest();
		//		HttpServletResponse res1 = messageContext.getHttpServletResponse();

		//		MessageContext msgCtxt = wsCtxt.getMessageContext();
		//	    HttpServletRequest req = (HttpServletRequest)msgCtxt.get(MessageContext.SERVLET_REQUEST);
		//log.debug("HttpRequest is ="+req);

		if(cmd.equalsIgnoreCase("g_req_fetch_msg")){     
			FetchSMS pss = new FetchSMS(num_of_messages, user_msisdn, offset);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_fetch_msg_recent")){     
			FetchSMSRecent pss = new FetchSMSRecent(num_of_messages, user_msisdn, offset);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_get_msg_thread")){     
			FetchSMSThread pss = new FetchSMSThread(num_of_messages, user1_msisdn,user2_msisdn, offset);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_del_msg")){     
			DeleteSMS pss = new DeleteSMS(user_msisdn, msg_uid);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_del_msg_list")){     
			DeleteSMSList pss = new DeleteSMSList(user_msisdn, msg_uid_list);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_del_msg_thread")){     
			DeleteSMSThread pss = new DeleteSMSThread(user1_msisdn, user2_msisdn);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_search_msg")){     
			SearchSMS pss = new SearchSMS(user_msisdn, search_string, num_of_messages, offset);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_register_user")){     
			RegisterUser pss = new RegisterUser(user_msisdn, user_name);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_del_msg_thread_list")){     
			DeleteSMSThreadList pss = new DeleteSMSThreadList(user1_msisdn, user2_msisdn_list);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_del_all_msg")){     
			DeleteAllSMS pss = new DeleteAllSMS(user_msisdn);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_update_user_profile")){     
			UpdateUserProfile pss = new UpdateUserProfile(user_msisdn, user_name, user_profile);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_update_password")){     
			UpdateUserPassword pss = new UpdateUserPassword(user_msisdn, user_name, user_password);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_delete_user")){     
			DeleteUser pss = new DeleteUser(user_msisdn, user_name, user_password);
			res = (Resp)proces((T)pss);
		}else{
			PError err = new PError("518",cmd);
			err.errcode = "518";
			res = (Resp)proces((T)err);                    
		}

		log.debug("Wins : "+cmd+" ,POST respCmd->"+res.getCmdjson() +" resp="+res.toDebugString());
		return res;
	}


	@GET
	@Path("/mclient")
	@Produces(MediaType.APPLICATION_JSON)       
	public Resp getRequest(
			@QueryParam("cmdjson") String cmd, 
			@QueryParam("num_of_messages") String num_of_messages,
			@QueryParam("user_msisdn") String user_msisdn,
			@QueryParam("user1_msisdn") String user1_msisdn,
			@QueryParam("user2_msisdn") String user2_msisdn,
			@QueryParam("msg_uid") String msg_uid, 
			@QueryParam("msg_uid_list") List<String> msg_uid_list,
			@QueryParam("direction") String direction,
			@QueryParam("offset") String offset,
			@QueryParam("search_string") String search_string,
			@QueryParam("user_name") String user_name,
			@QueryParam("user_pin") String user_pin
			){
		Resp res = null;
		log.debug("Wins GET request : "+cmd 
				+" For user_msisdn: "+user_msisdn
				+" , num_of_messages is : "+num_of_messages
				+" , user1_msisdn is : "+user1_msisdn
				+" , user2_msisdn is : "+user2_msisdn
				+" , msg_uid is : "+msg_uid
				+" , msg_uid_list is : "+msg_uid_list
				+" , direction is : "+direction
				+" , offset is : "+offset 
				+" , search_string is : "+search_string
				+" , user_name="+user_name);

		user_msisdn = temporarilyFixMSISDN(user_msisdn);
		user1_msisdn = temporarilyFixMSISDN(user1_msisdn);
		user2_msisdn = temporarilyFixMSISDN(user2_msisdn);

		log.debug("updated value : " 
				+" user_msisdn: "+user_msisdn
				+" , user1_msisdn is : "+user1_msisdn
				+" , user2_msisdn is : "+user2_msisdn
				);

		if(cmd.equalsIgnoreCase("g_req_fetch_msg")){
			FetchSMS pss = new FetchSMS(num_of_messages, user_msisdn, offset);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_fetch_msg_recent")){     
			FetchSMSRecent pss = new FetchSMSRecent(num_of_messages, user_msisdn, offset);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_get_msg_thread")){     
			FetchSMSThread pss = new FetchSMSThread(num_of_messages, user1_msisdn,user2_msisdn, offset);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_del_msg")){     
			DeleteSMS pss = new DeleteSMS(user_msisdn, msg_uid);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_del_msg_list")){     
			DeleteSMSList pss = new DeleteSMSList(user_msisdn, msg_uid_list);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_del_msg_thread")){     
			DeleteSMSThread pss = new DeleteSMSThread(user1_msisdn, user2_msisdn);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_search_msg")){     
			SearchSMS pss = new SearchSMS(user_msisdn, search_string, num_of_messages, offset);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_register_user")){     
			RegisterUser pss = new RegisterUser(user_msisdn, user_name);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_authenticate_user")){     
			AuthenticateUser pss = new AuthenticateUser(user_msisdn, user_name, user_pin);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_user_profile")){     
			fetchUserProfile pss = new fetchUserProfile(user_msisdn, user_name);
			res = (Resp)proces((T)pss);
		}else if(cmd.equalsIgnoreCase("g_req_user_profile")){     
			fetchUserProfile pss = new fetchUserProfile(user_msisdn, user_name);
			res = (Resp)proces((T)pss);
		}else{
			PError err = new PError("518",cmd);
			err.errcode = "518";
			res = (Resp)proces((T)err);                   
		}

		log.debug("Wins : "+cmd+" ,GET respCmd->"+res.getCmdjson() +" resp="+res.toDebugString());
		return res;

	}

}
