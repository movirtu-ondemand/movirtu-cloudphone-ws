/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.movirtu.wins.mclient.service;

import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.movirtu.map.MapJavaHandler;
import com.movirtu.map.message.LURequest;
import com.movirtu.map.message.MsrnRequest;
import com.movirtu.map.message.MsrnResponse;
import com.movirtu.map.message.SRISMRequest;
import com.movirtu.map.message.SRISMResponse;
import com.movirtu.mxmanyme.dao.SipUserPoolDao;
import com.movirtu.mxmanyme.dao.WinsSmsInfoDao;
import com.movirtu.mxmanyme.dao.service.MsrnPoolService;
import com.movirtu.mxmanyme.domain.UserInfoBean;
import com.movirtu.mxmanyme.domain.WinsSmsInfo;
import com.movirtu.mxmanyme.entity.MsrnPool;
import com.movirtu.mxmanyme.exceptions.DuplicateRecordException;
import com.movirtu.mxmanyme.exceptions.NoAvailableSipUserException;
import com.movirtu.mxmanyme.exceptions.RecordNotFoundException;
import com.movirtu.mxmanyme.exceptions.SipUserAlreadySetException;
import com.movirtu.mxmanyme.exceptions.UserNotFoundException;
import com.movirtu.sms.handler.SmsHandlerManager;
import com.movirtu.sms.util.PropertyManager;
import com.movirtu.thrift.generated.ServiceAppType;
import com.movirtu.thrift.generated.TypeOfService;
import com.movirtu.usermapping.UserMappingManager;
import com.movirtu.usermapping.persistence.UserMappingDao;
import com.movirtu.wins.mclient.common.ErrorCode;
import com.movirtu.wins.mclient.common.Utils;
import com.movirtu.wins.mclient.request.Result;
import com.movirtu.wins.mclient.request.ResultSetWrapper;

/**
 *
 * @author Prashant
 */
@Component
public class MclientServiceImpl implements Observer {

	private static final Logger logger = Logger.getLogger(MclientServiceImpl.class);
	private static final String DEFAUL_PIN = "1234";
	private MapJavaHandler thriftHandler = null;
	ExecutorService executor = Executors.newFixedThreadPool(1);
	ScheduledExecutorService msrnReleaseTaskExecutor = Executors.newSingleThreadScheduledExecutor();

	@Autowired
	private WinsSmsInfoDao winsSmsInboxDao;

	@Autowired
	private SipUserPoolDao sipUserInfoDao;

	@Autowired
	private UserMappingDao userMappingDao;

	public MclientServiceImpl() {
		initializeThriftInterface();
	}

	private String getMappedUser(String user) throws RecordNotFoundException {
		String mappedUser = UserMappingManager.getInstance().getMappedUser(user);
		if(mappedUser==null) {
			throw new RecordNotFoundException("No mapping found for user "+user);
		}
		return mappedUser;
	}

	
	class Initializationtask implements Runnable {

		MclientServiceImpl mClientService;

		Initializationtask(MclientServiceImpl mClient) {
			this.mClientService=mClient;
		}

		@Override
		public void run() {
			try {
				//			String isupAddress = vServletConfig.getInitParameter("isupaddress");
				//			String isupPort = vServletConfig.getInitParameter("isupport");
				//			String thriftServerPort = vServletConfig.getInitParameter("thriftServerPort");
				//			logger.debug("isupAddress {}, isupPort {}, thriftServerPort {} ", isupAddress, isupPort, thriftServerPort);
				thriftHandler = MapJavaHandler.getInstance();
				logger.debug("thriftHandler is {} " + thriftHandler);	
				thriftHandler.addObserver(mClientService);
				logger.debug("Starting thriftHandler");
				thriftHandler.start();
			} catch (Exception ex) {
				logger.error("Unable to start Map handler", ex);
			}
		}
	}


	private void initializeThriftInterface() {
		logger.debug("initialize Thrift Interface ");
		executor.execute(new Initializationtask(this));
		logger.debug("Thrift Interface initialized");
	}

	// This is a callback method for JavaServerImpl. For all the thrift messages received
	// from mapgw, this method should get called. This may be redundant in current implementation
	// as most of the service logic is handled by JavaServerImpl class.
	@Override
	public void update(Observable observer, Object message) {
		logger.debug("Entering update with message = {}"+ message);

		if ((message instanceof SRISMResponse)) {
			handleSRISMResponse((SRISMResponse) message);
		} else if ((message instanceof MsrnRequest)) {
			handleMsrnRequest((MsrnRequest) message);
		} else {
			logger.warn("This message is not of desired type: {}" + message);
		}

	}

	private void handleSRISMResponse(SRISMResponse message) {
		logger.debug("Inside handleSRISMResponse");
		ResultSetWrapper resultSet = getUserProfileByMsisdn(message.getMsisdn());
		UserInfoBean user = (UserInfoBean) resultSet.getObject();
		user.setImsi(message.getImsi());
		user.setMcc(message.getMcc());
		user.setMnc(message.getMnc());
		logger.debug("updating imsi,mcc,mnc to DB");
		userMappingDao.updateUserInfo(user);
		//sendRegisterSMS(message.getImsi(), user.getPassword());
		sendRegisterSMS(message.getMsisdn(), user.getPassword());
	}

	private void handleMsrnRequest(MsrnRequest msrnReq) {
		logger.debug("Inside handleMsrnRequest");
		//TODO this should be taken from the DB msrn_pool table.
		//String msrn = PropertyManager.getInstance().getPropertyValue("tmp_msrn");

		String msrn = null;
		MsrnPool msrnEntry = MsrnPoolService.getInstance().getAvailableMsrnFromPool();
		if(msrnEntry != null) {
			msrn = msrnEntry.getMsrn();
		}

		ResultSetWrapper resultSet = getUserProfileByImsi(msrnReq.getImsi());
		UserInfoBean user = (UserInfoBean) resultSet.getObject();
		user.setMsrn(msrn);
		logger.debug("updating msrn to DB");
		userMappingDao.updateUserInfo(user);
		// Sending Msrn Response
		MsrnResponse msrnRes;
		try {
			msrnRes = (MsrnResponse) msrnReq.createResponse();
		} catch (Exception ex) {	
			logger.error("Exception in creating MsrnResponse ",ex);
			return;
		}
		// TODO this is a temporary fix to add country code.
		String tmpMsrn = msrn;
		String countryCode = PropertyManager.getInstance().getPropertyValue("tmp_country_code");
		if(tmpMsrn.startsWith("0")) {
			tmpMsrn = tmpMsrn.substring(1);
			tmpMsrn = countryCode.concat(tmpMsrn);
			logger.debug("Converted tmp msrn="+tmpMsrn);
		}
		msrnRes.setMsrn(tmpMsrn);
		// TODO ends

		//msrnRes.setMsrn(msrn);
		msrnRes.setError((short) 0);
		// submitting a timer task to release the msrn after some time.
		msrnReleaseTaskExecutor.schedule(new MsrnReleaseTimerTask(msrn, user), 60, TimeUnit.SECONDS );
		sendMsrnResponse(msrnRes);
	}

	class MsrnReleaseTimerTask extends TimerTask {

		String msrn;
		long sessionId;
		UserInfoBean userInfo;

		MsrnReleaseTimerTask(String msrn, UserInfoBean userInfo) {
			this.msrn = msrn;
			this.userInfo = userInfo;
		}

		@Override
		public void run() {
			logger.error("Releasing msrn="+msrn);
			MsrnPoolService.getInstance().releaseMsrn(msrn);
			userInfo.setMsrn(null);
			logger.debug("updating user info to release msrn");
			userMappingDao.updateUserInfo(userInfo);
			logger.debug("Msrn released successfully");
		}

	}

	public ResultSetWrapper performLoctionUpdate(String user_msisdn,String user_name) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside performLoctionUpdate with user_msisdn="+user_msisdn +" user_name="+user_name);
		}
		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);
		UserInfoBean user = null;
		try {
			if(user_name!=null && !user_name.isEmpty()) {
				user = userMappingDao.findUserByUserName(user_name);
			} else if(user_msisdn!=null && !user_msisdn.isEmpty()) {
				user = userMappingDao.findUserByMsisdn(user_msisdn);
			}
			sendLocationUpdate(user);
			return result;
		} catch (UserNotFoundException e) {
			logger.error("User not found for user_msisdn="+user_msisdn +" user_name="+user_name);
		}

		result.setResult(Result.FAIL);
		result.setErrorCode(ErrorCode.USER_NOT_FOUND);
		return result;
	}

	public void sendLocationUpdate(UserInfoBean user) {
		LURequest luRequest = new LURequest(user.getImsi());
		luRequest.setMsisdn(user.getMsisdn());
		this.thriftHandler.submit(luRequest);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////////// JSON SERVICE METHOD IMPLEMENTATION /////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	public ResultSetWrapper fetchSMS(String num_of_messages, String user_msisdn, String offset) {
		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);
		List<WinsSmsInfo> messageList = null;

		try {
			messageList = winsSmsInboxDao.findAll(user_msisdn);
			messageList = Utils.normanizeList(messageList, Integer.parseInt(offset),Integer.parseInt(num_of_messages));

			if(messageList==null || messageList.isEmpty()){
				logger.debug("No SMS found for: "+user_msisdn);
				result.setResult(Result.FAIL);
				result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);
				return result;
			}

			//result.setResult(Result.SUCCESS);
			result.setObject(messageList);

		} catch (RecordNotFoundException e) {
			logger.debug("No SMS found for: "+user_msisdn +" "+e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);
		}

		return result;
	}

	public ResultSetWrapper FetchSMSRecent(String num_of_messages, String user_msisdn, String offset) {
		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);
		List<WinsSmsInfo> messageList = null;
		try {
			//String mappedUser = getMappedUser(user_msisdn);
			messageList = winsSmsInboxDao.FetchSMSRecent(user_msisdn);
			logger.debug("Normalizing Message list ="+messageList);
			//TODO This is a work around till we find a sql query to get sindle entry from DB. Currently
			// we get two entries for recent. one for sender=a and receiver=b; two sender=b and receiver=a;
			//messageList = Utils.normanizeList(messageList, Integer.parseInt(offset),Integer.parseInt(num_of_messages));
			messageList = Utils.normanizeList(messageList, Integer.parseInt(offset)*2,Integer.parseInt(num_of_messages));
			logger.debug("Normalized Message list ="+messageList);

			logger.debug("Sorting Message list ="+messageList);
			messageList = Utils.sortListOnTime(messageList, 0, Integer.parseInt(num_of_messages));
			logger.debug("Sorted Message list ="+messageList);

			logger.debug("removing duplicate entries="+messageList);
			messageList = Utils.removeDuplicateRecent(messageList);
			logger.debug("removed duplicate entries ="+messageList);




			if(messageList==null || messageList.isEmpty()){
				logger.debug("No recent SMS found for: "+user_msisdn);
				result.setResult(Result.FAIL);
				result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);
				return result;
			}

			//result.setResult(Result.SUCCESS);
			result.setObject(messageList);

		} catch (RecordNotFoundException e) {
			logger.debug("No recent SMS found for: "+user_msisdn +" "+e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);
		}

		return result;
	}

	public ResultSetWrapper fetchSMSThread(String num_of_messages, String user1_msisdn, String user2_msisdn, String offset) {
		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);
		List<WinsSmsInfo> messageList = null;
		try {
			//			String mappedUser1 = getMappedUser(user1_msisdn);
			//			String mappedUser2 = getMappedUser(user2_msisdn);
			messageList = winsSmsInboxDao.findAllByBParty(user1_msisdn, user2_msisdn);
			messageList = Utils.normanizeList(messageList, Integer.parseInt(offset),Integer.parseInt(num_of_messages));

			if(messageList==null || messageList.isEmpty()){
				logger.debug("No SMS thread found for: "+user1_msisdn +" and " +user2_msisdn);
				result.setResult(Result.FAIL);
				result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);
				return result;
			}

			//result.setResult(Result.SUCCESS);
			result.setObject(messageList);

		} catch (RecordNotFoundException e) {
			logger.debug("No SMS thread found for: "+user1_msisdn +" and " +user2_msisdn +" "+e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);
		}

		return result;
	}

	public ResultSetWrapper deleteSMS(String msg_uid) {
		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);

		try {
			boolean resultSet = winsSmsInboxDao.deleteByMessageId(msg_uid);
			if(resultSet) {
				//				result.setResult(Result.SUCCESS);
				return result;
			}
		} catch (RecordNotFoundException e) {
			logger.debug("No SMS found for: "+msg_uid);
			//			result.setResult(Result.FAIL);
			//			result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);
		}
		result.setResult(Result.FAIL);
		result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);

		return result;
	}

	public ResultSetWrapper deleteSMSList(List<String> msg_uid_list) {
		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);
		//result.setResult(Result.FAIL);

		try {
			boolean resultSet = winsSmsInboxDao.deleteByMessageIdList(msg_uid_list);
			if(resultSet) {
				//result.setResult(Result.SUCCESS);
				return result;
			}
		} catch (RecordNotFoundException e) {
			logger.debug("No SMS found for: "+msg_uid_list);
			//			result.setResult(Result.FAIL);
			//			result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);
		}
		result.setResult(Result.FAIL);
		result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);
		return result;
	}

	public ResultSetWrapper deleteSMSThread(String user1_msisdn, String user2_msisdn) {
		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);
		//result.setResult(Result.FAIL);

		try {
			boolean resultSet = winsSmsInboxDao.deleteMessageThread(user1_msisdn, user2_msisdn);
			if(resultSet) {
				//result.setResult(Result.SUCCESS);
				return result;
			}
		} catch (RecordNotFoundException e) {
			logger.debug("No SMS thread found for: "+user1_msisdn +" and "+user2_msisdn);
		}

		result.setResult(Result.FAIL);
		result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);
		return result;
	}

	public ResultSetWrapper deleteAllSMSThreads(String user_msisdn) {
		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);

		try {
			boolean resultSet = winsSmsInboxDao.deleteAllMessageThreads(user_msisdn);
			if(resultSet) {
				//result.setResult(Result.SUCCESS);
				return result;
			}
		} catch (RecordNotFoundException e) {
			logger.debug("No SMS thread found for: "+user_msisdn);
		}

		result.setResult(Result.FAIL);
		result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);
		return result;
	}

	public ResultSetWrapper deleteSMSThreadList(String user1_msisdn, List<String> user2_msisdn_list) {
		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);

		for(String user2_msisdn: user2_msisdn_list) {
			ResultSetWrapper resultSet = deleteSMSThread(user1_msisdn, user2_msisdn);
			if(resultSet.getResult().equals(Result.FAIL)) {
				return result;
			}
		}

		//		result.setResult(Result.FAIL);
		//		result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);
		return result;
	}

	public ResultSetWrapper searchContent(String user_msisdn, String search_string, String num_of_messages, int numOfEntries, String offset) {
		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);
		//result.setResult(Result.SUCCESS);
		List<WinsSmsInfo> messageList = null;

		try {
			messageList = winsSmsInboxDao.searchContent(user_msisdn, search_string);
			messageList = Utils.normanizeList(messageList, Integer.parseInt(offset),Integer.parseInt(num_of_messages));

			if(messageList!=null && !messageList.isEmpty()){
				result.setObject(messageList);
				return result;
			}

			//result.setObject(messageList);

		} catch (RecordNotFoundException e) {
			logger.error("Exception"+ e);
			//			result.setResult(Result.FAIL);
			//			result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);
		}

		result.setResult(Result.FAIL);
		result.setErrorCode(ErrorCode.MESSAGE_NOT_FOUND);
		logger.debug("No SMS found for: "+user_msisdn +" and search string=" +search_string);
		return result;
	}

	private ResultSetWrapper createUser(String user_msisdn,String user_name) {
		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);

		// Check if user already exists.
		if(user_msisdn!=null && !user_msisdn.isEmpty()) {
			result = getUserProfileByMsisdn(user_msisdn);
		} else if(user_name!=null && !user_name.isEmpty()) {
			result = getUserProfileByUserName(user_name);
		}

		if(result.getResult().equals(Result.SUCCESS)) {
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.USER_ALREADY_EXISTS);
			return result;
		} else {
			// Resetting results.
			result.setResult(Result.SUCCESS);
		}

		// Its a new user, create it.
		try{
			UserInfoBean userInfo = new UserInfoBean();
			userInfo.setMsisdn(user_msisdn);
			userInfo.setUser_name(user_name);
			// This will search for available Sip user and sip password and
			// set it in the userInfoBean object.
			sipUserInfoDao.setSipCredentials(userInfo);
			//TODO pin is hardcoded
			//userInfo.setPassword(DEFAUL_PIN);
			userInfo.setPassword(""+Utils.randomInt(100000, 999999));
			userMappingDao.saveUserToDB(userInfo);

		} catch (NoAvailableSipUserException e) {

			logger.error("NoAvailableSipUserException "+ e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;

		} catch (SipUserAlreadySetException e) {

			logger.error("SipUserAlreadySetException "+ e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;

		} catch (DuplicateRecordException e) {

			logger.error("DuplicateRecordException "+ e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.USER_ALREADY_EXISTS);
			return result;

		} catch (Exception e) {

			logger.error("Exception "+ e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;

		}

		//		result.setResult(Result.SUCCESS);
		return result;
	}

	public ResultSetWrapper registerUser(String user_msisdn,String user_name) {
		ResultSetWrapper result = createUser(user_msisdn, user_name);

		if(result.getResult() == Result.FAIL) {
			return result;
		}
		ResultSetWrapper resultSet = getUserProfile(user_msisdn, user_name);
		UserInfoBean user = (UserInfoBean) resultSet.getObject();
		//sendRegisterSMS(user_msisdn, user.getPassword());
		// TO BE DELETED
		sendRegisterSMSTemoraryMethod(user_msisdn, user.getPassword());
		sendSriSm(user_msisdn);
		result.setResult(Result.SUCCESS);
		return result;
	}

	public void sendSriSm(String user_msisdn) {
		// Need to SRI SM to mapgw first to receive imsi, mcc and mnc
		this.thriftHandler.submit(new SRISMRequest(user_msisdn));

	}

	public void sendMsrnResponse(MsrnResponse message) {
		message.setServiceAppType(new ServiceAppType(TypeOfService.MxShareService));
		this.thriftHandler.submit(message);
	}

	private void sendRegisterSMSTemoraryMethod(String msisdn, String pin) {
		logger.debug("Inside sendRegisterSMSTemoraryMethod");
		sendRegisterSMS(msisdn, pin);
	}


	public void sendRegisterSMS(String user, String pin) {
		logger.debug("Inside  sendRegisterSMS with user="+user);
		//TODO Commenting maogw.sendSMS() code as it is not ready. Sending temporarily 
		// using smpp.
		// Need to SRI SM to mapgw first to receive imsi and all
		//this.thriftHandler.submit(new SRISMRequest(user_msisdn));

		//		Map<String, String> ext = new HashMap<String, String>();
		//		ext.put(SmsRequest.MT_KEY, SmsRequest.MT_KEY_VALUE);
		//		this.thriftHandler.submit(new SmsRequest(user_msisdn, "PIN IS 1234", ext));

		String smsSender = PropertyManager.getInstance().getPropertyValue(PropertyManager.REGISTER_SMS_SENDER);
		//TODO 
		SmsHandlerManager.getInstance().sendSms(smsSender, user, pin);
	}

	public ResultSetWrapper authenticateByUserName(String user_name, String user_pin) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside authenticateUser with user_name="+user_name);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);
		// TODO Temporary change as validation PIN SMS does not reach user.
		if(user_pin.equals(DEFAUL_PIN)) {
			return result;
		}

		if(userMappingDao.authenticateByUserName(user_name, user_pin)) {
			//result.setResult(Result.SUCCESS);
			return result;
		}else {
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.USER_NOT_AUTHENTICATED);
			return result;
		}
	}

	public ResultSetWrapper authenticateByMsisdn(String user_msisdn, String user_pin) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside authenticateUser with user_msisdn="+user_msisdn);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);
		// TODO Temporary change as validation PIN SMS does not reach user.
		if(user_pin.equals(DEFAUL_PIN)) {
			return result;
		}

		if(userMappingDao.authenticateByMsisdn(user_msisdn, user_pin)) {
			//result.setResult(Result.SUCCESS);
			return result;
		}else {
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.USER_NOT_AUTHENTICATED);
			return result;
		}
	}

	public ResultSetWrapper authenticateUser(String user_msisdn,String user_name, String user_pin) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside authenticateUser with user_msisdn="+user_msisdn +" user_name="+user_name);
		}
		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);

		try {
			if(user_name!=null && !user_name.isEmpty()) {
				userMappingDao.findUserByUserName(user_name);
				return authenticateByUserName(user_name,user_pin);
			} else if(user_msisdn!=null && !user_msisdn.isEmpty()) {
				userMappingDao.findUserByMsisdn(user_msisdn);
				return authenticateByMsisdn(user_msisdn, user_pin);
			}
		} catch (UserNotFoundException e) {
			logger.error("User not found for user_msisdn="+user_msisdn +" user_name="+user_name);
		}	
		result.setResult(Result.FAIL);
		result.setErrorCode(ErrorCode.USER_NOT_FOUND);
		return result;
	}


	public ResultSetWrapper getUserProfileByMsisdn(String user_msisdn) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside getUserProfileByMsisdn with user_msisdn="+user_msisdn);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);

		try {
			UserInfoBean user = userMappingDao.findUserByMsisdn(user_msisdn);
			result.setObject(user);
			return result;
		} catch (UserNotFoundException e) {
			logger.error("User not found for user_msisdn="+user_msisdn);
		}

		result.setResult(Result.FAIL);
		result.setErrorCode(ErrorCode.USER_NOT_FOUND);
		return result;
	}

	public ResultSetWrapper getUserProfileByImsi(String imsi) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside getUserProfileByMsisdn with imsi="+imsi);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);

		try {
			UserInfoBean user = userMappingDao.findUserByImsi(imsi);
			result.setObject(user);
			return result;
		} catch (UserNotFoundException e) {
			logger.error("User not found for imsi="+imsi);
		}

		result.setResult(Result.FAIL);
		result.setErrorCode(ErrorCode.USER_NOT_FOUND);
		return result;
	}

	public ResultSetWrapper getUserProfileByUserName(String user_name) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside getUserProfileByUserName with user_name="+user_name);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);

		try {
			UserInfoBean user = userMappingDao.findUserByUserName(user_name);
			result.setObject(user);
			return result;
		} catch (UserNotFoundException e) {
			logger.error("User not found for user_name="+user_name);
		}

		result.setResult(Result.FAIL);
		result.setErrorCode(ErrorCode.USER_NOT_FOUND);
		return result;
	}

	public ResultSetWrapper getUserProfile(String user_msisdn,String user_name) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside getUserProfile with user_msisdn="+user_msisdn +" user_name="+user_name);
		}
		ResultSetWrapper result = new ResultSetWrapper(Result.SUCCESS);

		if(user_msisdn!=null && !user_msisdn.isEmpty()) {
			return getUserProfileByMsisdn(user_msisdn);
		} else if(user_name!=null && !user_name.isEmpty()) {
			return getUserProfileByUserName(user_name);
		}	

		result.setResult(Result.FAIL);
		result.setErrorCode(ErrorCode.USER_NOT_FOUND);
		return result;
	}

	public ResultSetWrapper updateUserProfile(String user_msisdn,String user_name, JSONObject userProfile) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside getUserProfile with user_msisdn="+user_msisdn +" user_name="+user_name +" userProfile="+userProfile);
		}

		ResultSetWrapper resultSet = getUserProfile(user_msisdn, user_name);
		Result result = resultSet.getResult();

		if(result.equals(result.FAIL)) {
			return resultSet;
		}

		UserInfoBean user = (UserInfoBean) resultSet.getObject();

		try {
			UserInfoBean userInfoBean = Utils.makeUserProfileFromJson(userProfile, user);
			userMappingDao.updateUserInfo(userInfoBean);

		} catch (JSONException e) {

			resultSet.setResult(Result.FAIL);
			resultSet.setErrorCode(ErrorCode.GENERIC_ERROR);
			return resultSet;
		}

		return resultSet;
	}

	public ResultSetWrapper updateUserPassword(String user_msisdn,String user_name, String user_password) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside updateUserPassword with user_msisdn="+user_msisdn +" ,user_name: "+user_name +",user_password: "+user_password);
		}

		ResultSetWrapper resultSet = getUserProfile(user_msisdn, user_name);
		Result result = resultSet.getResult();

		if(result.equals(result.FAIL)) {
			return resultSet;
		}

		UserInfoBean userInfoBean = (UserInfoBean) resultSet.getObject();
		userInfoBean.setPassword(user_password);
		userMappingDao.updateUserPassword(userInfoBean);

		if(logger.isDebugEnabled()) {
			logger.debug("leaving updateUserPassword with resultSet="+resultSet.toDebugString());
		}
		return resultSet;
	}

	public ResultSetWrapper deleteUser(String user_msisdn,String user_name, String user_password) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside deleteUser with user_msisdn="+user_msisdn +" ,user_name: "+user_name +",user_password: "+user_password);
		}

		ResultSetWrapper resultSet = getUserProfile(user_msisdn, user_name);
		Result result = resultSet.getResult();

		if(result.equals(result.FAIL)) {
			return resultSet;
		}

		UserInfoBean userInfoBean = (UserInfoBean) resultSet.getObject();
		try {
			userMappingDao.deleteUser(userInfoBean);
		} catch (UserNotFoundException e) {
			logger.error("User not found for user_msisdn="+user_msisdn +" user_name="+user_name);
			resultSet.setResult(Result.FAIL);
			resultSet.setErrorCode(ErrorCode.USER_NOT_FOUND);
		}

		if(logger.isDebugEnabled()) {
			logger.debug("Leaving deleteUser with resultSet="+resultSet.toDebugString());
		}
		return resultSet;
	}
}
