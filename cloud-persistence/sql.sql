CREATE TABLE manyme_number
(
  id serial NOT NULL,
  available_number character varying(15),
  reserved character varying(15),
  imsi character varying(15),
  assigned_to character varying(100),--Subscriber
  CONSTRAINT manyme_number_pkey PRIMARY KEY (id)
);

CREATE TABLE manyme_activation_history
(
  id serial NOT NULL,
  manyme_number character varying(15),
  activatetion_time timestamp without time zone,
  activated boolean,
  interval_activation boolean,
  CONSTRAINT manyme_activation_history_pkey PRIMARY KEY (id)
);

CREATE TABLE manyme_recharge_history
(
  id serial NOT NULL,
  recharge_number character varying(15),
  recharge_time timestamp without time zone,
  amount double precision,
  CONSTRAINT manyme_recharge_history_pkey PRIMARY KEY (id)
);

CREATE TABLE manyme_sms
(
  id serial NOT NULL,
  sender character varying(15),
  receiver character varying(15),
  message character varying(15),
  sending_time timestamp without time zone,
  receiving_time timestamp without time zone,
  start_balance double precision,
  end_balance double precision,
  duration character varying(20),
  incoming boolean,
  CONSTRAINT manyme_sms_pkey PRIMARY KEY (id)
);

CREATE TABLE manyme_subscriber
(
  id serial NOT NULL,
  actual_msisdn character varying(15),
  "name" character varying(100),
  pin character varying(10),
  lang_code character varying(10),
  imsi character varying(15),
  CONSTRAINT manyme_subscriber_pkey PRIMARY KEY (id)
);

CREATE TABLE manyme_subscriber_number
(
  id serial NOT NULL,
  many_me_number character varying(15),
  date_purchased timestamp without time zone,
  prepaid_type boolean,
  "prefix" integer,
  bill_this_number boolean,
  imsi character varying(15),
  active boolean default true,
  default_number character varying(15),
  time_interval_set boolean,
  divert_to_number character varying(15),
  divert_to_voice_mail character varying(15),
  many_me_subscriber_id biginteger, --relates to subscriber id
  CONSTRAINT manyme_subscriber_number_pkey PRIMARY KEY (id)
);

CREATE TABLE manyme_subscriber_number_manager
(
  id serial NOT NULL,
  default_number boolean,
  interval_on_start character varying(50),
  CONSTRAINT manyme_subscriber_number+manager_pkey PRIMARY KEY (id)
);

CREATE TABLE manyme_subscriber_number_manager
(
  id serial NOT NULL,
  default_number boolean,
  interval_on_start character varying(50),
  CONSTRAINT manyme_subscriber_number+manager_pkey PRIMARY KEY (id)
);

CREATE TABLE manyme_call
(
  id serial NOT NULL,
  calling_number character varying(15),
  called_number character varying(15),
  many_me_number character varying(15),
  connecting_time timestamp without time zone,
  connected_time timestamp without time zone,
  disconnected_time timestamp without time zone,
  start_balance double precision,
  end_balance double precision,
  duration character varying(20),
  type integer,
  CONSTRAINT manyme_call_pkey PRIMARY KEY (id)
);


CREATE TABLE manyme_imsi
(
  id serial NOT NULL,
  imsi_number character varying(15),
  in_use bool,
  msrn character varying(15),
  many_me_number character varying(15),
  CONSTRAINT manyme_imsi_pkey PRIMARY KEY (id)
);
