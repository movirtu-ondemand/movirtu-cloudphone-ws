package com.movirtu.mxmanyme.dao;

import com.movirtu.mxmanyme.exceptions.RecordNotFoundException;

/**
 *
 * Records calls, whether incoming or outgoing. For each status received,
 * <br/>the record is updated on database
 *
 * @author mlungisi
 * @since 1.0
 *
 *
 */
public interface PropertiesDao {


	/**
	 * This method will not work if key is not given
	 *
	 * @param id to find the record
	 * @return call
	 * @throws RecordNotFoundException in case record not found for the given id
	 */
	String findProperties(final String key, final String tableName);

}
