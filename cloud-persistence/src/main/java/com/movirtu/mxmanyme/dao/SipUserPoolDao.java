package com.movirtu.mxmanyme.dao;

import com.movirtu.mxmanyme.domain.UserInfoBean;
import com.movirtu.mxmanyme.exceptions.NoAvailableSipUserException;
import com.movirtu.mxmanyme.exceptions.SipUserAlreadySetException;

/**
 *
 * Records calls, whether incoming or outgoing. For each status received,
 * <br/>the record is updated on database
 *
 * @author prahsant
 * @since 1.0
 *
 *
 */
public interface SipUserPoolDao {

	/**
	 * This method will search a SIP user from a sip user pool and assign it to requested user.
	 *  
	 * @param userInfo User Info object for which SIP credentials will be reserved from pool.
	 * @throws NoAvailableSipUserException in case of no free sip user is available in pool.
	 * @throws SipUserAlreadySetException if sip user is already set for the user.
	 */
	void setSipCredentials(final UserInfoBean userInfo) throws NoAvailableSipUserException , SipUserAlreadySetException;

	/**
	 * This method will search a SIP user associated with the user and release it..
	 *  
	 * @param userInfo User Info object for which SIP credentials will be release from pool.
	 * @throws NoAvailableSipUserException in case of no free sip user is available in pool.
	 */
	void releaseSipCredentials(final UserInfoBean userInfo) throws NoAvailableSipUserException;


}
