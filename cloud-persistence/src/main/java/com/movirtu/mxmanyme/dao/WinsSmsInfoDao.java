package com.movirtu.mxmanyme.dao;

import java.util.List;

import com.movirtu.mxmanyme.domain.WinsSmsInfo;
import com.movirtu.mxmanyme.exceptions.DaoException;
import com.movirtu.mxmanyme.exceptions.DuplicateImsiNumberException;
import com.movirtu.mxmanyme.exceptions.DuplicateRecordException;
import com.movirtu.mxmanyme.exceptions.RecordNotFoundException;

/**
 *
 * Records calls, whether incoming or outgoing. For each status received,
 * <br/>the record is updated on database
 *
 * @author mlungisi
 * @since 1.0
 *
 *
 */
public interface WinsSmsInfoDao {

	/**
	 *
	 * @param call values to add to database excluding id
	 * @return id generated
	 * @throws DuplicateRecordException in case of unique key violation
	 * @throws DaoException any sql issue
	 */
	long insert(final WinsSmsInfo inbox) throws DuplicateImsiNumberException;

	/**
	 * This method will not work if id is not given
	 *
	 * @param call to update existing record based on id.
	 * @throws DuplicateRecordException in case of unique key violation
	 * @throws DaoException any sql issue
	 * @throws RecordNotFoundException
	 */
	void update(final WinsSmsInfo inbox) throws DuplicateImsiNumberException, RecordNotFoundException;

	/**
	 * This method will not work if id is not given
	 *
	 * @param call with id as the important parameter to delete
	 * @throws DaoException in case of sql issues including database
	 * @throws RecordNotFoundException
	 */
	boolean deleteByMessageId(final String messageId) throws RecordNotFoundException;

	/**
	 * This method will not work if id is not given
	 *
	 * @param call with id as the important parameter to delete
	 * @throws DaoException in case of sql issues including database
	 * @throws RecordNotFoundException
	 */
	boolean deleteByMessageIdList(final List<String> messageId) throws RecordNotFoundException;

	/**
	 * This method will not work if id is not given
	 *
	 * @param call with id as the important parameter to delete
	 * @throws DaoException in case of sql issues including database
	 * @throws RecordNotFoundException
	 */
	boolean deleteMessageThread(final String user1_msisdn, final String user2_msisdn) throws RecordNotFoundException;

	/**
	 * This method will not work if id is not given
	 *
	 * @param call with id as the important parameter to delete
	 * @throws DaoException in case of sql issues including database
	 * @throws RecordNotFoundException
	 */
	boolean deleteAllMessageThreads(final String user_msisdn) throws RecordNotFoundException;

	/**
	 * This method will not work if id is not given
	 *
	 * @param id to find the record
	 * @return call
	 * @throws RecordNotFoundException in case record not found for the given id
	 */
	WinsSmsInfo findById(final long id) throws RecordNotFoundException;

	//	/**
	//	 * This method will not work if messageId is not given
	//	 *
	//	 * @param id to find the record
	//	 * @return call
	//	 * @throws RecordNotFoundException in case record not found for the given id
	//	 */
	//	WinsSmsInboxNew findBySmsId(final String messageId) throws RecordNotFoundException;

	/**
	 * This method will not work if user is not given
	 *
	 * @param id to find the record
	 * @return call
	 * @throws RecordNotFoundException in case record not found for the given id
	 */
	List<WinsSmsInfo> findAll(String user) throws RecordNotFoundException;

	/**
	 * This method will not work if user is not given
	 *
	 * @param id to find the record
	 * @return call
	 * @throws RecordNotFoundException in case record not found for the given id
	 */
	List<WinsSmsInfo> FetchSMSRecent(String user) throws RecordNotFoundException;

	//	/**
	//	 * This method will not work if user is not given
	//	 *
	//	 * @param id to find the record
	//	 * @return call
	//	 * @throws RecordNotFoundException in case record not found for the given id
	//	 */
	//	List<WinsSmsInfo> findAll(String user, String sipUser) throws RecordNotFoundException;

	//	/**
	//	 * This method will not work if aparty is not given
	//	 *
	//	 * @param id to find the record
	//	 * @return call
	//	 * @throws RecordNotFoundException in case record not found for the given id
	//	 */
	//	List<WinsSmsInboxNew> findAll(String aparty, final int numOfEntries) throws RecordNotFoundException;

	/**
	 * This method will not work if aparty and bparty are not given
	 *
	 * @param id to find the record
	 * @return call
	 * @throws RecordNotFoundException in case record not found for the given id
	 */
	List<WinsSmsInfo> findAllByBParty(String aparty, String bparty) throws RecordNotFoundException;

	//	/**
	//	 * This method will not work if aparty and bparty are not given
	//	 *
	//	 * @param id to find the record
	//	 * @return call
	//	 * @throws RecordNotFoundException in case record not found for the given id
	//	 */
	//	List<WinsSmsInfo> findAllByBParty(String aparty, String mappedAParty, String bparty, String mappedBParty) throws RecordNotFoundException;

	//	/**
	//	 * This method will not work if aparty and bparty are not given
	//	 *
	//	 * @param id to find the record
	//	 * @return call
	//	 * @throws RecordNotFoundException in case record not found for the given id
	//	 */
	//	List<WinsSmsInboxNew> findAllByBParty(String aparty, String bparty, final int numOfEntries) throws RecordNotFoundException;

	/**
	 *
	 * @return list of all calls
	 * @throws RecordNotFoundException in case no calls exist on database
	 * @throws DaoException
	 */
	List<WinsSmsInfo> findAll() throws RecordNotFoundException;

	/**
	 * This method will not work if searchString is not given
	 *
	 * @param id to find the record
	 * @return call
	 * @throws RecordNotFoundException in case record not found for the given id
	 */
	List<WinsSmsInfo> searchContent(final String user_msisdn, final String searchString) throws RecordNotFoundException;
}
