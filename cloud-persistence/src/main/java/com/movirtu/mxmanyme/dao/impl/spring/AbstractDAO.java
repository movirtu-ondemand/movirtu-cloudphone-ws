package com.movirtu.mxmanyme.dao.impl.spring;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.movirtu.usermapping.persistence.TmpUserMappingDaoImpl;

/**
 * Base Abstract Dao support class
 *
 * @author movirtu
 *
 */
public abstract class AbstractDAO<T> {

	private static Logger logger = Logger.getLogger(AbstractDAO.class);
	
    private SimpleJdbcTemplate simpleJdbcTemplate;
    protected SimpleJdbcInsert jdbcInsert;
    private String tableName;
    private Class<T> thisClass;
    @Autowired
    private DataSource dataSource;
//TODO
    public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
//TODO ends
	public AbstractDAO(String tableName, Class<T> thisClass) {
        this.tableName = tableName;
        this.thisClass = thisClass;
    }

    @PostConstruct
	protected void init() {
        simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        jdbcInsert = new SimpleJdbcInsert(dataSource);
        jdbcInsert.setTableName(this.tableName);
    }

    public SimpleJdbcTemplate getSimpleJdbcTemplate() {
        return simpleJdbcTemplate;
    }

    public T findById(Serializable id) {
        return getSimpleJdbcTemplate().getJdbcOperations().queryForObject("select * from " + tableName + " where id = ?", new Object[]{id}, thisClass);
    }

    /**
     * @return the jdbcInsert
     */
    public SimpleJdbcInsert getJdbcInsert() {
        return jdbcInsert;
    }

    /**
     * @param jdbcInsert the jdbcInsert to set
     */
    public void setJdbcInsert(SimpleJdbcInsert jdbcInsert) {
        this.jdbcInsert = jdbcInsert;
    }
}