package com.movirtu.mxmanyme.dao.impl.spring;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.movirtu.mxmanyme.dao.PropertiesDao;
import com.movirtu.mxmanyme.domain.WinsSmsInfo;

@Repository
public class PropertiesDaoImpl extends AbstractDAO<WinsSmsInfo> implements PropertiesDao {

	private static Logger logger = Logger.getLogger(PropertiesDaoImpl.class);
	private static final String TABLE_NAME = "call";

	public PropertiesDaoImpl() {
		super(TABLE_NAME, WinsSmsInfo.class);
	}

	public String findProperties(final String key, final String tableName) {
		if (logger.isDebugEnabled()) {
			logger.debug("findProperties: key =" +key);
		}
		String sql = "select value from " +tableName +" where key = ?";

		try {
			//List<String> strLst = getSimpleJdbcTemplate().query(sql, new BeanPropertyRowMapper(PropertiesRowMapper.class), key);
			List<String> strLst = getSimpleJdbcTemplate().query(sql, new PropertiesRowMapper(), key);

			if ( strLst.isEmpty() ){
				return null;
			}else if ( strLst.size() == 1 ) { // list contains exactly 1 element
				return strLst.get(0);
			}else{  // list contains more than 1 elements. returning first
				return strLst.get(0);
			}
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	private class PropertiesRowMapper implements RowMapper<String> {

		public String mapRow(ResultSet rs, int rowNum) throws SQLException {
			return rs.getString(1);
		}

	}
}
