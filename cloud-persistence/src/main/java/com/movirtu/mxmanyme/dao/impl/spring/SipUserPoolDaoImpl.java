package com.movirtu.mxmanyme.dao.impl.spring;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.movirtu.mxmanyme.dao.SipUserPoolDao;
import com.movirtu.mxmanyme.domain.SipUserInfoBean;
import com.movirtu.mxmanyme.domain.UserInfoBean;
import com.movirtu.mxmanyme.exceptions.NoAvailableSipUserException;
import com.movirtu.mxmanyme.exceptions.SipUserAlreadySetException;

@Repository
public class SipUserPoolDaoImpl extends AbstractDAO<SipUserInfoBean> implements SipUserPoolDao {

	private static Logger logger = Logger.getLogger(SipUserPoolDaoImpl.class);
	private static final String TABLE_NAME = "sip_user_pool";
	private static final int NOT_ASSIGNED=0;
	private static final int ASSIGNED=1;

	public SipUserPoolDaoImpl() {
		super(TABLE_NAME, SipUserInfoBean.class);
	}

	/**
	 * This method will search a SIP user from a sip user pool and assign it to requested user.
	 *  
	 * @param userInfo User Info object for which SIP credentials will be reserved from pool.
	 * @throws NoAvailableSipUserException in case of no free sip user is available in pool.
	 * @throws SipUserAlreadySetException if sip user is already set for the user.
	 */
	public void setSipCredentials(final UserInfoBean userInfo) throws NoAvailableSipUserException , SipUserAlreadySetException {
		if (logger.isDebugEnabled()) {
			logger.debug("setSipCredentials: userInfo =" + userInfo );
		}
		if(userInfo.getSip_user() != null) {
			throw new SipUserAlreadySetException("Sip User already set "+userInfo.getSip_user());
		}
		String sql = "select DISTINCT ON (status) * FROM sip_user_pool where status= ?";
		try {
			SipUserInfoBean sipUserInfo = getSimpleJdbcTemplate().queryForObject(sql, new SipUserInfoBeanRowMapper(), NOT_ASSIGNED);
			userInfo.setSip_user(sipUserInfo.getSip_user());
			userInfo.setSip_password(sipUserInfo.getSip_password());
		} catch (EmptyResultDataAccessException e) {
			throw new NoAvailableSipUserException("No Available Sip User found "+userInfo);
		}
		changeSipUserState(userInfo.getSip_user(),ASSIGNED);
	}

	/**
	 * This method will search a SIP user from a sip user pool and assign it to requested user.
	 *  
	 * @param userInfo User Info object for which SIP credentials will be reserved from pool.
	 * @throws NoAvailableSipUserException in case of no free sip user is available in pool.
	 * @throws SipUserAlreadySetException if sip user is already set for the user.
	 */
	public void changeSipUserState(final String sip_user, final int status) throws NoAvailableSipUserException {
		if (logger.isDebugEnabled()) {
			logger.debug("changeSipUserState: sip_user =" + sip_user  +" status="+status);
		}
		String sql = "update sip_user_pool set status=? where sip_user=?";
		try {
			getSimpleJdbcTemplate().update(sql, status, sip_user);			
		} catch (EmptyResultDataAccessException e) {
			throw new NoAvailableSipUserException("No Sip User found "+sip_user);
		}
	}

	/**
	 * This method will search a SIP user associated with the user and release it..
	 *  
	 * @param userInfo User Info object for which SIP credentials will be release from pool.
	 * @throws NoAvailableSipUserException in case of no free sip user is available in pool.
	 */
	public void releaseSipCredentials(final UserInfoBean userInfo) throws NoAvailableSipUserException {
		changeSipUserState(userInfo.getSip_user(),NOT_ASSIGNED);
	}


	/**
	 *
	 * @doc This is inner class for the implementation of RowMapper to return 
	 * SipUserInfoBean object. This object is mapped on table sip_user_pool.
	 */
	
	private class SipUserInfoBeanRowMapper implements RowMapper<SipUserInfoBean> {

		public SipUserInfoBean mapRow(ResultSet rs, int rows) throws SQLException {
			SipUserInfoBean user = new SipUserInfoBean();
			user.setId(rs.getLong(1));
			user.setMsisdn(rs.getString(2));
			user.setSip_user(rs.getString(3));
			user.setSip_password(rs.getString(4));
			user.setStatus(rs.getInt(5));
			return user;
		}
	}

}
