package com.movirtu.mxmanyme.dao.impl.spring;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.movirtu.mxmanyme.dao.WinsSmsInfoDao;
import com.movirtu.mxmanyme.domain.WinsSmsInfo;
import com.movirtu.mxmanyme.exceptions.DuplicateImsiNumberException;
import com.movirtu.mxmanyme.exceptions.RecordNotFoundException;

@Repository
public class WinsSmsInfoDaoImpl extends AbstractDAO<WinsSmsInfo> implements WinsSmsInfoDao {

	private static Logger logger = Logger.getLogger(WinsSmsInfoDaoImpl.class);
	private static final String TABLE_NAME = "call";
	private static final int OUTGOING=0;
	private static final int INCOMING=1;

	public WinsSmsInfoDaoImpl() {
		super(TABLE_NAME, WinsSmsInfo.class);
	}

	/**
	 * @see WinsSmsInfoDao#insert(Call)
	 */
	public long insert(WinsSmsInfo call) throws DuplicateImsiNumberException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	/**
	 * @throws RecordNotFoundException
	 * @see WinsSmsInfoDao#update(Call)
	 */
	public void update(WinsSmsInfo call) throws DuplicateImsiNumberException, RecordNotFoundException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	/**
	 * @throws RecordNotFoundException
	 * @see WinsSmsInfoDao#deleteByMessageId(Call)
	 */
	public boolean deleteByMessageId(String messageId) throws RecordNotFoundException {
		if (logger.isDebugEnabled()) {
			logger.debug("deleteByMessageId: messageId=" +messageId);
		}
		String sql = "delete from df_sms_info where messageid = ?";
		try {
			int rows = getSimpleJdbcTemplate().update(sql, messageId);
			return rows > 0;
		} catch (EmptyResultDataAccessException e) {
			throw new RecordNotFoundException("No SMS found for id " + messageId);
		}
	}

	/**
	 * @throws RecordNotFoundException
	 * @see WinsSmsInfoDao#deleteByMessageIdList(Call)
	 */
	public boolean deleteByMessageIdList(final List<String> messageIdList) throws RecordNotFoundException {

		if (logger.isDebugEnabled()) {
			logger.debug("deleteByMessageIdList: messageId=" +messageIdList);
		}
		String sql = "delete from df_sms_info where messageid =?";

		List<Object[]> batchArgs = new ArrayList<Object[]>();
		for(String id: messageIdList) {
			batchArgs.add(new Object[]{id});
		}
		int[] results = getSimpleJdbcTemplate().batchUpdate(sql, batchArgs);
		for(int i=0; i<results.length; i++) {
			System.out.println(results[i]); 
		}
		return true;
	}

	//	/**
	//	 * @throws RecordNotFoundException
	//	 * @see WinsSmsInfoDao#deleteByMessageIdList(Call)
	//	 */
	//	public boolean deleteByMessageIdList(final List<String> messageIdList) throws RecordNotFoundException {
	//		if (logger.isDebugEnabled()) {
	//			logger.debug("deleteByMessageIdList: messageId=" +messageIdList);
	//		}
	//		//String sql = "delete from df_sms_info where messageid IN ( ?" +")";
	//		//String sql = "delete from df_sms_info where messageid =?";
	//		//		try {
	//		//			//int rows = getSimpleJdbcTemplate().update(sql, messageIdList);
	//		//			getSimpleJdbcTemplate().queryStoredProcedure(sql, messageIdList);
	//		//			Object[] params = { messageIdList };
	//		//			return rows > 0;
	//		//		} catch (EmptyResultDataAccessException e) {
	//		//			throw new RecordNotFoundException("No SMS found for id " + messageIdList);
	//		//		}
	//		//		
	//		//		getSimpleJdbcTemplate().update(getUnnamedPreparedStatement(sql), new PreparedStatementSetter() {
	//		//
	//		//	        @Override
	//		//	        public void setValues(PreparedStatement ps) throws SQLException {
	//		//	            int i = 1;
	//		//	            for (Object o : queryParameters) {
	//		//	                ps.setObject(i++, o);
	//		//	            }
	//		//	        }
	//		//	    });
	//		try{
	//			String sql = "delete from df_sms_info where messageid =?";
	//			Object[] params = messageIdList.toArray();
	//			int[] types = {Types.CHAR};
	//			int rows = getSimpleJdbcTemplate().update(sql, params, types);
	//			System.out.println(rows + " row(s) deleted.");
	//		} catch (EmptyResultDataAccessException e) {
	//			throw new RecordNotFoundException("No SMS found for id " + messageIdList);
	//		}
	//		return true;
	//		//		final String sql = "delete from df_sms_info where messageid =?";
	//		//		PreparedStatement stmt;
	//		//		try {
	//		//			stmt = getDataSource().getConnection().prepareStatement(sql); 
	//		//			for(int i=0;i<messageIdList.size(); i++) {
	//		//				stmt.setObject(i, messageIdList.get(i),Types.CHAR);
	//		//			}
	//		//			ResultSet rs = stmt.executeQuery();
	//		//			return true;
	//		//		} catch (SQLException e) {
	//		//			logger.error("deleteMessageThread: Exception ",e);
	//		//			return false;
	//		//		} 
	//
	//		//		try {
	//		//			PreparedStatementCreator psc = new PreparedStatementCreator() {
	//		//				public PreparedStatement createPreparedStatement(Connection c) throws SQLException {
	//		//					PreparedStatement ps = c.prepareStatement(sql);
	//		//					for(String messageId : messageIdList) {
	//		//						ps.setObject(3, messageId, Types.CHAR);
	//		//					}
	//		//					return ps;
	//		//				}
	//		//			};
	//		//			PreparedStatement  ps = psc.createPreparedStatement(getDataSource().getConnection());
	//		//			getSimpleJdbcTemplate().getJdbcOperations().update(ps);
	//	}

	/**
	 * @throws RecordNotFoundException
	 * @see WinsSmsInfoDao#deleteMessageThread(Call)
	 */
	public boolean deleteMessageThread(String user1_msisdn, String user2_msisdn) throws RecordNotFoundException {
		if (logger.isDebugEnabled()) {
			logger.debug("deleteMessageThread: user1_msisdn=" +user1_msisdn +" user2_msisdn="+user2_msisdn);
		}
		//		String sql = "delete from df_sms_info where sender= ? and receiver=? and direction=? Union All delete from df_sms_info where receiver= ? and sender=? and direction=?";
		//		try {
		//			int rows = getSimpleJdbcTemplate().update(sql, user1_msisdn, user2_msisdn, OUTGOING, user1_msisdn, user2_msisdn,INCOMING);
		//			return rows > 0;
		//		} catch (EmptyResultDataAccessException e) {
		//			logger.debug("No SMS found for=" +user1_msisdn +" user2_msisdn="+user2_msisdn);
		//		}
		//		return false;
		String sql = "delete from df_sms_info where sender= ? and receiver=? and direction=? ";

		List<Object[]> batchArgs = new ArrayList<Object[]>();
		batchArgs.add(new Object[]{user1_msisdn,user2_msisdn,OUTGOING});
		batchArgs.add(new Object[]{user2_msisdn,user1_msisdn,INCOMING});
		int[] results = getSimpleJdbcTemplate().batchUpdate(sql, batchArgs);
		for(int i=0; i<results.length; i++) {
			System.out.println(results[i]); 
		}
		return true;
	}

	/**
	 * @throws RecordNotFoundException
	 * @see WinsSmsInfoDao#deleteMessageThread(Call)
	 */
	public boolean deleteAllMessageThreads(String user_msisdn) throws RecordNotFoundException {
		if (logger.isDebugEnabled()) {
			logger.debug("deleteAllMessageThreads: user1_msisdn=" +user_msisdn);
		}

		String sql = "delete from df_sms_info where sender= ? and direction=? ";
		try {
			int rows = getSimpleJdbcTemplate().update(sql, user_msisdn, OUTGOING);
			//return rows > 0;
		} catch (EmptyResultDataAccessException e) {
			throw new RecordNotFoundException("No SMS found for sender " + user_msisdn);
		}

		sql = "delete from df_sms_info where receiver= ? and direction=? ";
		try {
			int rows1 = getSimpleJdbcTemplate().update(sql, user_msisdn, INCOMING);
			return rows1 > 0;
		} catch (EmptyResultDataAccessException e) {
			throw new RecordNotFoundException("No SMS found for receiver " + user_msisdn);
		}
	}


	/**
	 * @see WinsSmsInfoDao#findById(long)
	 */
	public WinsSmsInfo findById(long id) throws RecordNotFoundException {
		if (logger.isDebugEnabled()) {
			logger.debug("findById: id =" +id);
		}
		String sql = "select messageid, sender, receiver, log_time, message  from df_sms_info where id = ? order by log_time DESC";
		try {
			WinsSmsInfo call = getSimpleJdbcTemplate().queryForObject(sql, new WinsSmsInfoRowMapper(), id);
			return call;
		} catch (EmptyResultDataAccessException e) {
			throw new RecordNotFoundException("No SMS found for id " + id);
		}
	}

	public List<WinsSmsInfo> findAll(String user) throws RecordNotFoundException {
		//		if (logger.isDebugEnabled()) {
		//			logger.debug("findAll: sender =" + sender );
		//		}
		//		String sql = "select messageid, sender, receiver, log_time, message from df_sms_info where sender= ? order by log_time";
		//		try {
		//			List<WinsSmsInboxNew> messageList = getSimpleJdbcTemplate().query(sql, new BeanPropertyRowMapper(WinsSmsInboxNew.class), sender );
		//			return messageList;
		//		} catch (EmptyResultDataAccessException e) {
		//			throw new RecordNotFoundException("No SMS found for id " + sender);
		//		}
		if (logger.isDebugEnabled()) {
			logger.debug("findAll: sender =" + user );
		}
		//String sql = "select messageid, sender, receiver, log_time, message from df_sms_info where sender= ? Union All select messageid, sender, receiver, log_time, message from df_sms_info where receiver = ? order by log_time";
		// TODO direction should be takes from some variable here
		String sql = "select * from df_sms_info where sender= ? and direction=? Union All select * from df_sms_info where receiver = ? and direction=? order by log_time DESC";
		try {
			List<WinsSmsInfo> messageList = getSimpleJdbcTemplate().query(sql, new BeanPropertyRowMapper(WinsSmsInfo.class), user,OUTGOING, user,INCOMING );
			return messageList;
		} catch (EmptyResultDataAccessException e) {
			throw new RecordNotFoundException("No SMS found for id " + user);
		}

	}

	public List<WinsSmsInfo> FetchSMSRecent(String user) throws RecordNotFoundException {
		if (logger.isDebugEnabled()) {
			logger.debug("FetchSMSRecent: user =" + user );
		}
		// TODO direction should be takes from some variable here
		String sql = "(select DISTINCT ON (receiver) * FROM df_sms_info where sender=? and direction=? order by receiver, log_time desc) UNION ALL (select DISTINCT ON (sender) * FROM df_sms_info where receiver=? and direction=? order by sender, log_time desc) order by log_time DESC;";
		try {
			List<WinsSmsInfo> messageList = getSimpleJdbcTemplate().query(sql, new BeanPropertyRowMapper(WinsSmsInfo.class), user,OUTGOING, user,INCOMING );
			return messageList;
		} catch (EmptyResultDataAccessException e) {
			throw new RecordNotFoundException("No SMS found for id " + user);
		}

	}

	//	public List<WinsSmsInfo> findAll(String sender, String sipUser) throws RecordNotFoundException {
	//		if (logger.isDebugEnabled()) {
	//			logger.debug("findAll: sender =" + sender +" sipUser="+sipUser);
	//		}
	//		//String sql = "select messageid, sender, receiver, log_time, message from df_sms_info where sender= ? Union All select messageid, sender, receiver, log_time, message from df_sms_info where receiver = ? order by log_time";
	//		String sql = "select * from df_sms_info where sender= ? OR sender= ? Union All select * from df_sms_info where receiver= ? OR receiver= ? order by log_time";
	//		try {
	//			List<WinsSmsInfo> messageList = getSimpleJdbcTemplate().query(sql, new BeanPropertyRowMapper(WinsSmsInfo.class), sender, sipUser,sender , sipUser);
	//			return messageList;
	//		} catch (EmptyResultDataAccessException e) {
	//			throw new RecordNotFoundException("No SMS found for id " + sender);
	//		}
	//
	//	}

	//	public List<WinsSmsInboxNew> findAll(String sender, int numOfEntries) throws RecordNotFoundException {
	//		if (logger.isDebugEnabled()) {
	//			logger.debug("findAll: sender =" + sender );
	//		}
	//		String sql = "select messageid, sender, receiver, log_time, message from df_sms_info where sender= ? order by log_time limit "+numOfEntries;
	//		try {
	//			List<WinsSmsInboxNew> messageList = getSimpleJdbcTemplate().query(sql, new BeanPropertyRowMapper(WinsSmsInboxNew.class), sender);
	//			return messageList;
	//		} catch (EmptyResultDataAccessException e) {
	//			throw new RecordNotFoundException("No SMS found for id " + sender);
	//		}
	//	}

	public List<WinsSmsInfo> findAllByBParty(String sender, String receiver) throws RecordNotFoundException {
		if (logger.isDebugEnabled()) {
			logger.debug("findAllByreceiver: sender= " + sender +"receiver= " + receiver);
		}
		//String sql = "select messageid, sender, receiver, log_time, message from df_sms_info where sender= ? and receiver = ? Union All select messageid, sender, receiver, log_time, message from df_sms_info where sender= ? and receiver = ? order by log_time";
		String sql = "select * from df_sms_info where sender= ? and receiver = ? and direction=? Union All select * from df_sms_info where sender= ? and receiver = ? and direction=? order by log_time DESC";
		try {
			List<WinsSmsInfo> messageList = getSimpleJdbcTemplate().query(sql, new BeanPropertyRowMapper(WinsSmsInfo.class), sender, receiver, OUTGOING, receiver, sender, INCOMING);
			return messageList;
		} catch (EmptyResultDataAccessException e) {
			throw new RecordNotFoundException("No SMS found for id " + receiver);
		}
	}

	//	public List<WinsSmsInfo> findAllByBParty(String sender, String mappedSender, String receiver, String mappedReceiver) throws RecordNotFoundException {
	//		if (logger.isDebugEnabled()) {
	//			logger.debug("findAllByreceiver: sender= " + sender +"receiver= " + receiver);
	//		}
	//		//String sql = "select messageid, sender, receiver, log_time, message from df_sms_info where sender= ? and receiver = ? Union All select messageid, sender, receiver, log_time, message from df_sms_info where sender= ? and receiver = ? order by log_time";
	//		String sql = "select * from df_sms_info where sender= ? or sender=? and receiver = ? and receiver=? Union All select * from df_sms_info where sender= ? or sender=? and receiver = ? or receiver=? order by log_time";
	//		try {
	//			List<WinsSmsInfo> messageList = getSimpleJdbcTemplate().query(sql, new BeanPropertyRowMapper(WinsSmsInfo.class), sender, mappedSender, receiver,mappedReceiver, receiver,mappedReceiver, sender, mappedSender);
	//			return messageList;
	//		} catch (EmptyResultDataAccessException e) {
	//			throw new RecordNotFoundException("No SMS found for id " + receiver);
	//		}
	//	}


	//	public List<WinsSmsInboxNew> findAllByreceiver(String sender, String receiver, int numOfEntries) throws RecordNotFoundException {
	//		if (logger.isDebugEnabled()) {
	//			logger.debug("findAllByreceiver: sender= " + sender +"receiver= " + receiver);
	//		}
	//		String sql = "select messageid, sender, receiver, log_time, message from df_sms_info where sender= ? and receiver = ? order by log_time limit "+numOfEntries;
	//		try {
	//			List<WinsSmsInboxNew> messageList = getSimpleJdbcTemplate().query(sql, new BeanPropertyRowMapper(WinsSmsInboxNew.class), sender, receiver);
	//			return messageList;
	//		} catch (EmptyResultDataAccessException e) {
	//			throw new RecordNotFoundException("No SMS found for id " + receiver);
	//		}
	//	}

	/**
	 * @see WinsSmsInfoDao#findAll()
	 */
	public List<WinsSmsInfo> findAll() throws RecordNotFoundException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public List<WinsSmsInfo> searchContent(final String user_msisdn, final String searchString) throws RecordNotFoundException {
		if (logger.isDebugEnabled()) {
			logger.debug("searchContent: user_msisdn="+user_msisdn +"searchString= " + searchString);
		}
		String sql = "select * from df_sms_info where sender = ? and direction=? and MESSAGE ~* ? Union All select * from df_sms_info where receiver = ? and direction=? and MESSAGE ~* ? order by log_time DESC";
		try {
			List<WinsSmsInfo> messageList = getSimpleJdbcTemplate().query(sql, new BeanPropertyRowMapper(WinsSmsInfo.class), user_msisdn, OUTGOING, searchString, user_msisdn, INCOMING, searchString);
			return messageList;
		} catch (EmptyResultDataAccessException e) {
			throw new RecordNotFoundException("No SMS found for id " + searchString);
		}
	}

	/**
	 *
	 * @doc author prashantkumar This is inner class for the implementation
	 * of RowMapper to return Call object as mentioned below mapRow is Call bean
	 * object setting the fields to be returned by CallMapper
	 */
	private class WinsSmsInfoRowMapper implements RowMapper<WinsSmsInfo> {

		public WinsSmsInfo mapRow(ResultSet rs, int rows) throws SQLException {
			WinsSmsInfo winSmsInbox = new WinsSmsInfo();
			winSmsInbox.setId(rs.getLong(1));
			winSmsInbox.setSender(rs.getString(2));
			winSmsInbox.setReceiver(rs.getString(3));
			winSmsInbox.setLogTime(rs.getTimestamp(4));
			winSmsInbox.setMessage(rs.getString(5));
			return winSmsInbox;
		}
	}

}
