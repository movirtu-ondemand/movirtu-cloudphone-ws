package com.movirtu.mxmanyme.dao.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.movirtu.mxmanyme.entity.MsrnPool;

/**
 *
 * Manage the msrn number pool. Provides a free msrn whenever requested if available
 * <br/> releases the msrn once not in use.
 *
 * @author prashantkumar
 * @since 1.0
 *
 *
 */
public class MsrnPoolService {

	private Logger logger = Logger.getLogger(MsrnPoolService.class);
	private static String config = "hibernate_msrnpool.cfg.xml";

	//@Autowired
	private static SessionFactory sessionFactory;
	private static ServiceRegistry serviceRegistry;
	private static volatile MsrnPoolService instance;

	private MsrnPoolService(String config) {

		try {
			// Create the SessionFactory from hibernate.cfg.xml
			Configuration configuration = new Configuration();
			//configuration.configure(Application.getXmlPath()+"/"+config);
			configuration.configure(config);
			serviceRegistry = new ServiceRegistryBuilder().applySettings(
					configuration.getProperties()).buildServiceRegistry();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}

	}

	//	private void initializeAppContext() {
	//		// open/read the application context file
	//		// This path of applicationContext.xml is a temporary fix.
	//		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("persistent-applicationContext.xml");
	//		initialize(ctx);
	//	}

	public static MsrnPoolService getInstance() {
		if(instance == null){
			instance = new MsrnPoolService(config);
		}
		return instance;	

	}

	public static void setDatabaseConfigFile(String sConfig) {
		config = sConfig;
	}


	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	//	@Autowired
	//	public void setSessionFactory(SessionFactory sessionFactory) {
	//		this.sessionFactory = sessionFactory;
	//	}

	public static void closeSessionFactory() {
		if (sessionFactory != null)
			sessionFactory.close();
	}

	public ArrayList<String> getAllMsrn() {
		ArrayList<String> ret = new ArrayList<String>();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Criteria cr = session.createCriteria(MsrnPool.class);
		cr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);;
		List<MsrnPool> list = cr.list();

		if(list != null && list.size()>0) {
			for(int i=0;i<list.size();i++) {
				ret.add(((MsrnPool) list.get(i)).getMsrn());
			}
			logger.info("MsrnPoolBean found num of rows = "+list.size());
		} else {
			logger.info("MsrnPoolBean empty");
		}
		session.getTransaction().commit();
		session.close();
		return ret;
	}

	public MsrnPool getAvailableMsrnFromPool() {
		MsrnPool ret = null;
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Criteria cr = session.createCriteria(MsrnPool.class);
		cr.add(Restrictions.eq("status", 0));
		List<MsrnPool> list = cr.list();

		if(list != null && list.size()>0) {
			MsrnPool msrn = (MsrnPool) list.get(0);
			ret = msrn;
			logger.info("Msrn found = ["+msrn.getMsrn()+"]" );
			assignMsrn(ret.getMsrn());
		} else {
			logger.error("No free Msrn found");
		}
		session.getTransaction().commit();
		session.close();
		return ret;
	}

	public MsrnPool getMsrnInfoFromPool(String msrn) {
		MsrnPool ret = null;
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Criteria cr = session.createCriteria(MsrnPool.class);
		cr.add(Restrictions.eq("msrn", msrn));
		List<MsrnPool> list = cr.list();

		if(list != null && list.size()>0) {
			ret = (MsrnPool) list.get(0);
			logger.info("Msrn info found = ["+ret+"]" );			
		} else {
			logger.error("Msrn info was not found");
		}
		session.getTransaction().commit();
		session.close();
		return ret;
	}

	public void assignMsrn(String msrn) {
		MsrnPool msrnBean = getMsrnInfoFromPool(msrn);
		if(msrnBean ==null) {
			logger.debug("No msrn info found");
		}

		logger.debug("Assigning msrn");
		msrnBean.setStatus(1);
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(msrnBean);
		session.getTransaction().commit();
		session.close();
	}

	public void releaseMsrn(String msrn) {
		MsrnPool msrnBean = getMsrnInfoFromPool(msrn);
		if(msrnBean ==null) {
			logger.debug("No msrn info found");
		}

		logger.debug("Releasing msrn");
		msrnBean.setStatus(0);
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(msrnBean);
		session.getTransaction().commit();
		session.close();
	}

}
