/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.mxmanyme.domain;

import java.io.Serializable;

/**
 *
 * @author Prashant Kumar
 */
public class SipUserInfoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String msisdn;
	private String sip_user;
	private String sip_password;
	private int status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getSip_user() {
		return sip_user;
	}

	public void setSip_user(String sip_user) {
		this.sip_user = sip_user;
	}

	public String getSip_password() {
		return sip_password;
	}

	public void setSip_password(String sip_password) {
		this.sip_password = sip_password;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof SipUserInfoBean)) {
			return false;
		}
		SipUserInfoBean other = (SipUserInfoBean) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "SipUserInfoBean{" + "id=" + id + 
				", phone_numbers=" + msisdn +
				", sip_user=" + sip_user +
				", sip_password=" + sip_password +
				", status=" + status +
				'}';
	}

}
