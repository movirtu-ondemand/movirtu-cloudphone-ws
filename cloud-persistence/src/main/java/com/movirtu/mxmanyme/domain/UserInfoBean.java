/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.mxmanyme.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Prashant Kumar
 */
@Entity
@Table(name = "users")
public class UserInfoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String user_name=null;
	private String email_id=null;
	private String password=null;
	private String first_name=null;
	private String last_name=null;
	private String user_image=null;
	private String msisdn=null;
	private String sip_user=null;
	private String sip_password=null;
	private String imsi=null;
	private int mcc=-1;
	private int mmc=-1;
	private String msrn = null;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getEmail_id() {
		return email_id;
	}

	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getUser_image() {
		return user_image;
	}

	public void setUser_image(String user_image) {
		this.user_image = user_image;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getSip_user() {
		return sip_user;
	}

	public void setSip_user(String sip_user) {
		this.sip_user = sip_user;
	}

	public String getSip_password() {
		return sip_password;
	}

	public void setSip_password(String sip_password) {
		this.sip_password = sip_password;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public int getMcc() {
		return mcc;
	}

	public void setMcc(int mcc) {
		this.mcc = mcc;
	}

	public int getMnc() {
		return mmc;
	}

	public void setMnc(int mcn) {
		this.mmc = mcn;
	}

	public String getMsrn() {
		return msrn;
	}

	public void setMsrn(String msrn) {
		this.msrn = msrn;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof UserInfoBean)) {
			return false;
		}
		UserInfoBean other = (UserInfoBean) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "WinsSmsInboxNew{" + "id=" + id + 
				", user_name=" + user_name + 
				", email_id=" + email_id + 
				", password=" + password + 
				", first_name=" + first_name + 
				", last_name=" + last_name + 
				", user_image=" + user_image + 
				", phone_numbers=" + msisdn +
				", sip_user=" + sip_user +
				", sip_password=" + sip_password +
				", imsi=" + imsi +
				", mcc=" + mcc +
				", mcn=" + mmc +
				", msrn=" + msrn +
				'}';
	}


}
