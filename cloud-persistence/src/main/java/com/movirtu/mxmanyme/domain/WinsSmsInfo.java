/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.mxmanyme.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author prashant kumar
 */
public class WinsSmsInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String messageid;
	private int direction;
	private int state;
	private String sender;
	private String receiver;
	private Timestamp logTime;
	private String message;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMessageid() {
		return messageid;
	}

	public void setMessageid(String messageid) {
		this.messageid = messageid;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public Timestamp getLogTime() {
		return logTime;
	}

	public void setLogTime(Timestamp logTime) {
		this.logTime = logTime;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof WinsSmsInfo)) {
			return false;
		}
		WinsSmsInfo other = (WinsSmsInfo) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "WinsSmsInboxNew{" + "id=" + id + ", messageid=" + messageid + ", direction=" + direction + ", state=" + state + ", sender=" + sender + ", receiver=" + receiver + ", logTime=" + logTime + ", message=" + message +'}';
	}


}
