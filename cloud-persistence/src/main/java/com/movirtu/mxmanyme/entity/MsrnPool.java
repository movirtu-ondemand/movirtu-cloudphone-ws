package com.movirtu.mxmanyme.entity;


//@Entity
//@Table(name = "msrn_pool")
public class MsrnPool {

	//	@Id @GeneratedValue
	//	@Column(name = "id")
	private int id;

	//@Column(name = "msrn")
	private String msrn;

	//@Column(name = "status")
	private int status;

	public MsrnPool() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMsrn() {
		return msrn;
	}

	public void setMsrn(String msrn) {
		this.msrn = msrn;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
