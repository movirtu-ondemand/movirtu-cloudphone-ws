package com.movirtu.mxmanyme.exceptions;

public class NoAvailableSipUserException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public NoAvailableSipUserException() {
		super();
	}

	/**
	 * @see Exception#Exception(String)
	 */
	public NoAvailableSipUserException(String message) {
		super(message);
	}

	public NoAvailableSipUserException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public NoAvailableSipUserException(Throwable arg0) {
		super(arg0);
	}
}
