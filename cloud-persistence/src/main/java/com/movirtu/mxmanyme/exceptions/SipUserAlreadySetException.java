package com.movirtu.mxmanyme.exceptions;

public class SipUserAlreadySetException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public SipUserAlreadySetException() {
        super();
    }

    public SipUserAlreadySetException(String message) {
        super(message);
    }
    
    public SipUserAlreadySetException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public SipUserAlreadySetException(Throwable arg0) {
        super(arg0);
    }
}
