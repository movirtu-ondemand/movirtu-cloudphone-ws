package com.movirtu.mxmanyme.exceptions;

/**
 *
 * In case no records found from database
 *
 * @author mlungisi
 * @since 1.0
 *
 */
public class UserNotFoundException extends Exception {

    /**
     * @see Exception#Exception(String)
     */
    public UserNotFoundException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception()
     */
    public UserNotFoundException() {
        super();
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public UserNotFoundException(String message, Throwable t) {
        super(message, t);
    }
    /**
     *
     */
    private static final long serialVersionUID = -2900954900528567619L;
}
