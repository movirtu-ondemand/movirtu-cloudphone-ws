package com.movirtu.mxmanyme.util;

import com.movirtu.mxmanyme.domain.UserInfoBean;

public class PersistanceUtils {


	public static UserInfoBean createUser(long id,
			String user_name,
			String email_id,
			String password,
			String first_name,
			String last_name,
			String user_image,
			String msisdn,
			String sip_user,
			String sip_password) {
		UserInfoBean user = new UserInfoBean();
		user.setId(id);
		user.setUser_name(user_name);
		user.setEmail_id(email_id);
		user.setPassword(sip_password);
		user.setFirst_name(first_name);
		user.setLast_name(last_name);
		user.setUser_image(user_image);
		user.setMsisdn(msisdn);
		user.setSip_user(sip_user);
		user.setSip_password(sip_password);
		return user;
	}

}
