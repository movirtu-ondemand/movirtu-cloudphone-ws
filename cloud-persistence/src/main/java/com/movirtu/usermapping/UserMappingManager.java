package com.movirtu.usermapping;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.movirtu.usermapping.persistence.UserMappingDao;

public class UserMappingManager {

	private static volatile UserMappingManager userMappingManager;
	private static UserMappingDao userMappingDao;
//	@Autowired
//	private ApplicationContext appContext;

	public static UserMappingManager getInstance() {
		if(userMappingManager==null) {
			userMappingManager = new UserMappingManager();
		}
		return userMappingManager;
	}

	private UserMappingManager() {
		//TODO
		//initializeManager();
		initializeAppContext();
	}

	private void initializeAppContext() {
		// open/read the application context file
		// This path of applicationContext.xml is a temporary fix.
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("persistent-applicationContext.xml");
		initialize(ctx);
	}

	public String getSipUserFromPhoneNumber(String phoneNumber) {
		return userMappingDao.getSipUserFromPhoneNumber(phoneNumber);
	}

	public String getPhoneNumberFromSipUser(String sipUser) {
		return userMappingDao.getPhoneNumberFromSipUser(sipUser);
	}

	public String getMappedUser(String user) {
		String mappedUser = getPhoneNumberFromSipUser(user);
		if(mappedUser==null || mappedUser.trim().length()==0) {
			mappedUser =  getSipUserFromPhoneNumber(user);
		}
		return mappedUser;
	}

	public void initialize(ApplicationContext appContext) {
		userMappingDao = (UserMappingDao)appContext.getBean("userMapping");
		System.out.println("userMappingDao="+userMappingDao);
	}

}
