package com.movirtu.usermapping.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.movirtu.mxmanyme.dao.impl.spring.AbstractDAO;
import com.movirtu.mxmanyme.domain.UserInfoBean;
import com.movirtu.mxmanyme.exceptions.DuplicateRecordException;
import com.movirtu.mxmanyme.exceptions.RecordNotFoundException;
import com.movirtu.mxmanyme.exceptions.UserNotFoundException;

@Repository
public class TmpUserMappingDaoImpl extends AbstractDAO<UserInfoBean> implements UserMappingDao {

	private static Logger logger = Logger.getLogger(TmpUserMappingDaoImpl.class);
	private static final String TABLE_NAME = "user_info";

	public TmpUserMappingDaoImpl() {
		super(TABLE_NAME, UserInfoBean.class);
	}

	@PostConstruct
	protected void init() {
		super.init();	
		getJdbcInsert().setGeneratedKeyName("id");
	}

	@Override
	public String getSipUserFromPhoneNumber(String phoneNumber) {
		System.out.println("Inside getSipUserFromPhoneNumber with phoneNumber="+phoneNumber);
		if (logger.isDebugEnabled()) {
			logger.debug("getSipMapping: phoneNumber =" +phoneNumber);
		}
		String sql = "select sip_user from user_info where phone_numbers = ?";

		try {
			List<String> strLst = getSimpleJdbcTemplate().query(sql, new PropertiesRowMapper(), phoneNumber);
			System.out.println("Leaving getSipUserFromPhoneNumber with strLst="+strLst);
			if ( strLst.isEmpty() ){
				return null;
			}else if ( strLst.size() == 1 ) { // list contains exactly 1 element
				return strLst.get(0);
			}else{  // list contains more than 1 elements. returning first
				return strLst.get(0);
			}
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public String getPhoneNumberFromSipUser(String sipUser) {
		System.out.println("Inside getPhoneNumberFromSipUser with sipUser="+sipUser);
		if (logger.isDebugEnabled()) {
			logger.debug("getPhoneMapping: sipUser =" +sipUser);
		}
		String sql = "select phone_numbers from user_info where sip_user = ?";

		try {
			List<String> strLst = getSimpleJdbcTemplate().query(sql, new PropertiesRowMapper(), sipUser);
			System.out.println("Leaving getPhoneNumberFromSipUser with strLst="+strLst);
			if ( strLst.isEmpty() ){
				return null;
			}else if ( strLst.size() == 1 ) { // list contains exactly 1 element
				return strLst.get(0);
			}else{  // list contains more than 1 elements. returning first
				return strLst.get(0);
			}
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	public void saveUserToDB(final UserInfoBean userInfo) throws DuplicateRecordException {
		if (logger.isDebugEnabled()) {
			logger.debug("saveUserToDB: userInfo =" +userInfo);
		}
		//getJdbcInsert().setGeneratedKeyName("id");
		Map<String, Object> params = new HashMap<String, Object>(3);
		//params.put("id", userInfo.getId());
		params.put("user_name", userInfo.getUser_name());
		params.put("email_id", userInfo.getEmail_id());
		params.put("password", userInfo.getPassword());
		params.put("first_name", userInfo.getFirst_name());
		params.put("last_name", userInfo.getLast_name());
		params.put("user_image", userInfo.getUser_image());
		params.put("phone_numbers", userInfo.getMsisdn());
		params.put("sip_user", userInfo.getSip_user());
		params.put("sip_password", userInfo.getSip_password());		
		getJdbcInsert().execute(params);
	}

	@Override
	public UserInfoBean findUserByMsisdn(final String number) throws UserNotFoundException {
		String sql = "SELECT * FROM user_info where phone_numbers = ?";
		try {
			return getSimpleJdbcTemplate().queryForObject(sql, new UserInfoBeanRowMapper(), number);
		} catch (EmptyResultDataAccessException e) {
			throw new UserNotFoundException(
					"Could not find number for the given msisdn number="+number, e);
		}
	}

	@Override
	public UserInfoBean findUserByUserName(final String userName) throws UserNotFoundException {
		String sql = "SELECT * FROM user_info where user_name = ?";
		try {
			return getSimpleJdbcTemplate().queryForObject(sql, new UserInfoBeanRowMapper(), userName);
		} catch (EmptyResultDataAccessException e) {
			throw new UserNotFoundException(
					"Could not find number for the given userName="+userName, e);
		}
	}

	@Override
	public UserInfoBean findUserByImsi(final String imsi) throws UserNotFoundException {
		String sql = "SELECT * FROM user_info where imsi = ?";
		try {
			return getSimpleJdbcTemplate().queryForObject(sql, new UserInfoBeanRowMapper(), imsi);
		} catch (EmptyResultDataAccessException e) {
			throw new UserNotFoundException(
					"Could not find number for the given imsi="+imsi, e);
		}
	}

	@Override
	public boolean authenticateByUserName(String user_name, String user_pin) {
		if (logger.isDebugEnabled()) {
			logger.debug("authenticateByUserName: user_name =" +user_name);
		}
		String sql = "select * from user_info where user_name = ? and password = ?";
		try {
			UserInfoBean call = getSimpleJdbcTemplate().queryForObject(sql, new UserInfoBeanRowMapper(), user_name, user_pin);
			return true;
		} catch (EmptyResultDataAccessException e) {
			logger.error("authenticateByUserName: user not authenticated =" +user_name);
			return false;
		}
	}

	@Override
	public boolean authenticateByMsisdn(String user_msisdn, String user_pin) {
		if (logger.isDebugEnabled()) {
			logger.debug("authenticateByMsisdn: user_msisdn =" +user_msisdn);
		}
		String sql = "select * from user_info where phone_numbers = ? and password = ?";
		try {
			UserInfoBean call = getSimpleJdbcTemplate().queryForObject(sql, new UserInfoBeanRowMapper(), user_msisdn, user_pin);
			return true;
		} catch (EmptyResultDataAccessException e) {
			logger.error("authenticateByMsisdn: user not authenticated =" +user_msisdn);
			return false;
		}
	}

	public boolean updateUserInfo(final UserInfoBean userInfoBean) {
		if (logger.isDebugEnabled()) {
			logger.debug("updateUserInfo: userInfoBean =" +userInfoBean);
		}

		String sql = "update user_info set user_name= ?, email_id=?, password=?, first_name=?, last_name=?, user_image=?, phone_numbers=?, sip_user=?, sip_password=?, imsi=?, mcc=?, mnc=?, msrn=? where id=? ";

		int rows = getSimpleJdbcTemplate().update(sql, userInfoBean.getUser_name(),
				userInfoBean.getEmail_id(),userInfoBean.getPassword(), userInfoBean.getFirst_name(),
				userInfoBean.getLast_name(), userInfoBean.getUser_image(), userInfoBean.getMsisdn(),
				userInfoBean.getSip_user(), userInfoBean.getSip_password(), userInfoBean.getImsi(), 
				userInfoBean.getMcc(), userInfoBean.getMnc(), userInfoBean.getMsrn(), userInfoBean.getId());

		return rows > 0;

	}

	public boolean updateUserPassword(final UserInfoBean userInfoBean) {
		if (logger.isDebugEnabled()) {
			logger.debug("updateUserPassword: userInfoBean =" +userInfoBean);
		}

		String sql = "update user_info set user_name= ?, email_id=?, password=?, first_name=?, last_name=?, user_image=?, phone_numbers=?, sip_user=?, sip_password=? where id=? ";

		int rows = getSimpleJdbcTemplate().update(sql, userInfoBean.getUser_name(),
				userInfoBean.getEmail_id(),userInfoBean.getPassword(), userInfoBean.getFirst_name(),
				userInfoBean.getLast_name(), userInfoBean.getUser_image(), userInfoBean.getMsisdn(),
				userInfoBean.getSip_user(), userInfoBean.getSip_password(), userInfoBean.getId());

		return rows > 0;

	}


	/**
	 * @doc This method deleted the user. 
	 *  
	 * @param userInfoBean bean object which contains all the info for a user. This
	 * bean is mapped to user_info table in database.
	 */
	public boolean deleteUser(final UserInfoBean userInfoBean) throws UserNotFoundException {
		if (logger.isDebugEnabled()) {
			logger.debug("Inside deleteUser: userInfoBean =" +userInfoBean);
		}

		try {
			String sql = "delete from user_info where phone_numbers= ?";
			int rows = getSimpleJdbcTemplate().update(sql, userInfoBean.getMsisdn());

			sql = "UPDATE sip_user_pool set status=0 where sip_user=?";
			int rows1 = getSimpleJdbcTemplate().update(sql, userInfoBean.getSip_user());

			return (rows > 0 && rows1 > 0);

		} catch (EmptyResultDataAccessException e) {
			throw new UserNotFoundException("No user found for id " + userInfoBean.getSip_user());
		}
	}

	private class PropertiesRowMapper implements RowMapper<String> {

		public String mapRow(ResultSet rs, int rowNum) throws SQLException {
			return rs.getString(1);
		}

	}

	/**
	 *
	 * @doc This is inner class for the implementation of RowMapper to return 
	 * UserInfoBean object. This object is mapped on table user_info table.
	 */
	private class UserInfoBeanRowMapper implements RowMapper<UserInfoBean> {

		public UserInfoBean mapRow(ResultSet rs, int rows) throws SQLException {
			UserInfoBean user = new UserInfoBean();
			user.setId(rs.getLong(1));
			user.setUser_name(rs.getString(2));
			user.setEmail_id(rs.getString(3));
			user.setPassword(rs.getString(4));
			user.setFirst_name(rs.getString(5));
			user.setLast_name(rs.getString(6));
			user.setUser_image(rs.getString(7));
			user.setMsisdn(rs.getString(8));
			user.setSip_user(rs.getString(9));
			user.setSip_password(rs.getString(10));
			user.setImsi(rs.getString(11));
			user.setMcc(rs.getInt(12));
			user.setMnc(rs.getInt(13));
			user.setMsrn(rs.getString(14));
			return user;
		}
	}


}
