package com.movirtu.usermapping.persistence;

import com.movirtu.mxmanyme.domain.UserInfoBean;
import com.movirtu.mxmanyme.exceptions.DuplicateRecordException;
import com.movirtu.mxmanyme.exceptions.RecordNotFoundException;
import com.movirtu.mxmanyme.exceptions.UserNotFoundException;

public interface UserMappingDao {

	/**
	 * @doc This method will return the sip user associated for given msisdn.
	 *  
	 * @param number msisdn number of the user.
	 */
	public String getSipUserFromPhoneNumber(String number);

	/**
	 * @doc This method will return the msisdn number associated for given sip user.
	 *  
	 * @param sipUser Sip user associated with any user.
	 */
	public String getPhoneNumberFromSipUser(String sipUser);

	/**
	 * @doc This method persists the UserInfoBean object to DB. This method will not work 
	 * if userInfo object is not give.
	 *
	 * @param userInfo User Info object to be persisted.
	 * @throws DuplicateRecordException in case duplicate record was found. 
	 */
	void saveUserToDB(final UserInfoBean userInfo) throws DuplicateRecordException;

	/**
	 * @doc This method authenticate the user based on user name and pin. This method will  
	 * not work if any of these parameters are not provided.
	 *
	 * @param user_name name associated with the user.
	 * @param user_pin pin associated with the user. 
	 */
	boolean authenticateByUserName(final String user_name, final String user_pin) ;

	/**
	 * @doc This method authenticate the user based on user msisdn and pin. This method will  
	 * not work if any of these parameters are not provided.
	 *
	 * @param number name msisdn with the user.
	 * @param user_pin pin associated with the user. 
	 */
	boolean authenticateByMsisdn(final String number, final String user_pin) ;

	/**
	 * @doc This method will search if an user already exists for a provided msisdn.
	 *  
	 * @param number msisdn number of the user.
	 */
	public UserInfoBean findUserByMsisdn(final String number) throws UserNotFoundException ;

	/**
	 * @doc This method will search if an user already exists for a provided user name.
	 *  
	 * @param userName user name of the user.
	 */
	public UserInfoBean findUserByUserName(final String userName) throws UserNotFoundException ;

	/**
	 * @doc This method will search if an user already exists for a provided user name.
	 *  
	 * @param userName user name of the user.
	 */
	public UserInfoBean findUserByImsi(final String imsi) throws UserNotFoundException ;


	/**
	 * @doc This method updated the user info for any user. 
	 *  
	 * @param userInfoBean bean object which contains all the info for a user. This
	 * bean is mapped to user_info table in database.
	 */
	public boolean updateUserInfo(final UserInfoBean userInfoBean) ;

	/**
	 * @doc This method updated the user password for any user. 
	 *  
	 * @param userInfoBean bean object which contains all the info for a user. This
	 * bean is mapped to user_info table in database.
	 */
	public boolean updateUserPassword(final UserInfoBean userInfoBean) ;

	/**
	 * @doc This method deleted the user. 
	 *  
	 * @param userInfoBean bean object which contains all the info for a user. This
	 * bean is mapped to user_info table in database.
	 */
	public boolean deleteUser(final UserInfoBean userInfoBean) throws UserNotFoundException ;


}
