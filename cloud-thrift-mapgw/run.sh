#!/bin/sh
if [ "$1" == "restart" ]; then
    kill -9 `ps -aef | grep mx-thrift-mapgw-java-console.jar | grep -v grep | awk '{print $2}'`
    wait
    java -Dlogback.configurationFile=./logback.xml -jar mx-thrift-mapgw-java-console.jar  >> /dev/null 2>&1 &
    if [ "$2" == "test" ]; then
        echo "Running on test mode"
        java -cp mx-thrift-mapgw-java-console.jar com.movirtu.thrift.server.CPPServerThread  >> /dev/null 2>&1 &
    else
        echo "Running on production mode"
    fi
elif [ "$1" == "start" ]; then
    java -Dlogback.configurationFile=./logback.xml -jar mx-thrift-mapgw-java-console.jar >> /dev/null 2>&1 &
    if [ "$2" == "test" ]; then
        echo "Running on test mode"
        java -cp mx-thrift-mapgw-java-console.jar com.movirtu.thrift.server.CPPServerThread >> /dev/null 2>&1 & 
    else
        echo "Running on production mode"
    fi
elif [ "$1" == "stop" ]; then
    kill -9 `ps -aef | grep mx-thrift-mapgw-java-console.jar | grep -v grep | awk '{print $2}'`
    echo "common mapGW stopped successfully"
elif [ "$1" == "state" ]; then
    PID=`ps -aef | grep mx-thrift-mapgw-java-console.jar | grep -v grep | awk '{print $2}'`
    if ps -p $PID > /dev/null
    then
        echo "Running"
    else
        echo "Stopped"
    fi
else
    echo "Error: Usage: ./run.sh [start] [stop] [restart] [state]"
fi
