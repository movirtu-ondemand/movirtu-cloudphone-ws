CREATE OR REPLACE FUNCTION sms_inbox(imsiin character varying, a_party character varying, text_message character varying)
  RETURNS character varying AS
$BODY$   
DECLARE   
     bParty varchar;    
BEGIN   
	select mxshare into bParty from df_mxshare_lookup where imsi = imsiin;
	--RAISE NOTICE 'Found bPart/mxshare as: %',bParty;
	if (bParty IS NOT NULL) then
		insert into df_mxshare_sms_inbox (log_time, aparty, mxshare, message, state) values (now(), a_party, bParty, text_message, 1);
		--RAISE NOTICE 'SMS added succesfully';
		return bParty;
	else
		--RAISE EXCEPTION 'mxshare could not be found for given imsi %', imsiin;
		return NULL;
	end if;
	
END;   
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sms_inbox(character varying, character varying, character varying)
  OWNER TO gain;
