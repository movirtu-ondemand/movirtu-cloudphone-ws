/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author kobodi
 */

package com.movirtu.map;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.server.TThreadPoolServer.Args;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.map.message.BaseMessage;
import com.movirtu.map.message.LURequest;
import com.movirtu.map.message.MsrnResponse;
import com.movirtu.map.message.SRISMRequest;
import com.movirtu.map.message.SmsRequest;
import com.movirtu.persistence.mxshare.dao.SettingsDao;
import com.movirtu.persistence.mxshare.dao.impl.SettingsDaoImpl;
import com.movirtu.persistence.mxshare.domain.Settings;
import com.movirtu.thrift.connection.MapJavaInterfaceWrapper;
import com.movirtu.thrift.connection.MapgwJavaConnectionManager;
import com.movirtu.thrift.connection.processor.MapJavagwProcessor;
import com.movirtu.thrift.generated.MapJavagw;
import com.movirtu.utils.PropertiesUtils;

public class MapJavaHandler extends Observable implements Observer {

	private static Logger logger = LoggerFactory.getLogger(MapJavaHandler.class);
	protected static ExecutorService Process = Executors.newFixedThreadPool(2);
	protected boolean mIsRunning = false;
	private static SettingsDao settingsDao = new SettingsDaoImpl();
	private MapJavagwProcessor mapJavaGwProcessor;
	private int listeningTo;
	private String remoteHost;
	private int remotePort;
	Timer timer ;
	private static int thriftMessageTimeoutInSec = 60;
	static Map<String, TimerTask> thriftMessageMap;
	Map<String, String> keyValuePair;
	public final static int SERVICE_ID = 1;
	public final static String SERVICE_ID_KEY = "ServiceId";
	private static volatile MapJavaHandler mapJavaHandler;

	private MapJavaHandler() {
		timer = new Timer();
		thriftMessageMap = new ConcurrentHashMap<String, TimerTask>();		
	}

	public static MapJavaHandler getInstance() {
		if(mapJavaHandler==null) {
			mapJavaHandler = new MapJavaHandler();
		}
		return mapJavaHandler;
	}

	/*
	 * (non-Javadoc)
	 * Implementing Observer interface
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	public void update(Observable observer, Object message)
	{
		logger.debug("Inside update");

		if(message instanceof BaseMessage) {
			cancelTimerTask((BaseMessage) message);
		}

		setChanged();
		notifyObservers(message);

	}

	public void initialize() {
		keyValuePair = new HashMap<String, String>();
		List<Settings> allSettings = settingsDao.getAllSettings();

		for (Settings settings : allSettings) {
			keyValuePair.put(settings.getKey(), settings.getValue());
			logger.debug("key:="+settings.getKey() +" value="+settings.getValue());
		}

		listeningTo = Integer.parseInt(keyValuePair.get("common_javaapp_port"));
		remoteHost = keyValuePair.get("mapGWHost");
		remotePort = Integer.parseInt(keyValuePair.get("mapGWPort"));
		logger.debug("common_javaapp_port="+listeningTo +" mapGWHost="+remoteHost +" mapGWPort="+remotePort);

	}

	public void start() {
		initialize();
		logger.debug("Inside javaServerThread.strat() with port="+listeningTo);
		// Initializing server connection
		mapJavaGwProcessor = new MapJavagwProcessor();
		mapJavaGwProcessor.addObserver(this);
		MapJavagw.Processor processor = new MapJavagw.Processor(mapJavaGwProcessor);

		TServerTransport transport = null;
		try {
			transport = new TServerSocket(listeningTo);
		} catch (TTransportException ex) {
			logger.error("Failed to start server at port " + listeningTo, ex);
		}
		Args args = new Args(transport);
		args.inputProtocolFactory(new TBinaryProtocol.Factory());
		args.inputTransportFactory(new TFramedTransport.Factory());
		args.maxWorkerThreads(1024);
		args.minWorkerThreads(512);
		args.outputProtocolFactory(new TBinaryProtocol.Factory());
		args.outputTransportFactory(new TFramedTransport.Factory());
		args.processor(processor);
		args.processorFactory(new TProcessorFactory(processor));
		args.protocolFactory(new TBinaryProtocol.Factory());
		args.transportFactory(new TFramedTransport.Factory());
		TThreadPoolServer poolServer = new TThreadPoolServer(args);
		logger.info("Starting server at port: " + listeningTo);

		// Initializing client connection
		sendInitRegisterNodeRequest();
		poolServer.serve();
	}

	public void stop(int listeningTo, String remoteHost, int remotePort, int tBinaryProtocolMaxLength, String url) {
		MapJavagw.Processor processor = new MapJavagw.Processor(new MapJavagwProcessor(remoteHost, remotePort, tBinaryProtocolMaxLength, url));

		TServerTransport transport = null;
		try {
			transport = new TServerSocket(listeningTo);
		} catch (TTransportException ex) {
			logger.error("Failed to stop server at port " + listeningTo, ex);
		}
		Args args = new Args(transport);
		args.inputProtocolFactory(new TBinaryProtocol.Factory());
		args.inputTransportFactory(new TFramedTransport.Factory());
		args.maxWorkerThreads(1024);
		args.minWorkerThreads(512);
		args.outputProtocolFactory(new TBinaryProtocol.Factory());
		args.outputTransportFactory(new TFramedTransport.Factory());
		args.processor(processor);
		args.processorFactory(new TProcessorFactory(processor));
		args.protocolFactory(new TBinaryProtocol.Factory());
		args.transportFactory(new TFramedTransport.Factory());
		TThreadPoolServer poolServer = new TThreadPoolServer(args);
		logger.info("Stopping server at port: " + listeningTo);
		poolServer.stop();
	}

	public static void sendInitRegisterNodeRequest() {
		try {
			Map<String, String> extension = new HashMap<String, String>();
			Properties p = PropertiesUtils.getPropertiesFile("settings.properties");
			String remoteHost = p.getProperty("mapGWHost", "localhost");
			int remotePort = Integer.parseInt(p.getProperty("mapGWPort", "16150"));
			logger.debug("Sending Node request to host "+ remoteHost + " and Port: "+ remotePort);
			MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection(remoteHost, remotePort);
			extension.put(SERVICE_ID_KEY, ""+SERVICE_ID);
			client.registerNodeRequest(extension);
			logger.debug("##Sent initial Node Request");
		} catch (Exception ex) {
			logger.error("Initial Node Request failed", ex);
		} 
	}

	public void submit(Object object){

		if(object instanceof BaseMessage) {
			createAndAddTimerTaskToMap((BaseMessage) object);
		}

		if(object instanceof SmsRequest) {
			sendSmsRequest((SmsRequest) object);
		} else if(object instanceof SRISMRequest) {
			sendSriSmRequest((SRISMRequest) object);
		} else if(object instanceof LURequest) {
			sendLocationUpdateRequest((LURequest) object);
		} else if(object instanceof MsrnResponse) {
			sendMsrnResponse((MsrnResponse) object);
		} 

	}

	private void sendMsrnResponse(MsrnResponse response) {
		logger.debug("Sending Msrn response "+response +"to host "+ remoteHost + " and Port: "+ remotePort);

		try {

			logger.debug("Sending Msrn response to host "+ remoteHost + " and Port: "+ remotePort);
			MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection(remoteHost, remotePort);                     
			client.respvMSRN(response.getServiceId(),response.getSessionId(),response.getMsrn(), response.getError(), response.getServiceAppType(), response.getExtension());
			logger.debug("Sent Msrn Response");

		} catch (Exception ex) {
			logger.error("Msrn Response failed", ex);
		} 
		
	}

	public void sendSmsRequest(SmsRequest req) {
		logger.debug("Sending SMS request "+req +"to host "+ remoteHost + " and Port: "+ remotePort);

		try {

			logger.debug("Sending Sms request to host "+ remoteHost + " and Port: "+ remotePort);
			MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection(remoteHost, remotePort);                     
			client.reqOutgoingSmsData(111,222,req.getCalling_party(),req.getCalled_party(),req.getCoding_scheme(),req.getData_string(),req.getExtension());
			logger.debug("Sent Sms Request");

		} catch (Exception ex) {
			logger.error("Sms Request failed", ex);
		} 

	} 

	public void sendSriSmRequest(SRISMRequest req) {
		logger.debug("Sending SRISM request "+req +"to host "+ remoteHost + " and Port: "+ remotePort);

		try {		
			MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection(remoteHost, remotePort);
			client.reqSriSm(req.getServiceId(),req.getSessionId(),req.getMsisdn(), req.getExtension());
			logger.debug("Sent SRISM Request");

		} catch (Exception ex) {
			logger.error("Sms Request failed", ex);
		}

	}

	public void sendLocationUpdateRequest(LURequest req) {
		logger.debug("Sending location update request "+req +"to host "+ remoteHost + " and Port: "+ remotePort);

		try {
			MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection(remoteHost, remotePort);
			client.reqUpdateVLRDetails(req.getServiceId(),req.getSessionId(),req.getImsi(), req.getExtension());
			logger.debug("Sent location update");

		} catch (Exception ex) {
			logger.error("Location Update Request failed", ex);
		}

	}

	private void createAndAddTimerTaskToMap(BaseMessage message) {
		logger.debug("Inside createAndAddTimerTaskToMap");
		String mapId = createMapId(message);
		TimerTask timerTask = new ThriftMessageTimerTask(message);
		thriftMessageMap.put(mapId, timerTask);
		timer.schedule(timerTask, thriftMessageTimeoutInSec*1000);
		logger.debug("Leaving createAndAddTimerTaskToMap");
	}

	public static BaseMessage getRequest(int serviceId, long sessionId) {
		logger.debug("Inside getRequest");
		String mapId = createMapId(serviceId, sessionId);
		logger.debug("Leaving getRequest with "+thriftMessageMap.get(mapId));
		return ((ThriftMessageTimerTask)thriftMessageMap.get(mapId)).getMessage();
	}

	private void cancelTimerTask (BaseMessage message) {
		logger.debug("Inside cancelTimerTask");
		String mapId = createMapId(message);
		TimerTask timerTask = thriftMessageMap.get(mapId);
		if(timerTask!=null) {
			timerTask.cancel();
		}
		logger.debug("Leaving cancelTimerTask");
	}

	private String createMapId (BaseMessage message) {
		return createMapId(message.getServiceId(), message.getSessionId());
	}

	private static String createMapId (int serviceId, long sessionId) {
		String mapId = ""+serviceId + ""+sessionId;
		return mapId;
	}

	class ThriftMessageTimerTask extends TimerTask {

		int serviceId;
		long sessionId;
		BaseMessage message;

		ThriftMessageTimerTask(BaseMessage message) {
			this.serviceId = message.getServiceId();
			this.sessionId = message.getSessionId();
			this.message = message;
		}

		@Override
		public void run() {
			logger.error("Did not receive/send response for serviceId=[" +serviceId +"] and sessionId=["+sessionId +"]");
		}

		public int getServiceId() {
			return serviceId;
		}

		public void setServiceId(int serviceId) {
			this.serviceId = serviceId;
		}

		public long getSessionId() {
			return sessionId;
		}

		public void setSessionId(long sessionId) {
			this.sessionId = sessionId;
		}

		public BaseMessage getMessage() {
			return message;
		}

		public void setMessage(BaseMessage message) {
			this.message = message;
		}
	}

}