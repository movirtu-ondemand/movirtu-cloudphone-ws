package com.movirtu.map.message;

import java.util.concurrent.atomic.AtomicLong;

import com.movirtu.map.MapJavaHandler;

public abstract class BaseMessage {

	protected int serviceId = MapJavaHandler.SERVICE_ID;
	long sessionId;
	private static AtomicLong counter = new AtomicLong();

	public BaseMessage() {
		sessionId = generateSessionId();
	}

	public BaseMessage(int serviceId) {
		serviceId = serviceId;
		sessionId = generateSessionId();
	}

	public BaseMessage(int serviceId, long sessionId) {
		this.serviceId = serviceId;
		this.sessionId= sessionId; 
	}

	public int getServiceId() {
		return serviceId;
	}

	protected void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public long getSessionId() {
		return sessionId;
	}

	protected void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}

	protected static long generateSessionId() {
		return counter.getAndIncrement();
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		return buffer.append("serviceId=[" +serviceId +"]"
				+"sessionId=[" +sessionId +"]"
				).toString();
	}

	public abstract BaseMessage createResponse() throws Exception ;

}
