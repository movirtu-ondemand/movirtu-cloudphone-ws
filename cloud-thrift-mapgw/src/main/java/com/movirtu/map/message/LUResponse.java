package com.movirtu.map.message;

import java.util.HashMap;
import java.util.Map;

public class LUResponse extends BaseMessage {

	String msisdn;
	String imsi;
	int mnc=-1;
	int mcc=-1;
	//static int serviceId=ServiceId.SRISM.getServiceId();
	Map<String, String> extension;
	public static final String MCC = "MCC";
	public static final String MNC = "MNC";
	public LUResponse() {

	}

	public LUResponse(String imsi, int serviceId, long sessionId) {
		this(imsi, serviceId, sessionId,  ((Map)new HashMap<String, String>()));
	}

	public LUResponse(String imsi, int serviceId, long sessionId, Map<String, String> extension) {
		//super(serviceId);
		
		this.sessionId=sessionId;

		if(imsi != null) {
			this.imsi=imsi;
		}
		if(extension!=null) {
			this.extension=extension;
		}
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public int getMnc() {
		if(mnc==-1) {
			mnc = Integer.parseInt(extension.get(LUResponse.MNC));
		}
		return mnc;
	}

	public void setMnc(int mnc) {
		this.mnc = mnc;
	}

	public int getMcc() {
		if(mcc==-1) {
			mcc = Integer.parseInt(extension.get(LUResponse.MCC));
		}
		return mcc;
	}

	public void setMcc(int mcc) {
		this.mcc = mcc;
	}

	public Map<String, String> getExtension() {
		return extension;
	}

	public void setExtension(Map<String, String> extension) {
		this.extension = extension;
	}

	@Override
	public BaseMessage createResponse() throws Exception {
		throw new Exception("Not applicable");
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		return buffer.append("msisdn=[" +msisdn +"]"
				+"imsi=[" +imsi +"]"
				+"extension=[" +extension +"]"
				).toString();
	}

}
