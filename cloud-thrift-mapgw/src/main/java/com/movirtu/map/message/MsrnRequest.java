package com.movirtu.map.message;

import java.util.HashMap;
import java.util.Map;

public class MsrnRequest extends BaseMessage {

	String imsi;
	String msisdn;
	//static int serviceId=ServiceId.LU.getServiceId();
	long sessionId;
	public static final String MSISDN = "MSISDN";
	Map<String, String> extension;

	public MsrnRequest(String imsi) {
		this(imsi, ((Map)new HashMap<String, String>()));
	}

	public MsrnRequest(String imsi, Map<String, String> extension) {
		//super(serviceId);

		if(imsi!=null) {
			this.imsi=imsi;
		}

		if(extension!=null) {
			this.extension=extension;
		}
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		extension.put(MsrnRequest.MSISDN,msisdn);
		this.msisdn = msisdn;
	}

	public Map<String, String> getExtension() {
		return extension;
	}

	public void setExtension(Map<String, String> extension) {
		this.extension = extension;
	}

	public long getSessionId() {
		return sessionId;
	}

	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	@Override
	public BaseMessage createResponse() throws Exception {
		MsrnResponse response = new MsrnResponse();
		response.setMsisdn(msisdn);
		response.setImsi(imsi);
		response.setServiceId(serviceId);
		response.setSessionId(sessionId);
		return response;
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		return buffer.append("msisdn=[" +msisdn +"]"
				+"imsi=[" +imsi +"]"
				+"extension=[" +extension +"]"
				).toString();
	}

}
