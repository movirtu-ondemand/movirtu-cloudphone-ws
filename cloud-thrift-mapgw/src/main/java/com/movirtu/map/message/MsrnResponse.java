package com.movirtu.map.message;

import java.util.HashMap;
import java.util.Map;

import com.movirtu.thrift.generated.ServiceAppType;

public class MsrnResponse extends BaseMessage {

	String msisdn;
	String imsi;
	String msrn;
	//static int serviceId=ServiceId.SRISM.getServiceId();
	Map<String, String> extension;
	short error = 0;
	ServiceAppType serviceAppType;

	public MsrnResponse() {

	}

	public MsrnResponse(String imsi, int serviceId, long sessionId) {
		this(imsi, serviceId, sessionId,  ((Map)new HashMap<String, String>()));
	}

	public MsrnResponse(String imsi, int serviceId, long sessionId, Map<String, String> extension) {
		//super(serviceId);

		this.sessionId=sessionId;

		if(imsi != null) {
			this.imsi=imsi;
		}
		if(extension!=null) {
			this.extension=extension;
		}
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getMsrn() {
		return msrn;
	}

	public void setMsrn(String msrn) {
		this.msrn = msrn;
	}

	public short getError() {
		return error;
	}

	public void setError(short error) {
		this.error = error;
	}

	public ServiceAppType getServiceAppType() {
		return serviceAppType;
	}

	public void setServiceAppType(ServiceAppType serviceAppType) {
		this.serviceAppType = serviceAppType;
	}

	@Override
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public Map<String, String> getExtension() {
		return extension;
	}

	public void setExtension(Map<String, String> extension) {
		this.extension = extension;
	}

	@Override
	public BaseMessage createResponse() throws Exception {
		throw new Exception("Not applicable");
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		return buffer.append("msisdn=[" +msisdn +"]"
				+"imsi=[" +imsi +"]"
				+"msrn=[" +msrn +"]"
				+"extension=[" +extension +"]"
				).toString();
	}

}
