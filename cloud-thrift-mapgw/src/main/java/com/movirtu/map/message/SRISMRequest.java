package com.movirtu.map.message;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class SRISMRequest extends BaseMessage {

	String msisdn;
	//static int serviceId=ServiceId.SRISM.getServiceId();
	long sessionId;
	public static final String MCC = "MCC";
	public static final String MNC = "MNC";
	Map<String, String> extension;

	public SRISMRequest(String msisdn) {
		this(msisdn, ((Map)new HashMap<String, String>()));
	}

	public SRISMRequest(String msisdn, Map<String, String> extension) {
		//super(serviceId);

		if(msisdn!=null) {
			this.msisdn=msisdn;
		}

		if(extension!=null) {
			this.extension=extension;
		}
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public Map<String, String> getExtension() {
		return extension;
	}

	public void setExtension(Map<String, String> extension) {
		this.extension = extension;
	}

	@Override
	public BaseMessage createResponse() throws Exception {
		SRISMResponse response = new SRISMResponse();
		response.setMsisdn(msisdn);
		response.setServiceId(serviceId);
		response.setSessionId(sessionId);
		return response;
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		return buffer.append("msisdn=[" +msisdn +"]"
				+"extension=[" +extension +"]"
				).toString();
	}

}
