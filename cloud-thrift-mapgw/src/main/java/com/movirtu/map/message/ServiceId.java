package com.movirtu.map.message;

public enum ServiceId {
	
	SMS (0), SRISM(1), LU(2) ;

	private int serviceId;

	ServiceId(int serviceId) {
		this.serviceId=serviceId;
	}

	public int getServiceId() {
		return this.serviceId;
	}

}
