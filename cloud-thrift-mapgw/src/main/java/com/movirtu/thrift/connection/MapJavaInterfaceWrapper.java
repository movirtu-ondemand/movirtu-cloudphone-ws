package com.movirtu.thrift.connection;

import java.util.Map;

import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.thrift.generated.MapJavaInterface;
import com.movirtu.thrift.generated.ServiceAppType;

public class MapJavaInterfaceWrapper implements MapJavaInterface.Iface {

	private static Logger logger = LoggerFactory.getLogger(MapJavaInterfaceWrapper.class);
	private MapJavaInterface.Client client;
	private String remoteHost;
	private int remotePort;

	public MapJavaInterfaceWrapper(MapJavaInterface.Client client, String remoteHost, int remotePort) {
		this.client = client;
		this.remoteHost=remoteHost;
		this.remotePort=remotePort;
	}

	public MapJavaInterface.Client getClient() {
		return client;
	}

	public void setClient(MapJavaInterface.Client client) {
		this.client = client;
	}

	public String getRemoteHost() {
		return remoteHost;
	}

	public int getRemotePort() {
		return remotePort;
	}

	public void respMissedCallDetails(int service_id, long session_id, String coding_scheme, String missed_call_data, String diverted_msisdn, Map<String,String> extension) {
		// TODO Auto-generated method stub
	}

	public void reqUpdateVLRDetails(int service_id, long session_id, String imsi, Map<String,String> extension) {
		logger.debug("sending reqUpdateVLRDetails");
		try {
			client.reqUpdateVLRDetails(service_id, session_id, imsi, extension);
			logger.debug("done sending reqUpdateVLRDetails");
		} catch (TException ex) {
			logger.error("Unable to send reqUpdateVLRDetails. Trying again");
			try {
				MapgwJavaConnectionManager.getInstance().reinitilizeClientConnection(this);
				client.reqUpdateVLRDetails(service_id, session_id, imsi, extension);
				logger.debug("done sending reqUpdateVLRDetails");
			} catch (TException ex1) {
				logger.error(ex1.getMessage(), ex1);
			}
		}
	}

	public void respvMSRN(int service_id, long session_id, String vmsrn, short error, ServiceAppType type, Map<String,String> extension) {

		logger.debug("Sending respvMSRN");
		try {
			client.respvMSRN(service_id, session_id, vmsrn, error, type, extension);
			logger.debug("done sending respvMSRN");
		} catch (TException ex) {
			logger.error("Unable to send respvMSRN. Trying again");
			try {
				MapgwJavaConnectionManager.getInstance().reinitilizeClientConnection(this);
				client.respvMSRN(service_id, session_id, vmsrn, error, type, extension);
				logger.debug("done sending respvMSRN");
			} catch (TException ex1) {
				logger.error(ex1.getMessage(), ex1);
			}
		}
	}

	public void reqUSSD(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension) {
		// TODO Auto-generated method stub
	}

	public void respInitiateUSSDSession(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, short error, Map<String,String> extension) {
		// TODO Auto-generated method stub
	}

	public void respIncommingSmsData(int service_id, long session_id, short error, Map<String,String> extension) {
		// TODO Auto-generated method stub
	}

	public void reqOutgoingSmsData(int service_id, long session_id, String calling_party, String called_party, String coding_scheme, String data_string, Map<String,String> extension) {
		logger.debug("sending reqOutgoingSmsData");
		try {
			client.reqOutgoingSmsData(service_id, session_id, calling_party, called_party, coding_scheme, data_string, extension);
			logger.debug("done sending reqOutgoingSmsData");
		} catch (TException ex) {
			logger.error("Unable to send reqOutgoingSmsData. Trying again");
			try {
				MapgwJavaConnectionManager.getInstance().reinitilizeClientConnection(this);
				client.reqOutgoingSmsData(service_id, session_id, calling_party, called_party, coding_scheme, data_string, extension);
				logger.debug("done sending respIncommingSmsData");
			} catch (TException ex1) {
				logger.error(ex1.getMessage(), ex1);
			}
		}
	}

	public void registerNodeRequest(Map<String,String> extension) {
		logger.debug("sending registerNodeRequest");
		try {
			client.registerNodeRequest(extension);
			logger.debug("done sending registerNodeRequest");
		} catch (TException ex) {
			logger.error("Unable to send registerNodeRequest. Trying again");
			try {
				MapgwJavaConnectionManager.getInstance().reinitilizeClientConnection(this);
				client.registerNodeRequest(extension);
				logger.debug("done sending registerNodeRequest");
			} catch (TException ex1) {
				logger.error(ex1.getMessage(), ex1);
			}
		}
	}

	public void registerNodeResponse(Map<String,String> extension) {
		logger.debug("sending registerNodeResponse");
		try {
			client.registerNodeResponse(extension);
			logger.debug("done sending registerNodeResponse");
		} catch (TException ex) {
			logger.error("Unable to send registerNodeResponse. Trying again");
			try {
				MapgwJavaConnectionManager.getInstance().reinitilizeClientConnection(this);
				client.registerNodeResponse(extension);
				logger.debug("done sending registerNodeResponse");
			} catch (TException ex1) {
				logger.error(ex1.getMessage(), ex1);
			}
		}
	}

	public void serviceTypeResponse(int service_id, long session_id, ServiceAppType toss, Map<String,String> extension) {
		logger.debug("sending serviceTypeResponse");
		try {
			client.serviceTypeResponse(service_id, session_id, toss, extension);
			logger.debug("done sending serviceTypeResponse");
		} catch (TException ex) {
			logger.error("Unable to send serviceTypeResponse. Trying again");
			try {
				MapgwJavaConnectionManager.getInstance().reinitilizeClientConnection(this);
				client.registerNodeResponse(extension);
				logger.debug("done sending serviceTypeResponse");
			} catch (TException ex1) {
				logger.error(ex1.getMessage(), ex1);
			}
		}
	}

	public void subscriberInfoReq(int service_id, long session_id, String msisdn, Map<String,String> extension) {
		// TODO Auto-generated method stub
	}

	@Override
	public void reqSriSm(int service_id, long session_id, String msisdn,
			Map<String, String> extension) throws TException {
		logger.debug("sending reqSriSm");
		try {
			client.reqSriSm(service_id, session_id, msisdn, extension);
			logger.debug("done sending reqSriSm");
		} catch (TException ex) {
			logger.error("Unable to send reqSriSm. Trying again");
			try {
				MapgwJavaConnectionManager.getInstance().reinitilizeClientConnection(this);
				client.reqSriSm(service_id, session_id, msisdn, extension);
				logger.debug("done sending reqSriSm");
			} catch (TException ex1) {
				logger.error(ex1.getMessage(), ex1);
			}
		}

	}

}
