package com.movirtu.thrift.connection;

import java.util.Map;

import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.thrift.generated.MapJavagw;

public class MapJavagwWrapper implements MapJavagw.Iface {

	private static Logger logger = LoggerFactory.getLogger(MapJavagwWrapper.class);	
	private MapJavagw.Client client;
	private String remoteHost;
	private int remotePort;

	public MapJavagwWrapper(MapJavagw.Client client, String remoteHost, int remotePort) {
		this.client = client;
		this.remoteHost=remoteHost;
		this.remotePort=remotePort;
	}

	public MapJavagw.Client getClient() {
		return client;
	}

	public void setClient(MapJavagw.Client client) {
		this.client = client;
	}

	public String getRemoteHost() {
		return remoteHost;
	}

	public int getRemotePort() {
		return remotePort;
	}

	public void reqMissedCallDetails(int service_id, long session_id, String msisdn, Map<String,String> extension) {
		// TODO Auto-generated method stub
	}

	public void respUpdateVLRDetails(int service_id, long session_id, String hlr_number, short error, Map<String,String> extension) {
		logger.debug("sending respUpdateVLRDetails");
		try {
			client.respUpdateVLRDetails(service_id, session_id, hlr_number, error, extension);
			logger.debug("done sending respUpdateVLRDetails");
		} catch (TException ex) {
			logger.error("Unable to send respUpdateVLRDetails. Trying again");
			try {
				MapgwJavaConnectionManager.getInstance().reinitilizeServerConnection(this);
				client.respUpdateVLRDetails(service_id, session_id, hlr_number, error, extension);
				logger.debug("done sending respUpdateVLRDetails");
			} catch (TException ex1) {
				logger.error(ex1.getMessage(), ex1);
			}
		}
	}

	public void reqvMSRN(int service_id, long session_id, String imsi, Map<String,String> extension) {
		// TODO Auto-generated method stub
	}

	public void reqInitiateUSSDSession(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension) {
		// TODO Auto-generated method stub
	}

	public void respUSSD(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String,String> extension) {
		// TODO Auto-generated method stub
	}

	public void reqIncommingSmsData(int service_id, long session_id, String imsi, String coding_scheme, String data_string, String orig_party_addr, String timestamp, Map<String,String> extension) {
		// TODO Auto-generated method stub
	}

	public void respOutgoingSmsData(int service_id, long session_id, short error, Map<String,String> extension) {
		logger.debug("sending respOutgoingSmsData");
		try {
			client.respOutgoingSmsData(service_id, session_id, error, extension);
			logger.debug("done sending respOutgoingSmsData");
		} catch (TException ex) {
			logger.error("Unable to send respOutgoingSmsData. Trying again");
			try {
				MapgwJavaConnectionManager.getInstance().reinitilizeServerConnection(this);
				client.respOutgoingSmsData(service_id, session_id, error, extension);
				logger.debug("done sending respOutgoingSmsData");
			} catch (TException ex1) {
				logger.error(ex1.getMessage(), ex1);
			}
		}
	}

	public void registerNodeRequest(Map<String,String> extension) {
		try {
			client.registerNodeRequest(extension);
			logger.debug("RegisterNode Request send");
		} catch (TException ex) {
			logger.error("Unable to send registerNodeResponse. Trying again");
			try {
				MapgwJavaConnectionManager.getInstance().reinitilizeServerConnection(this);
				client.registerNodeResponse(extension);
				logger.debug("done sending registerNodeResponse");
			} catch (TException ex1) {
				logger.error(ex1.getMessage(), ex1);
			}
		}   
	}

	public void registerNodeResponse(Map<String,String> extension) {
		try {
			client.registerNodeResponse(extension);
			logger.debug("registerNodeResponse sent");
		} catch (TException ex) {
			logger.error("Unable to send RegisterNode request. Trying again");
			try {
				MapgwJavaConnectionManager.getInstance().reinitilizeServerConnection(this);
				client.registerNodeRequest(extension);
				logger.debug("done sending RegisterNode request");
			} catch (TException ex1) {
				logger.error(ex1.getMessage(), ex1);
			}

		}   
	}

	public void serviceTypeRequest(int service_id, long session_id, String msisdn, String imsi, Map<String,String> extension) {
		// TODO Auto-generated method stub
	}

	public void subscriberInfoResp(int service_id, long session_id, String msisdn, Map<String,String> extension) {
		// TODO Auto-generated method stub
	}

	@Override
	public void respSriSm(int service_id, long session_id, String imsi,
			Map<String, String> extension) throws TException {
		// TODO Auto-generated method stub
		
	}
}
