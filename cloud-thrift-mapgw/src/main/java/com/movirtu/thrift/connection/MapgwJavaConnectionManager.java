package com.movirtu.thrift.connection;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.thrift.generated.MapJavaInterface;
import com.movirtu.thrift.generated.MapJavagw;

public class MapgwJavaConnectionManager {

	private static Logger logger = LoggerFactory.getLogger(MapgwJavaConnectionManager.class);
	private Map<String, ConnectionWrapper> connections;
	private static volatile MapgwJavaConnectionManager connectionMgr;

	private MapgwJavaConnectionManager() {
		connections = new ConcurrentHashMap<String, ConnectionWrapper>();
	}

	public static MapgwJavaConnectionManager getInstance() {
		if(connectionMgr == null) {
			connectionMgr = new MapgwJavaConnectionManager(); ;
		}
		return connectionMgr;
	}

	public void createConection(String remoteHost,int remotePort) {
		String key = remoteHost+remotePort;
		closeConnectionIfExists(key);
		ConnectionWrapper connectionWrapper = new ConnectionWrapper(remoteHost, remotePort);
		addConnectionToPool(key, connectionWrapper);
	}

	private void closeConnectionIfExists(String key) {
		if(connections.containsKey(key)) {
			TProtocol prevClient = connections.get(key).getSocket();
			prevClient.getTransport().close();
			removeConnectionFromPool(key);	
		}	
	}

	public void reinitilizeClientConnection(MapJavaInterfaceWrapper clientWrapper) {
		String key = clientWrapper.getRemoteHost()+clientWrapper.getRemotePort();
		if(connections.containsKey(key)) {
			connections.get(key).reinitializeSocket();
		}
		//return connections.get(key).getClientInterfaceClient();
	}

	public void reinitilizeServerConnection(MapJavagwWrapper clientWrapper) {
		String key = clientWrapper.getRemoteHost()+clientWrapper.getRemotePort();
		if(connections.containsKey(key)) {
			connections.get(key).reinitializeSocket();
		}
		//return connections.get(key).getServerInterfaceClient();
	}

	private void addConnectionToPool(String key, ConnectionWrapper client) {
		this.connections.put(key, client);
	}

	private void removeConnectionFromPool(String key) {
		if(connections.containsKey(key)) {
			this.connections.remove(key);
		}
	}

	public MapJavagwWrapper getServerConnection(String remoteHost, int remotePort) {
		String key = remoteHost+remotePort;
		if(!connections.containsKey(key)) {
			createConection(remoteHost, remotePort);
		}
		return connections.get(key).getServerInterfaceClient();
	}

	public MapJavaInterfaceWrapper getClientConnection(String remoteHost, int remotePort) {
		String key = remoteHost+remotePort;
		if(!connections.containsKey(key)) {
			createConection(remoteHost, remotePort);
		}
		return connections.get(key).getClientInterfaceClient();
	}

	private class ConnectionWrapper {

		private MapJavagwWrapper serverInterfaceClient;
		private MapJavaInterfaceWrapper clientInterfaceClient;
		private TProtocol socket;
		private String remoteHost;
		private int remotePort;

		public ConnectionWrapper(String remoteHost, int remotePort) {
			try {
				this.remoteHost=remoteHost;
				this.remotePort=remotePort;
				TTransport transport = new TFramedTransport(new TSocket(remoteHost, remotePort));
				TProtocol protocol = new TBinaryProtocol(transport);
				transport.open();
				serverInterfaceClient = new MapJavagwWrapper(new MapJavagw.Client(protocol), remoteHost, remotePort);
				clientInterfaceClient = new MapJavaInterfaceWrapper(new MapJavaInterface.Client(protocol), remoteHost, remotePort);
				socket = protocol;
			} catch (TTransportException ex) {
				logger.debug("Exception in opening transport", ex);
			}
		}

		public void reinitializeSocket() {
			try {
				logger.error("Reinitializing socket");
				//Closing previous socket
				socket.getTransport().close();
				//Starting new socket
				TTransport transport = new TFramedTransport(new TSocket(remoteHost, remotePort));
				TProtocol protocol = new TBinaryProtocol(transport);
				transport.open();
				serverInterfaceClient.setClient(new MapJavagw.Client(protocol));
				clientInterfaceClient.setClient(new MapJavaInterface.Client(protocol));
				socket = protocol;
			} catch (TTransportException ex) {
				logger.debug("Exception in opening transport", ex);
			}

		}

		public MapJavagwWrapper getServerInterfaceClient() {
			return serverInterfaceClient;
		}

		public MapJavaInterfaceWrapper getClientInterfaceClient() {
			return clientInterfaceClient;
		}

		public TProtocol getSocket() {
			return socket;
		}
	}

}
