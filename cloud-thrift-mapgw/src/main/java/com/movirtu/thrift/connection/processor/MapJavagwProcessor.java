package com.movirtu.thrift.connection.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.map.MapJavaHandler;
import com.movirtu.map.message.MsrnRequest;
import com.movirtu.map.message.SRISMResponse;
import com.movirtu.persistance.db.SimpleJdbc;
import com.movirtu.persistence.mxshare.dao.SettingsDao;
import com.movirtu.persistence.mxshare.dao.impl.SettingsDaoImpl;
import com.movirtu.persistence.mxshare.domain.Settings;
import com.movirtu.sms.Sms;
import com.movirtu.thrift.connection.MapJavaInterfaceWrapper;
import com.movirtu.thrift.connection.MapgwJavaConnectionManager;
import com.movirtu.thrift.generated.MapJavagw;
import com.movirtu.thrift.generated.ServiceAppType;
import com.movirtu.thrift.generated.TypeOfService;
import com.movirtu.ussd.thrift.NewUssd;


/**
 *
 * @author Mlungisi Sincuba
 */
public class MapJavagwProcessor extends Observable implements MapJavagw.Iface {

	private static Logger logger = LoggerFactory.getLogger(MapJavagwProcessor.class);
	private String remoteHost;
	private int remotePort;
	private SimpleJdbc jdbc;
	private static SettingsDao settingsDao = new SettingsDaoImpl();


	public MapJavagwProcessor() {

		Map<String, String> keyValuePair = new HashMap<String, String>();
		List<Settings> allSettings = settingsDao.getAllSettings();
		for (Settings settings : allSettings) {
			keyValuePair.put(settings.getKey(), settings.getValue());
		}

		remoteHost = keyValuePair.get("common_gw_host");
		remotePort = Integer.parseInt(keyValuePair.get("common_gw_port"));
		//		String baseUrl = keyValuePair.get("app_base_url");
		//		jdbc = new SimpleJdbc();
		//		new NewUssd(baseUrl);

	}

	@Deprecated
	public MapJavagwProcessor(String remoteHost, int remotePort, int tBinaryProtocolMaxLength) {
		this.remoteHost = remoteHost;
		this.remotePort = remotePort;
		if (tBinaryProtocolMaxLength > 0) {
		}
		logger.debug("JavaServerImpl(" + remoteHost + ", " + remotePort + ")");
		jdbc = new SimpleJdbc();
		//		if (sessionMenuDao == null) {
		//			sessionMenuDao = new SessionMenuDaoImpl();
		//		}
		//		if (menuDao == null) {
		//			menuDao = new MenuDaoImpl();
		//		}
	}

	public MapJavagwProcessor(String remoteHost, int remotePort, int tBinaryProtocolMaxLength, String baseUrl) {
		this(remoteHost, remotePort, tBinaryProtocolMaxLength);
		new NewUssd(baseUrl);
	}

	//	private TTransport getTTransport(String remoteHost, int remotePort) throws TTransportException {
	//		transport = new TFramedTransport(new TSocket(remoteHost, remotePort));
	//		transport.open();
	//		return transport;
	//	}

	@Override
	//They respond
	public void respUpdateVLRDetails(int service_id, long session_id, String hlr_number, short error, Map<String, String> extension) throws TException {

		logger.debug("service_id = " + service_id);
		logger.debug("session_id = " + session_id);
		logger.debug("hlr_number = " + hlr_number);
		logger.debug("error = " + error);
		logger.debug("extension = " + extension);

		String msisdn = extension.get("msisdn");
		String imsi = extension.get("imsi");

		if((imsi==null || imsi.isEmpty()) && (msisdn==null || msisdn.isEmpty())) {
			logger.error("No imsi or msisdn present in extension");
			return;
		}
	}

	private int setMxServiceStatus(int state, String mxshare, String imsi){
		String columnName = "mxshare";
		String columnValue = mxshare;
		if(mxshare==null) {
			columnName = "imsi";
			columnValue = imsi;
		}
		String sql = "update df_mxshare_lookup set service_granted = ? where " +columnName +" = ?";
		int changed = jdbc.update(sql, state, columnValue);
		return changed;
	}

	private void addMxToWaitingTable(String mxshare, String imsi){
		String columnName = "mxshare";
		String columnValue = mxshare;
		if(mxshare==null) {
			columnName = "imsi";
			columnValue = imsi;
		}		
		String sql = "INSERT INTO df_mxs_await_vlrupdate(id, log_time, " +columnName +" ) VALUES (default, now(), ?)";                
		jdbc.update(sql, columnValue);
	}


	@Override
	//They request
	public void reqvMSRN(int serviceId, long sessionId, String imsi, Map<String, String> extension) throws TException {
		logger.info("Received reqvMSRN");
		logger.debug("service_id = " + serviceId);
		logger.debug("session_id = " + sessionId);
		logger.debug("imsi = " + imsi);
		logger.debug("extension = " + extension);

		try {
			MsrnRequest request = new MsrnRequest(imsi, extension);
			request.setServiceId(serviceId);
			request.setSessionId(sessionId);
			setChanged();
			notifyObservers(request);

		} catch (Exception ex) {
			logger.error("Exception in creating response", ex);
		}
	}

	@Override
	public void reqMissedCallDetails(int service_id, long session_id, String msisdn, Map<String, String> extension) throws TException {
		//TODO
	}

	@Override
	public void reqInitiateUSSDSession(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String, String> extension) throws TException {		
		//TODO
	}

	@Override
	public void respUSSD(int service_id, long session_id, String coding_scheme, String data_string, String msisdn, Map<String, String> extension) throws TException {
		//TODO
	}

	@Override
	public void reqIncommingSmsData(int service_id, long session_id, String imsi, String coding_scheme, String data_string, String orig_party_addr, String timestamp, Map<String, String> extension) throws TException {
		//TODO

	}

	@Override
	public void respOutgoingSmsData(int service_id, long session_id, short error, Map<String, String> extension) throws TException {
		logger.info("Received respOutgoingSmsData");
		logger.debug("service_id = " + service_id);
		logger.debug("session_id = " + session_id);
		logger.debug("error = " + error);
		logger.debug("extension = " + extension);
		Sms sms = new com.movirtu.sms.mo.mxshare.SmsMxShareImp();
		if (extension == null) {
			extension = new HashMap<String, String>();
		}
		String calling_party = extension.get("calling_party");
		String called_party = extension.get("called_party");
		String coding_scheme = extension.get("coding_scheme");
		String data_string = extension.get("data_string");
		int added = sms.sendSms(calling_party, called_party, coding_scheme, data_string, extension);
		//        error = (short) (added == 0 ? 4 : 0);
	}

	public void serviceTypeRequest(int service_id, long session_id, String msisdn, String imsi, Map<String, String> extension) throws TException {
		logger.info("Entering serviceTypeRequest with: service_id = {}, session_id = {}, msisdn = {}, imsi = {}, extension = {}",
				service_id, session_id, msisdn, imsi, extension);
		ServiceAppType appType = new ServiceAppType();
		//TODO 
		//String sql = "select imsi from df_mxshare_lookup where imsi = ?";
		//String foundImsi = jdbc.queryForPrimitiveWrapper(sql, String.class, imsi);
		//		if (foundImsi != null) {
		//			appType.setType(TypeOfService.MxShareService);
		//		} else {
		//			appType.setType(TypeOfService.ManyMeService);
		//		}
		// This is a temporaray fix. Actual service id needs to be get from DB
		appType.setType(TypeOfService.ManyMeService);
		try {
			logger.info("getting connection for remoteHost="+remoteHost +" and port="+remotePort);
			MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection(remoteHost, remotePort);
			if (extension == null) {
				extension = new HashMap<String, String>();
			}
			extension.put("imsi", imsi);
			// This is a temporaray fix. need to send actual msisdn
			//extension.put("msisdn", msisdn);
			extension.put("msisdn", "12345");
			client.serviceTypeResponse(service_id, session_id, appType, extension);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			throw new RuntimeException(ex);
		}
		logger.info("Leaving serviceTypeRequest with: service_id = {}, session_id = {}, appType = {}, extension = {}",
				service_id, session_id, appType, extension);
	}

	public void subscriberInfoResp(int service_id, long session_id, String msisdn, Map<String, String> extension) throws TException {
		//TODO
	}

	public void registerNodeRequest(Map<String, String> extension) throws TException {
		logger.info("Registering Node Request ==>");
		MapJavaInterfaceWrapper client = MapgwJavaConnectionManager.getInstance().getClientConnection(remoteHost, remotePort);
		client.registerNodeResponse(extension);
		logger.info("Send NodeResponse");
		logger.info("Just finished registerNodeRequest");
	}

	public void registerNodeResponse(Map<String, String> extension) throws TException {
		logger.debug("Registering Node Response recieved");
		logger.info("getting connection for remoteHost="+remoteHost +" and port="+remotePort);
	}

	@Override
	public void respSriSm(int serviceId, long sessionId, String imsi,
			Map<String, String> extension) throws TException {

		logger.info("Received respSriSm");
		logger.debug("service_id = " + serviceId);
		logger.debug("session_id = " + sessionId);
		logger.debug("imsi = " + imsi);
		logger.debug("extension = " + extension);

		try {
			SRISMResponse sriSmResponse = (SRISMResponse) MapJavaHandler.getRequest(serviceId, sessionId).createResponse();
			sriSmResponse.setImsi(imsi);
			sriSmResponse.setExtension(extension);

			setChanged();
			notifyObservers(sriSmResponse);

		} catch (Exception ex) {
			logger.error("Exception in creating response", ex);
		}
	}

}
