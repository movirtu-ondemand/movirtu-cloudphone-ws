/*
This is interface definition for MapGw and Java App
Author: Kushal Dev Malik
Date: 08-05-2012
*/
include "shared.thrift"
namespace cpp com.movirtu.mapjavainterface
/*
application needs to implement these methods as receiver/server; mapgateway is the client/sender
*/

service mapjavagw{

oneway void reqMissedCallDetails(1:i32 service_id, 2:i64 session_id, 3: string msisdn, 4:map<string,string>extension),
oneway void respUpdateVLRDetails(1:i32 service_id, 2:i64 session_id, 3:string hlr_number, 4:i16 error,5:map<string,string>extension),
oneway void reqvMSRN(1:i32 service_id, 2:i64 session_id, 3:string imsi, 4:map<string,string>extension),
oneway void reqInitiateUSSDSession(1:i32 service_id, 2:i64 session_id, 3:string coding_scheme, 4:string data_string, 5:string msisdn, 6:map<string,string>extension),
oneway void respUSSD(1:i32 service_id, 2:i64 session_id, 3:string coding_scheme, 4:string data_string, 5:string msisdn, 6:map<string,string>extension),
oneway void reqIncommingSmsData(1:i32 service_id, 2:i64 session_id, 3:string imsi, 4:string coding_scheme,5:string data_string, 6:string orig_party_addr, 7:string timestamp, 8:map<string,string>extension),
oneway void respOutgoingSmsData(1:i32 service_id, 2:i64 session_id, 3:i16 error, 4:map<string,string>extension),
oneway void registerNodeRequest( 1:map<string,string>extension),
oneway void registerNodeResponse( 1:map<string,string>extension),
oneway void serviceTypeRequest(1:i32 service_id, 2:i64 session_id, 3:string msisdn, 4:string imsi, 5:map<string,string>extension),
oneway void subscriberInfoResp(1:i32 service_id, 2:i64 session_id, 3:string msisdn, 4:map<string,string>extension)
}

/*
mapgateway needs to implement these methods as receiver/server; application is the client/sender
*/
service mapjavainterface{
oneway void respMissedCallDetails(1:i32 service_id, 2:i64 session_id, 3:string coding_scheme, 4:string missed_call_data, 5:string diverted_msisdn, 6:map<string,string>extension),
oneway void reqUpdateVLRDetails(1: i32 service_id, 2:i64 session_id, 3:string vimsi, 4:map<string,string>extension),
oneway void respvMSRN(1:i32 service_id, 2:i64 session_id, 3:string vmsrn, 4:i16 error, 5:shared.serviceAppType type, 6:map<string,string>extension),
oneway void reqUSSD(1:i32 service_id, 2:i64 session_id, 3:string coding_scheme, 4:string data_string, 5:string msisdn, 6:map<string,string>extension),
oneway void respInitiateUSSDSession(1:i32 service_id, 2:i64 session_id, 3:string coding_scheme, 4:string data_string, 5:string msisdn, 6:i16 error, 7:map<string,string>extension),
oneway void respIncommingSmsData(1:i32 service_id, 2:i64 session_id, 3:i16 error, 4:map<string,string>extension),
oneway void reqOutgoingSmsData(1:i32 service_id, 2:i64 session_id, 3:string calling_party, 4: string called_party, 5:string coding_scheme, 6:string data_string, 7:map<string,string>extension),
oneway void registerNodeRequest( 1:map<string,string>extension),
oneway void registerNodeResponse( 1:map<string,string>extension),
oneway void serviceTypeResponse(1:i32 service_id, 2:i64 session_id, 3:shared.serviceAppType toss,  4:map<string,string>extension),
oneway void subscriberInfoReq(1:i32 service_id, 2:i64 session_id, 3:string msisdn, 4:map<string,string>extension)
}

