namespace cpp com.movirtu.MxServiceJavainterface

enum TypeOfService {
MxShareService = 0,
ManyMeService = 1
}

struct serviceAppType {
1:TypeOfService type,
}

enum callStateType {
CallState_connecting = 0,
CallState_connected = 1,
CallState_disconnected = 2
}

struct callStateType_t {
1:callStateType callstate,
}

enum typeOfCall {
OutgoingCall = 0,
IncomingCall=1
}

struct typeOfCall_t {
1:typeOfCall callType,
}

enum errorType {
SubscriberAvailable = 0,
InvalidMsisdn = 1,
SubscriberUnavailable = 2,
InvalidImsi = 3, 
MSRNPoolExhausted = 4,
SessionIdInUse = 5,
UnknownReason = 127,

}

struct errorType_t {
1:errorType error,
}
