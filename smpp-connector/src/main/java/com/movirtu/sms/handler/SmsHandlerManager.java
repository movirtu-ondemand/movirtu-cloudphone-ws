package com.movirtu.sms.handler;

import com.movirtu.mxmanyme.domain.WinsSmsInfo;
import com.movirtu.smshandler.smpp.SmppSMsHandler;

public class SmsHandlerManager {

	private static volatile SmsHandlerManager smsManager;
	private SmsHandler smsHandler;
	//private SmsHandler smsHandler2;

//	@Autowired
//	public static PropertiesDao propertiesDao;
	//	private static String SMS_HANDLER_KEY="sms_handler";
	//	private static String smsHandlerName="ami";
	//	private static String PROPERTIES_TABLE="wins_properties";

	//	public static enum SmsHandlerName {
	//
	//		AMI("ami"), SMPP ("smpp"), CSOFT("csoft");
	//
	//		private String name;
	//
	//		SmsHandlerName(String name) {
	//			this.name=name;
	//		}
	//
	//		public String getName() {
	//			return this.name;
	//		}
	//	}

	public static SmsHandlerManager getInstance() {
		if(smsManager==null) {
			smsManager = new SmsHandlerManager();
		}
		return smsManager;
	}

	private SmsHandlerManager() {
		//TODO
		//initializeManager();
		initialize();
	}

	// TODO
	//public void initializeManager() {
	//public void initialize(PropertiesDao propertiesDao) {
	public void initialize() {
		//this.propertiesDao = propertiesDao;
		//		this.loopbackSmsHandler= LoopbackSmsHandler.getInstace();
		//		this.incomingSmsHandler = AmiSmsHandler.getInstace();

		//String smsHandlerName = propertiesDao.findProperties(SMS_HANDLER_KEY, PROPERTIES_TABLE);

		//		if(smsHandlerName.equalsIgnoreCase(SmsHandlerManager.SmsHandlerName.AMI.getName())) {
		//
		//			//this.smsHandler= AmiSmsHandler.getInstace();
		//			System.out.println("using AMI sms handler="+smsHandler);
		//
		//		} else if(smsHandlerName.equalsIgnoreCase(SmsHandlerManager.SmsHandlerName.CSOFT.getName())) {
		//
		//			//this.smsHandler= CSoftSmsHandler.getInstace();
		//			System.out.println("using csoft sms handler"+smsHandler);
		//
		//			//		} else if(smsHandlerName.equalsIgnoreCase(SmsHandlerManager.SmsHandlerName.LOOPBACK.getName())) {
		//			//			
		//			//			this.smsHandler= LoopbackSmsHandler.getInstace();
		//			//			System.out.println("using loopback sms handler"+smsHandler);
		//		} else if(smsHandlerName.equalsIgnoreCase(SmsHandlerManager.SmsHandlerName.SMPP.getName())) {

		this.smsHandler= new SmppSMsHandler();
		//this.smsHandler2= new Smpp2SMsHandler();
		System.out.println("using smpp sms handler"+smsHandler);

		//		} else {
		//			System.out.println("TODO : need work");
		//		}
		//		switch(smsHandlerName) {
		//		case SmsHandlerManager.SmsHandlerName.AMI.getName():
		//			break;
		//		case SmsHandlerManager.SmsHandlerName.SMPP.getName():
		//			break;
	}

	public SmsHandler getSmsHandler() {
		return this.smsHandler;
	}

	//	public SmsHandler getLoopbackSmsHandler() {
	//		return this.loopbackSmsHandler;
	//	}

	//	public boolean sendSms(WinsSmsInfo sms) {
	//		//		if(((LoopbackSmsHandler)loopbackSmsHandler).useLoopbackHandler(sms)) {
	//		//			return loopbackSmsHandler.sendSms(sms);
	//		//		}else{
	//		return smsHandler.sendSms(sms);
	//		//		}
	//	}

	public boolean sendSms(String sender, String receiver, String message) {
		//		if(((LoopbackSmsHandler)loopbackSmsHandler).useLoopbackHandler(sms)) {
		//			return loopbackSmsHandler.sendSms(sms);
		//		}else{
		return smsHandler.sendSms(sender, receiver, message);
		//		}
	}

	public boolean DeliverIncomingSms(WinsSmsInfo sms) {
		//TODO
		//return incomingSmsHandler.sendSms(sms);
		return true;
	}

	//	public void registerSmsHandler(SmsHandler smsHandler) {
	//		this.smsHandler = smsHandler;
	//	}

}
