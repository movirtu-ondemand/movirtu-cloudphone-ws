CREATE TABLE df_sms_info (
    id integer NOT NULL,
    messageid character varying(20),
    direction integer,
    state integer,
    sender character varying(20),
    receiver character varying(20),
    log_time timestamp without time zone,
    message character varying(512)
);

ALTER TABLE public.df_sms_info OWNER TO wins;

CREATE SEQUENCE df_sms_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER TABLE public.df_sms_info_id_seq OWNER TO wins;

ALTER SEQUENCE df_sms_info_id_seq OWNED BY df_sms_info.id;

SELECT pg_catalog.setval('df_sms_info_id_seq', 1, true);

ALTER TABLE ONLY df_sms_info ALTER COLUMN id SET DEFAULT nextval('df_sms_info_id_seq'::regclass);


