DROP TABLE IF EXISTS user_info;

CREATE TABLE user_info (
  id int NOT NULL, 
  user_name character varying(20),
  email_id character varying(50),
  password character varying(20),
  first_name character varying(20),
  last_name character varying(20),
  user_image character varying(50) DEFAULT NULL,
  phone_numbers character varying(500) NOT NULL,
  sip_user character varying(50) NOT NULL,
  sip_password character varying(50) NOT NULL
); 

ALTER TABLE public.user_info OWNER TO wins;

CREATE SEQUENCE user_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER TABLE public.user_info_id_seq OWNER TO wins;

ALTER SEQUENCE user_info_id_seq OWNED BY user_info.id;

SELECT pg_catalog.setval('user_info_id_seq', 1, true);

ALTER TABLE ONLY user_info ALTER COLUMN id SET DEFAULT nextval('user_info_id_seq'::regclass);


--
-- Dumping data for table `user_info`
--

INSERT INTO user_info VALUES (6,'demo','john.smith@movirtu.com','ZGVtbw==','john','smith','kim.jpg','+442037571080','3101','m0v1rtu123@'),(7,'demo1','','ZGVtbw==','john kevin','lorayna','kim.jpg','+442037571080','3101','m0v1rtu123@'),(8,'demo2','demo2@movirtu.com','ZGVtbw==','john2','smith','kim.jpg','+442037571080','3101','m0v1rtu123@'),(9,'dialogic','','ZGlhbG9naWM=','Dialogic','D','Ruchita.jpg','+442037571080','3101','m0v1rtu123@'),(10,'carsten','','Y2Fyc3Rlbg==','Carsten','B','Carsten.jpg','+442037571080','3101','m0v1rtu123@'),(11,'kim','','a2lt','Kim','Hartlev','kim.jpg','+442037571080','3101','m0v1rtu123@'),(12,'eike','','ZWlrZQ==','Eike','Marx','Eike.jpg','+442037571080','3101','m0v1rtu123@'),(13,'johnny','','am9obm55','Johnny','Gray','Johnny.jpg','+442037571080','3101','m0v1rtu123@'),(14,'gaurav','','Z2F1cmF2','Gaurav','Sharma','gaurav.jpg','+442037571080','3101','m0v1rtu123@'),(15,'dingani','','ZGluZ2FuaQ==','Dingani','Brian','Dingani.jpg','+442037571080','3101','m0v1rtu123@'),(18,'mouli','contactmouli@gmail.com','bW91bGk=','Chandramouli','Parasuraman','login-img.png','+442037571080','3101','m0v1rtu123@'),(19,'wins','kim@movirtu.com','d2lucw==','Warren','Mejlaq','login-img.png','+442037571080','3101','m0v1rtu123@'),(21,'m1','m1@gmail.com','cGFzc3dvcmQ=','Chandramouli','Parasuraman','mouli-sreya.JPG','+442037571080','3101','m0v1rtu123@'),(22,'m2','m2@gmail.com','cGFzc3dvcmQ=','Chandramouli','Parasuraman','mouli-sreya.JPG','+919840783974','3101','m0v1rtu123@'),(23,'pnp','pnparasuraman@gmail.com','cGFzc3dvcmQ=','Parasuraman','Natarajasarma','pnp.jpg','+442037571080','3101','m0v1rtu123@'),(25, 'demo','test2@movirtu.com','ZGVtbw==','martin','allen','login-img.png','+919841696856','3101','m0v1rtu123@'),(26, 'Atin','Atin@gmail.com','ZGVtbw==','Atin','kumar','login-img.png','+919994113206','3101','m0v1rtu123@'),(27, 'Prashant','prashant@gmail.com','ZGVtbw==','Prashant','kumar','login-img.png','+919899355555','2100','m0v1rtu123@') ,(27, 'Prashant-1','prashant-1@gmail.com','ZGVtbw==','Prashant-1','kumar-1','login-img.png','+919899366666','2101','m0v1rtu123@') ;

