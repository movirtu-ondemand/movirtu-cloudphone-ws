CREATE TABLE msrn_pool (
   id integer NOT NULL,
   msrn character varying(20),
   status integer
);

ALTER TABLE public.msrn_pool OWNER TO wins;

CREATE SEQUENCE msrn_pool_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER TABLE public.msrn_pool_id_seq OWNER TO wins;

ALTER SEQUENCE msrn_pool_id_seq OWNED BY msrn_pool.id;

SELECT pg_catalog.setval('msrn_pool_id_seq', 1, true);

ALTER TABLE msrn_pool ALTER COLUMN id SET DEFAULT nextval('"msrn_pool_id_seq"');

