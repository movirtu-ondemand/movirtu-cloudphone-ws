CREATE TABLE sip_user_pool (
    id integer NOT NULL,
    messageid character varying(20),
    direction integer,
    state integer,
    sender character varying(20),
    receiver character varying(20),
    log_time timestamp without time zone,
    message character varying(512)
);

ALTER TABLE public.sip_user_pool OWNER TO wins;

CREATE SEQUENCE sip_user_pool_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER TABLE public.sip_user_pool_id_seq OWNER TO wins;

ALTER SEQUENCE sip_user_pool_id_seq OWNED BY sip_user_pool.id;

SELECT pg_catalog.setval('sip_user_pool_id_seq', 1, true);

ALTER TABLE ONLY sip_user_pool ALTER COLUMN id SET DEFAULT nextval('sip_user_pool_id_seq'::regclass);

