CREATE TABLE user_info (
    id integer NOT NULL,
    user_name character varying(20),
    email_id character varying(50),
    password character varying(20),
    first_name character varying(20),
    last_name character varying(20),
    user_image character varying(50) DEFAULT NULL::character varying,
    phone_numbers character varying(500) NOT NULL,
    sip_user character varying(50) NOT NULL,
    sip_password character varying(50) NOT NULL
);

ALTER TABLE public.user_info OWNER TO wins;

CREATE SEQUENCE user_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER TABLE public.user_info_id_seq OWNER TO wins;

ALTER SEQUENCE user_info_id_seq OWNED BY user_info.id;

SELECT pg_catalog.setval('user_info_id_seq', 1, true);

ALTER TABLE user_info ALTER COLUMN id SET DEFAULT nextval('"user_info_id_seq"');

